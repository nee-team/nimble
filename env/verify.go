package env

import (
	"github.com/spf13/viper"
)

// VerifyConfig 校验系统必备的配置信息
func VerifyConfig() {
	currentEnv := verifyENV()
	verifyDPM(currentEnv)
}

func verifyENV() RuntimeEnv {
	currentEnv := GetRuntimeEnv() // 可以读取环境变量 CFG_ENV
	if currentEnv != Production && currentEnv != Development && currentEnv != Test {
		panic(`
==================================== 

请通过环境变量配置当前运行的环境：

- production  生产环境
- development 开发环境
- test        测试环境

示例：
export CFG_ENV=development

警告：
在开发测试环境，允许通过 cookie 模拟/切换登录身份，请务必确保在生产环境使用 production 配置！

====================================`)
	}

	return currentEnv
}

func verifyDPM(currentEnv RuntimeEnv) {
	domain := viper.GetString("app.domain")
	project := viper.GetString("app.project")
	module := viper.GetString("app.module")

	if currentEnv != Development && (len(domain) == 0 || len(project) == 0 || len(module) == 0) {
		panic(`
====================================================================================== 
非开发环境（` + string(currentEnv) + `）必须配置 DOMAIN/PROJECT/MODULE 信息，请配置如下信息: 
--------------------------------------------------------------------------------------

- app.domain  业务领域
- app.project 项目名称
- app.module  模块名称

--------------------------------------------------------------------------------------
通过环境变量进行配置请参见：http://rasse.pages.oa.com/build-in/config.html	
======================================================================================`)
	}
}
