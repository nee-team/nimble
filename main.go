package main

import (
	"gitee.com/nee-team/nimble/launcher"
)

func main() {
	app := launcher.NewApp()
	app.Start()
}
