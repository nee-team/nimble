package ip

import (
	"fmt"
	"net"
	"strconv"
	"strings"
)

var (
	intranetIP    string
	getIntraIPErr error
)

// GetPublicIP 通过连接远程地址（remoteHost:remotePort）获取公开的 IP 地址
func GetPublicIP(remoteHost string, remotePort int) (string, error) {
	conn, err := net.Dial("udp", remoteHost+":"+strconv.Itoa(remotePort))
	if err != nil {
		return "", err
	}

	defer conn.Close()
	localAddr := conn.LocalAddr().String()
	idx := strings.LastIndex(localAddr, ":")
	return localAddr[0:idx], nil
}

// GetIntranetIP 获取内网地址
func GetIntranetIP() (string, error) {
	if len(intranetIP) == 0 && getIntraIPErr == nil {
		// 由于getIntranetIP接口比较耗时，因此在首次调用后将结果缓存，可显著提升性能
		intranetIP, getIntraIPErr = getIntranetIP()
	}
	return intranetIP, getIntraIPErr
}

func getIntranetIP() (string, error) {
	addrs, err := net.InterfaceAddrs()

	if err != nil {
		return "", err
	}

	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP != nil && ipnet.IP.To4() != nil {
				// fmt.Printf("%s: %v %v %v %v %v %v\n", ipnet.IP.To4(), ipnet.IP.IsGlobalUnicast(), ipnet.IP.IsInterfaceLocalMulticast(), ipnet.IP.IsLinkLocalMulticast(), ipnet.IP.IsLinkLocalUnicast(), ipnet.IP.IsMulticast(), ipnet.IP.IsUnspecified())
				return ipnet.IP.String(), nil
			}
		}
	}

	return "", fmt.Errorf("Fail to get intranet IP.")
}
