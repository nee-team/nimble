package ip

import (
	"testing"
)

// GetPublicIP 获取公开地址
func TestGetPublicIP(t *testing.T) {
	ip, _ := GetPublicIP("oa.com", 80)
	t.Logf("Got ip: %s", ip)
}

// GetIntranetIP 获取公开地址
func TestGetIntranetIP(t *testing.T) {
	ip, _ := GetIntranetIP()
	t.Logf("Got ip: %s", ip)
}
