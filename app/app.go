package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitee.com/nee-team/nimble/xlog"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type App struct {
	Domain, Project, Module string
	Modules                 []ModuleName
	Engine                  *gin.Engine
	StartTime               time.Time
}

const HostPort = 80
const HostPath = "/"

var Instance *App

func (a *App) Start() {
	port := viper.GetInt("host.port")
	if port < 1 || port > 65535 {
		port = HostPort
	}
	a.StartOnPort(port)
}

func (a *App) StartOnPort(port int) {
	if port < 1 || port > 65535 {
		panic(fmt.Errorf("can not listen on port %v", port))
	}
	if port < 1 || port > 65535 {
		port = HostPort
	}
	addr := fmt.Sprintf(":%v", port)
	srv := &http.Server{
		Addr:    addr,
		Handler: a.Engine,
	}
	go func() {
		duration := time.Now().Sub(a.StartTime).Nanoseconds() / 1000000
		xlog.Infof("[Rasse] Application Listening and serving HTTP on port %v with modules %q (%dms)\n", port, a.Modules, duration)
		// 服务连接
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	quit := make(chan os.Signal)
	// 等待中断信号以优雅地关闭服务器
	signal.Notify(quit, os.Interrupt)
	<-quit
	xlog.Info("Shutdown Server ...")

	// 设置 5 秒的超时时间，超时后真实执行关闭
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	srv.RegisterOnShutdown(func() {
		xlog.Info("Server exited")
	})
	xlog.Info("Server exiting")
	if err := srv.Shutdown(ctx); err != nil {
		xlog.Fatalf("Server Shutdown: %s", err.Error())
	}
}
