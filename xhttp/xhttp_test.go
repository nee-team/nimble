package xhttp

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xlog"
)

func initForTest() {
	xlog.Init(&xlog.LogSettings{})
	Init("test")
}

func TestNewWithContext(t *testing.T) {
	initForTest()
	var xctx = xcontext.NewTestContext()
	var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var userId = r.Header.Get(xcontext.USERID_KEY)
		var userName = r.Header.Get(xcontext.USERNAME_KEY)
		var user = xctx.GetUserInfo()
		if userId != user.UserId || userName != user.UserName {
			w.WriteHeader(500)
			var msg = fmt.Sprintf("获取用户信息错误！want userID = %s,userName = %s,but userID = %s,userName = %s",
				user.UserId, user.UserName, userId, userName)
			w.Write([]byte(msg))
			return
		}

		var trace = xctx.GetRasseTrace()
		var traceId = r.Header.Get(xcontext.RASSE_TRACE_KEY)
		if string(trace) != traceId {
			w.WriteHeader(500)
			var msg = fmt.Sprintf("获取跟踪信息错误！want TraceID = %s, but TraceID = %s",
				trace, traceId)
			w.Write([]byte(msg))
			return
		}
		var chain = xctx.GetBizTrace()
		var chainID = r.Header.Get(xcontext.BIZ_TRACE_KEY)
		if string(chain) != chainID {
			w.WriteHeader(500)
			var msg = fmt.Sprintf("获取业务跟踪信息错误！want chainID = %s, but chainID = %s",
				chain, chainID)
			w.Write([]byte(msg))

			return
		}

		var langInfo = xctx.GetLang()
		var lang = r.Header.Get(LangHeaderKey)
		if langInfo.Code != lang {
			w.WriteHeader(500)
			var msg = fmt.Sprintf("获取业务跟踪信息错误！want chainID = %s, but chainID = %s",
				langInfo, lang)
			w.Write([]byte(msg))

			return
		}

		var apm = xctx.GetMeshTrace()
		for k, v := range apm {
			var value = r.Header.Get(k)
			if v != value {
				w.WriteHeader(500)
				var msg = fmt.Sprintf("获取链路跟踪信息错误！want key：%s,value:%s, but key:%s,value:%s",
					k, v, k, value)
				w.Write([]byte(msg))
				return
			}
		}
		w.WriteHeader(200)
	})
	server := httptest.NewServer(robotsTxtHandler)
	defer server.Close()

	res, err := New().
		Url(server.URL).WithContext(xctx).Request()
	if err != nil {
		t.Errorf(err.Error())
	}
	if res.StatusCode != 200 {
		t.Error(res.Content())
	}
}

func TestRequestDumpWitchContext(t *testing.T) {
	initForTest()

	var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Res-Header", "res-header-value")
		fmt.Fprintf(w, "res-body")
	})

	server := httptest.NewServer(robotsTxtHandler)
	defer server.Close()

	res, err := New().
		Url(server.URL).
		Method(http.MethodPost).
		AddHeader("Req-Header", "req-header-value").
		AddHeader("Host", "xhttp-test1.oa.com").
		AddHeader("Host", "xhttp-test2.oa.com").
		SetBodyString("req-body").
		Request()
	if err != nil {
		t.Fatal(err.Error())
	}

	if res.Content() != "res-body" ||
		res.StatusCode != http.StatusOK ||
		res.Headers["Res-Header"][0] != "res-header-value" {
		t.Fatal("结果有误")
	}
}

func TestRequest(t *testing.T) {
	initForTest()

	var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Res-Header", "res-header-value")
		fmt.Fprintf(w, "res-body")
	})

	server := httptest.NewServer(robotsTxtHandler)
	defer server.Close()

	res, err := New().
		Url(server.URL).
		Method(http.MethodPost).
		AddHeader("Req-Header", "req-header-value").
		AddHeader("Host", "xhttp-test1.oa.com").
		AddHeader("Host", "xhttp-test2.oa.com").
		SetBodyString("req-body").
		Request()
	if err != nil {
		t.Fatal(err.Error())
	}

	if res.Content() != "res-body" ||
		res.StatusCode != http.StatusOK ||
		res.Headers["Res-Header"][0] != "res-header-value" {
		t.Fatal("结果有误")
	}
}

func TestRequestAsync(t *testing.T) {
	initForTest()

	var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Query().Get("timeout") != "" {
			w.WriteHeader(http.StatusRequestTimeout)
			dr, _ := time.ParseDuration(r.URL.Query().Get("timeout"))
			time.Sleep(dr)
		} else if r.URL.Query().Get("panic") != "" {
			panic("panic from server")
		} else if r.URL.Query().Get("5xx") != "" {
			w.WriteHeader(http.StatusInternalServerError)
		} else if r.URL.Query().Get("4xx") != "" {
			w.WriteHeader(http.StatusBadRequest)
		} else if r.URL.Query().Get("not_found") != "" {
			w.WriteHeader(http.StatusNotFound)
		} else if r.URL.Query().Get("redirect") != "" {
			w.WriteHeader(http.StatusTemporaryRedirect)
		} else {
			w.WriteHeader(http.StatusOK)
		}

		fmt.Fprintf(w, "res-body")
	})

	server := httptest.NewServer(robotsTxtHandler)
	defer server.Close()

	srvurl := server.URL

	// OK
	cbOk := func(r *Res, e error) {
		if e != nil {
			t.Fatal(e.Error())
		}

		if r.Content() != "res-body" ||
			r.StatusCode != http.StatusOK {
			t.Fatal("结果有误")
		}
	}
	New().
		Url(srvurl).
		Method(http.MethodPost).
		RequestAsync(cbOk)

	New().
		Url(srvurl).
		Method(http.MethodPost).
		IgnoreErrorOnFailureStatusCode().
		RequestAsync(cbOk)

	// timeout
	time1 := time.Now()
	cbTimeout := func(r *Res, e error) {
		if e == nil {
			t.Fatal("预期异步请求发生超时错误")
		}

		if r == nil {
			t.Fatal("预期 timeout 请求能返回响应实体-1")
		}

		ms := time.Since(time1).Milliseconds()
		t.Logf("request async timeout at %d", ms)
	}
	New().
		Post(srvurl + "?timeout=600ms").
		Timeout(300).
		RequestAsync(cbTimeout)

	time1 = time.Now()
	New().
		Post(srvurl + "?timeout=600ms").
		Timeout(300).
		IgnoreErrorOnFailureStatusCode().
		RequestAsync(cbTimeout)

	// panic
	//
	cbPanic := func(r *Res, e error) {
		if e == nil {
			t.Fatal("预期异步请求发生 panic 错误")
		}

		if r == nil {
			t.Fatal("预期 panic 请求能返回响应实体")
		}
	}
	New().
		Post(srvurl + "?panic=true").
		RequestAsync(cbPanic)

	New().
		Post(srvurl + "?panic=true").
		IgnoreErrorOnFailureStatusCode().
		RequestAsync(cbPanic)

	// 4xx
	New().
		Post(srvurl + "?4xx=true").
		RequestAsync(func(r *Res, e error) {
			if e == nil {
				t.Fatal("预期异步请求返回 4xx 错误")
			}

			if r == nil {
				t.Fatal("预期 4xx 请求能返回响应实体-1")
			}
		})

	New().
		Post(srvurl + "?4xx=true").
		IgnoreErrorOnFailureStatusCode().
		RequestAsync(func(r *Res, e error) {
			if e != nil {
				t.Fatal("预期异步请求“不”返回 4xx 错误")
			}

			if r == nil {
				t.Fatal("预期 4xx 请求能返回响应实体-2")
			}
		})

		// 404
	New().
		Post(srvurl + "?not_found=404").
		RequestAsync(func(r *Res, e error) {
			if e == nil {
				t.Fatal("预期异步请求返回 404 错误")
			}

			if r == nil {
				t.Fatal("预期 404 请求能返回响应实体-1")
			}
		})

	New().
		Post(srvurl + "?not_found=404").
		IgnoreErrorOnFailureStatusCode().
		RequestAsync(func(r *Res, e error) {
			if e != nil {
				t.Fatal("预期异步请求“不”返回 404 错误")
			}

			if r == nil {
				t.Fatal("预期 404 请求能返回响应实体-2")
			}
		})

	// 5xx
	New().
		Post(srvurl + "?5xx=true").
		RequestAsync(func(r *Res, e error) {
			if e == nil {
				t.Fatal("预期异步请求返回 5xx 错误")
			}

			if r == nil {
				t.Fatal("预期 5xx 请求能返回响应实体-1")
			}
		})

	New().
		Post(srvurl + "?5xx=true").
		IgnoreErrorOnFailureStatusCode().
		RequestAsync(func(r *Res, e error) {
			if e != nil {
				t.Fatal("预期异步请求“不”返回 5xx 错误")
			}

			if r == nil {
				t.Fatal("预期 5xx 请求能返回响应实体-2")
			}
		})
}

//TestRequestHandlePanic 验证 httpAgent.Request() 方法不得抛出 panic，只能通过 error 返回错误
func TestRequestHandlePanic(t *testing.T) {
	initForTest()
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("Panic recovered: %s, it should be error returned", r)
		}
	}()

	_, err := New().
		Url("http://not-exists-url:1234").
		Method(http.MethodGet).
		Request()

	if err == nil {
		t.Error("Expected error not found-1")
	}

	var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("panic from server")
	})

	server := httptest.NewServer(robotsTxtHandler)
	defer server.Close()
	_, err = New().
		Url(server.URL).
		Method(http.MethodGet).
		Request()

	if err == nil {
		t.Error("Expected error not found-2")
	}
}

func TestHTTPAgent_SetRioToken(t *testing.T) {
	initForTest()

	httpAgent := New()
	res, err := httpAgent.
		Url("http://test.bus.rio.oa.com:443/asf_api/test_asf/api/").
		Method(http.MethodGet).
		SetRioToken("39ca78afa42bbcbc7a3ff982933072a0").
		Request()

	if err != nil {
		t.Fatal(err.Error())
	}

	if res.StatusCode != http.StatusOK {
		t.Fatalf("结果有误：%d", res.StatusCode)
	}

	t.Logf("Response: %s", res.Content())
}

func TestHTTPAgent_SetInsecureSkipVerify(t *testing.T) {
	initForTest()

	var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "res-body")
	})
	server := httptest.NewTLSServer(robotsTxtHandler)
	defer server.Close()

	res, err := New().
		Url(server.URL).
		Method(http.MethodGet).
		SetInsecureSkipVerify(true).
		Request()

	if err != nil {
		t.Fatal(err.Error())
	}

	if res.StatusCode != http.StatusOK {
		t.Fatal("结果有误")
	}

	t.Logf("Response: %s", res.Content())
}

func TestHTTPAgent_Certificate(t *testing.T) {
	// initForTest()

	// // TODO：需要构造一个能返回证书错误的 https 接口

	// var robotsTxtHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 	fmt.Fprintf(w, "res-body")
	// })
	// server := httptest.NewTLSServer(robotsTxtHandler)

	// httpAgent := New()
	// res, err := httpAgent.
	// 	Url(server.URL).
	// 	Method(http.MethodPost).
	// 	Request()

	// if err != nil {
	// 	t.Fatal(err.Error())
	// }

	// if res.StatusCode != http.StatusOK {
	// 	t.Fatal("结果有误")
	// }

	// t.Logf("Response: %s", res.Content())

}
