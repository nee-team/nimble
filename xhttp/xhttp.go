// TODO: 完善参数和异常校验；单元测试

package xhttp

import (
	"bytes"
	"crypto/sha256"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"

	"github.com/spf13/viper"

	"gitee.com/nee-team/nimble/extension/xroutine"
	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xlog"

	"github.com/pkg/errors"
)

// DefaultCateName http请求日志Category
const DefaultCateName = "xhttp"

const MaxOkStatus = 399

// LangHeaderKey 从中获取 i18n 语言的HTTP请求头
const LangHeaderKey = "Content-Language"

var logger *xlog.ZapAdapter

// Init 初始化
func Init(cateName string) error {
	logger = xlog.WithCataLog(cateName)
	return nil
}

// HTTPAgent Http 请求帮助类
// 要创建实例，请使用 New() 方法
type HTTPAgent struct {
	method                         string
	URL                            *url.URL
	headers                        *http.Header
	request                        *http.Request
	body                           *bytes.Buffer
	xcontext                       xcontext.XContext
	timeout                        int
	ignoreErrorOnFailureStatusCode bool
	rioToken                       string
	traceID                        string
	insecureSkipVerify             bool
}

// New 创建默认的请求实例
func New() *HTTPAgent {
	return &HTTPAgent{
		method:  http.MethodGet,
		headers: &http.Header{},
		timeout: 100 * int(time.Second),
	}
}

func NewWithContext(ctx xcontext.XContext) *HTTPAgent {
	return New().WithContext(ctx)

}

func (h *HTTPAgent) WithContext(ctx xcontext.XContext) *HTTPAgent {
	h.xcontext = ctx
	return h
}

func (h *HTTPAgent) Method(method string) *HTTPAgent {
	// TODO
	h.method = method
	return h
}

func (h *HTTPAgent) Get(rawurl string) *HTTPAgent {
	return h.Method(http.MethodGet).Url(rawurl)
}

func (h *HTTPAgent) Post(rawurl string) *HTTPAgent {
	return h.Method(http.MethodPost).Url(rawurl)
}

func (h *HTTPAgent) Put(rawurl string) *HTTPAgent {
	return h.Method(http.MethodPut).Url(rawurl)
}

func (h *HTTPAgent) Delete(rawurl string) *HTTPAgent {
	return h.Method(http.MethodDelete).Url(rawurl)
}

func (h *HTTPAgent) Url(rawurl string) *HTTPAgent {
	u, err := url.Parse(rawurl)
	if err != nil {
		panic(err)
	}
	h.URL = u
	return h
}

// ClearQuery 清空 QueryString
func (h *HTTPAgent) ClearQuery(name string) *HTTPAgent {
	h.URL.RawQuery = ""
	return h
}

// AddQuery 追加新的 QueryString
func (h *HTTPAgent) AddQuery(name, value string) *HTTPAgent {
	qs := h.URL.Query()
	qs.Add(name, value)
	h.URL.RawQuery = qs.Encode()
	return h
}

// AddQueries 追加新的 QueryString
func (h *HTTPAgent) AddQueries(queries map[string]string) *HTTPAgent {
	if queries != nil {
		qs := h.URL.Query()
		for k, v := range queries {
			qs.Add(k, v)
		}
		h.URL.RawQuery = qs.Encode()
	}
	return h
}

// SetQuery 设置 QueryString，已存在的将被覆盖，不存在的将会追加上
func (h *HTTPAgent) SetQuery(name, value string) *HTTPAgent {
	qs := h.URL.Query()
	qs.Set(name, value)
	h.URL.RawQuery = qs.Encode()
	return h
}

// SetQueries 设置 QueryString，已存在的将被覆盖，不存在的将会追加上
func (h *HTTPAgent) SetQueries(queries map[string]string) *HTTPAgent {
	if queries != nil {
		qs := h.URL.Query()
		for k, v := range queries {
			qs.Set(k, v)
		}
		h.URL.RawQuery = qs.Encode()
	}
	return h
}

// AddHeader 追加新的 HTTP 头
func (h *HTTPAgent) AddHeader(name, value string) *HTTPAgent {
	h.headers.Add(name, value)
	return h
}

// AddHeaders 追加新的 HTTP 头
func (h *HTTPAgent) AddHeaders(headers map[string]string) *HTTPAgent {
	if headers != nil {
		for k, v := range headers {
			h.headers.Add(k, v)
		}
	}
	return h
}

// SetHeader 设置 HTTP 头，已存在的将被覆盖，不存在的将会追加上
func (h *HTTPAgent) SetHeader(name, value string) *HTTPAgent {
	h.headers.Set(name, value)
	return h
}

// SetHeaders 设置 HTTP 头，已存在的将被覆盖，不存在的将会追加上
func (h *HTTPAgent) SetHeaders(headers map[string]string) *HTTPAgent {
	if headers != nil {
		for k, v := range headers {
			h.headers.Set(k, v)
		}
	}
	return h
}

// SetBodyString 设置请求体
func (h *HTTPAgent) SetBodyString(body string) *HTTPAgent {
	h.body = bytes.NewBufferString(body)
	return h
}

// SetBodyBuffer 设置请求体
func (h *HTTPAgent) SetBodyBuffer(body *bytes.Buffer) *HTTPAgent {
	h.body = body
	return h
}

// SetBodyBytes 设置请求体
func (h *HTTPAgent) SetBodyBytes(body []byte) *HTTPAgent {
	h.body = bytes.NewBuffer(body)
	return h
}

// SetRioToken 设置应用网关 TOKEN，使得请求可以通过应用网关请求
func (h *HTTPAgent) SetRioToken(token string) *HTTPAgent {
	h.rioToken = token
	return h
}

// SetTraceID 设置 HTTP 请求的调用链跟踪 ID，将会被写入 HTTP HEAD 传递给下游接口。
// 请求头名称参见：xhttp.TraceIdKey
func (h *HTTPAgent) SetTraceID(traceID string) *HTTPAgent {
	h.traceID = traceID
	return h
}

// SetInsecureSkipVerify  设置跳过https安全证书
func (h *HTTPAgent) SetInsecureSkipVerify(skip bool) *HTTPAgent {
	h.insecureSkipVerify = skip
	return h
}

// Timeout 设置请求超时时间。单位：毫秒
func (h *HTTPAgent) Timeout(timeoutInMs int) *HTTPAgent {
	h.timeout = timeoutInMs
	return h
}

// validate 校验请求参数等是否合法
func (h *HTTPAgent) validate() error {
	// TODO
	if h.URL == nil {
		return fmt.Errorf("URL required")
	}

	scheme := strings.ToLower(h.URL.Scheme)
	if scheme != "http" && scheme != "https" {
		return fmt.Errorf("Url scheme \"%s\" is not supported", h.URL.Scheme)
	}

	return nil
}

// GetRioSign 获取用于请求应用网关的签名
func GetRioSign(token string) (timestamp string, signature string) {
	timestamp = fmt.Sprintf("%d", time.Now().Unix())
	sn := timestamp + token + timestamp
	signature = fmt.Sprintf("%x", sha256.Sum256([]byte(sn)))
	return timestamp, signature
}

// Request 发起服务请求
//
// 默认行为：
// 	- 当请求执行失败，如通信失败、请求超时、无法读取响应流等，将返回原始的 *net/url.Error 对象。
//	- 当请求执行成功，但响应码大于 399 时，将会返回 *xhttp.Error 对象。
//
// -----------------------------
//
// 摘要逻辑：
//
// golang 和 C# 的 http client 实现有区别：
//	- 在 golang 中，只要服务成功请求到，无论状态码是什么，都不会返回 error。
//	- 在 C# 中，满足 StatusCode <= 299 or (StatusCode<=399 && AllowAutoRedirect==false) 才不会抛出异常，否则会认定为接口错误。
//
// 为了屏蔽以上“开发人员习惯上的”差异，提供 IgnoreErrorOnFailureStatusCode 方法用于决定 error 的行为。也即，调用该方法之后，响应码大于 MaxOkStatus 时不会返回 error。
//
// -----------------------------
//
// 参考：
//
// 在微软 C# HttpWebRequest 类中，根据响应码抛出异常的逻辑为：
//	- > 399 = fatal
//	- some of 3XX and AllowAutoRedirect==true will result into an exceptional respone
//	    ResponseStatusCode==HttpStatusCode.Ambiguous          || // 300
//      ResponseStatusCode==HttpStatusCode.Moved              || // 301
//      ResponseStatusCode==HttpStatusCode.Redirect           || // 302
//      ResponseStatusCode==HttpStatusCode.RedirectMethod     || // 303
//      ResponseStatusCode==HttpStatusCode.RedirectKeepVerb ))   // 307
// 	- SUCCESS Status is <= 299 or (<=399 && AllowAutoRedirect==false) will result into a normal (non exceptional) response
// 	https://referencesource.microsoft.com/#System/net/System/Net/HttpWebRequest.cs,5222
// 	https://referencesource.microsoft.com/#System/net/System/Net/HttpWebRequest.cs,5885
//
// 本模块不支持对 3XX 重定向的自动重新请求，因此不考虑类似微软 C# 逻辑中的 AllowAutoRedirect 设置。
func (h *HTTPAgent) Request() (res *Res, err error) {

	// 请求延时计数
	startTime := time.Now()
	res = &Res{}

	err = h.validate()
	if err != nil {
		return res, err
	}

	var reader io.Reader
	if h.body != nil {
		reader = bytes.NewReader(h.body.Bytes())
	}

	urlstr := StripPassword(h.URL)
	req, err := http.NewRequest(h.method, urlstr, reader)
	if err != nil {
		return res, errors.Wrapf(err, "can not create HttpRequest to: %s", urlstr)
	}

	h.request = req
	if *h.headers != nil {
		for k, v := range *h.headers {
			if strings.ToLower(k) == "host" {
				req.Host = strings.Join(v, " ")
			} else {
				req.Header[k] = v
			}
		}
	}

	if h.xcontext != nil {
		var userInfo = h.xcontext.GetUserInfo()
		if userInfo.UserId != "" {
			req.Header.Set(xcontext.USERID_KEY, userInfo.UserId)
		}
		if userInfo.UserName != "" {

			req.Header.Set(xcontext.USERNAME_KEY, userInfo.UserName)
		}
		req.Header.Set(xcontext.RASSE_TRACE_KEY, string(h.xcontext.GetRasseTrace()))
		req.Header.Set(xcontext.BIZ_TRACE_KEY, string(h.xcontext.GetBizTrace()))
		req.Header.Set(LangHeaderKey, h.xcontext.GetLang().Code)
		var apm = h.xcontext.GetMeshTrace()
		if apm != nil {
			for k, v := range apm {
				req.Header.Set(k, v)
			}
		}
	}

	if len(h.rioToken) > 0 {
		// set api gateway token
		timestamp, signature := GetRioSign(h.rioToken)
		req.Header.Set("timestamp", timestamp)
		req.Header.Set("signature", signature)
	}

	if len(h.traceID) > 0 {
		req.Header.Set(xcontext.BIZ_TRACE_KEY, h.traceID)
	}

	var reqDump []byte
	var resDump []byte

	// dump request
	// must before client.Do(req), or the body will be empty
	func() {
		// 记录请求体
		logDumpReq := viper.GetBool("xhttp.dump.request")
		if logDumpReq && !h.isUploadFile() {
			// 不覆盖外部的 err 变量
			var err error
			reqDump, err = httputil.DumpRequest(h.request, true)
			if err != nil {
				reqDump = []byte(fmt.Sprintf("[xhttp]CAN NOT DUMP REQUEST: %s", err.Error()))
			}
		}
	}()

	client := &http.Client{}
	//如果跳过https 安全证书错误, 并且是https://协议
	if h.insecureSkipVerify && h.URL.Scheme == "https" {
		client.Transport = &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	}
	if h.timeout <= 0 {
		return nil, fmt.Errorf("Http request timeout invalid: %v", h.timeout)
	}
	client.Timeout = time.Duration(h.timeout) * time.Millisecond
	response, err := client.Do(req)

	var latency time.Duration

	// dump response
	// must before defer, or CAN NOT DUMP RESPONSE
	func(r *http.Response) {
		logDumpRes := viper.GetBool("xhttp.dump.response")
		if logDumpRes && r != nil && !isDownloadFile(r) {
			var err error
			resDump, err = httputil.DumpResponse(r, true)
			if err != nil {
				resDump = []byte(fmt.Sprintf("[xhttp]CAN NOT DUMP RESPONSE: %s", err.Error()))
			}
		}
	}(response)

	// 在真实发起http请求之后进行回调
	if RequestCallbackHandler != nil {
		defer func() {
			args := RequestCallbackArgs{
				Req:     req,
				Res:     res,
				Elapsed: latency,
				Err:     err,
			}
			xroutine.SafeRoutine(func(i ...interface{}) { RequestCallbackHandler(i[0].(RequestCallbackArgs)) }, args)
		}()
	}
	defer func(r *http.Response, e error) {
		latency = time.Now().Sub(startTime)
		var statusCode int
		var msg = ""

		if r != nil {
			statusCode = r.StatusCode
		}

		if reqDump == nil {
			reqDump = []byte{}
		}
		if resDump == nil {
			resDump = []byte{}
		}

		latency := time.Now().Sub(startTime)
		var df = map[string]interface{}{
			"method":   h.method,
			"url":      h.URL.String(),
			"trace_id": h.traceID,
			"code":     statusCode,
			"latency":  latency.Milliseconds(),
			"req":      string(reqDump),
			"res":      string(resDump),
		}
		var entry = logger.WithFields(df)
		if h.xcontext != nil {
			entry.WithContext(h.xcontext)
		}

		if e != nil {
			msg = e.Error()
			entry.Error(msg)
		} else {
			log2xx := viper.GetBool("xhttp.log2xx")

			if statusCode > 499 {
				// 5xx
				entry.Error(msg)
			} else if statusCode > 399 {
				// 4xx
				entry.Warn(msg)
			} else if statusCode > 299 || log2xx {
				// 3xx or (2xx && log2xx)
				entry.Info(msg)
			} else {
				// 2xx && !log2xx
				entry.Debug(msg)
			}
		}

		if r != nil {
			r.Body.Close()
		}
	}(response, err)

	if err != nil {
		// 如果发生 panic 或 throw Exception，由于服务端实现的不同，返回的行为是不同的。
		// ---------------------------------------------------------------------
		//          | res | err
		// ---------|-----|-----------------------------------------------------
		//  go      | nil | <*net/url.Error> -> <*errors.errorString> -> "EOF"
		// ---------|-----|-----------------------------------------------------
		//  .net    | 404 | nil
		// ---------|-----|-----------------------------------------------------
		//
		// 为了兼容上述两种情况，避免 xhttp.Request() 方法不返回响应实例的情况，此处针对 go 的 panic 人为构造一些信息进行输出。
		//
		// 补充：在发生超时、重定向时，无论是 go 还是 .net 服务端，这里的结果均会是 res = nil, err = EPO 或 IO Exception。
		if v, ok := err.(*url.Error); ok {
			if v.Timeout() {
				res.StatusCode = http.StatusBadRequest
				err = errors.Wrapf(err, "got timeout from client.Do: %s", urlstr)
			} else if v.Temporary() {
				res.StatusCode = http.StatusTemporaryRedirect
				err = errors.Wrapf(err, "got temporary redirect from client.Do: %s", urlstr)
			} else {
				res.StatusCode = http.StatusInternalServerError
				err = errors.Wrapf(err, "got *url.Error from client.Do: %s", urlstr)
			}
		} else {
			res.StatusCode = http.StatusInternalServerError
			err = errors.Wrapf(err, "got not expected error from client.Do: %s", urlstr)
		}
		return res, err
	}

	// 到这里 client.Do 没有返回 err，那按预期 response 不会为 nil
	res.StatusCode = response.StatusCode
	res.Headers = response.Header
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return res, errors.Wrapf(err, "can not read body from: %s", urlstr)
	}
	res.Body = bytes.NewBuffer(body)

	if !h.ignoreErrorOnFailureStatusCode {
		if response.StatusCode > MaxOkStatus {
			err = &StatusError{statusCode: res.StatusCode}
		}
	}

	return res, err
}

// RequestAsync 发起异步请求
func (h *HTTPAgent) RequestAsync(cb func(*Res, error)) {
	type asyncResult struct {
		res *Res
		err error
	}
	cResult := make(chan asyncResult, 1)
	cPanic := make(chan error, 1)
	xroutine.SafeRoutineChan(cPanic, func(args ...interface{}) {
		res, err := h.Request()
		cResult <- asyncResult{res, err}
	})

	// 多个 case 同时成立时，select 将随机选择
	select {
	case p, hasmore := <-cPanic:
		// 在极端情况下，即使 cResult 中能取到值，也可能因为 cPanic 已经被关闭可能会走到这个case，
		// 因此此处要再次判断 cPanic 是否已经被关闭，如果是，则要继续尝试去取 cResult 中的结果。
		if !hasmore {
			select {
			case rst := <-cResult:
				cb(rst.res, rst.err)
			default:
				panic("unexpected case!")
			}
		} else {
			cb(nil, p)
		}
	case rst := <-cResult:
		cb(rst.res, rst.err)
	}
}

// isUploadFile 是否是上传文件
func (h *HTTPAgent) isUploadFile() bool {
	reqContentType := h.headers.Get("Content-Type")
	return strings.HasPrefix(reqContentType, "multipart/form-data")
}

// isDownloadFile 是否是下载文件
func isDownloadFile(res *http.Response) bool {
	if res == nil {
		return false
	}
	respContentDisposition := res.Header.Get("Content-Disposition")
	return strings.HasPrefix(respContentDisposition, "attachment")
}

//StripPassword strip the password in URL
func StripPassword(u *url.URL) string {
	_, passSet := u.User.Password()
	if passSet {
		return strings.Replace(u.String(), u.User.String()+"@", u.User.Username()+":***@", 1)
	}
	return u.String()
}
