package xhttp

import (
	"bytes"
	"net/http"

	"github.com/pkg/errors"
)

// Res http 请求响应对象
type Res struct {
	StatusCode int
	Body       *bytes.Buffer
	Headers    http.Header
}

// Content 返回 Body 的字符串形式
func (r *Res) Content() string {
	if r == nil || r.Body == nil {
		return ""
	}

	return r.Body.String()
}

// StatusError xhttp 请求成功，但根据状态码从逻辑上认定为失败的错误
type StatusError struct {
	statusCode int
}

func (e *StatusError) Error() string {
	return errors.Errorf("got failure status code: %d", e.statusCode).Error()
}
