package xhttp

import (
	"net/http"
	"time"
)

// RequestCallbackHandler 完成对外 http 请求调用之后的回调。
var RequestCallbackHandler RequestCallback

//RequestCallbackArgs 回调事件对象。
//在回调中修改这些值是不安全的。
type RequestCallbackArgs struct {
	Req     *http.Request
	Res     *Res
	Elapsed time.Duration
	Err     error
}

// RequestCallback 完成对外 http 请求调用之后的回调
type RequestCallback func(args RequestCallbackArgs)
