package xhttp

// IgnoreErrorOnFailureStatusCode 修改 Request() 方法响应码大于 MaxOkStatus 时的默认行为。
//
// 调用该方法后，Request() 方法在响应码大于 MaxOkStatus 时不再返回 error 对象，用户需要自行从 *xhttp.Res 中判断响应码进行处理。
//
// 该方法对服务端 panic、请求超时、服务端重定向的场景无效，仍然会返回 error。
func (h *HTTPAgent) IgnoreErrorOnFailureStatusCode() *HTTPAgent {
	h.ignoreErrorOnFailureStatusCode = true
	return h
}

//ReturnResponseOnFailureStatusCode 已废弃
//Deprecated: 请使用 ReturnErrorByStatusCode
func (h *HTTPAgent) ReturnResponseOnFailureStatusCode() *HTTPAgent {
	h.ignoreErrorOnFailureStatusCode = true
	return h
}
