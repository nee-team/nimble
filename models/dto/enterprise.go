package dto

// Enterprise 企业认证接口参数结构体
type Enterprise struct {
	OpenID  string `json:"open_id"`
	AppFlag string `json:"app_flag"`
}

// EnterpriseInfo 返回企业认证数据
// status 状态 2 离职 1 在职 3 试用期 8 待入职 9 未入职
type EnterpriseInfo struct {
	EngName        string `json:"EngName"`
	ChnName        string `json:"ChnName"`
	DeptIDString   string `json:"DeptIDString"`
	DeptNameString string `json:"DeptNameString"`
	Status         int    `json:"status"`
}
