package dto

// MapAppinfo appid与信息对应表
type MapAppinfo struct {
	Appid  string `json:"appid"`
	Secret string `json:"secret"`
	Token  string `json:"token"`
	Desc   string `json:"desc"`
}
