package dto

// RIOKey rio 返回用户key信息
type RIOKey struct {
	Key     string `json:"key"`
	EngName string `json:"engName"`
	StaffId string `json:"staffID"`
}
