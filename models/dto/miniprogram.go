package dto

type MiniToken struct {
	AccessToken string  `json:"access_token"`
	ExpiresIn   string  `json:"expires_in"`
}

type MiniTicket struct {
	Errcode   int      `json:"errcode"`
	Errmsg    string   `json:"errmsg"`
	Ticket    string   `json:"ticket"`
	ExpiresIn int      `json:"expires_in"`
}

type MiniResponse struct {
	Ticket    string  `json:"ticket"`
	Noncestr  string  `json:"noncestr"`
	Timestamp string  `json:"timestamp"`
	Signature string  `json:"signature"`
	Appid     string  `json:"appid"`
}