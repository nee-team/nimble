package dto

// VerifyResult 腾讯云实名认证接口返回结果
type VerifyResult struct {
	Result        string `json:"result"`
	Description   string `json:"description"`
	RequestId     string `json:"requestid"`
}

// PersonVerifyInfo 用户实名信息相关
type PersonVerifyInfo struct {
	PersonId      string `json:"person_id"`
	Truename      string `json:"truename"`
	Idcard        string `json:"idcard"`
	Mobile        string `json:"mobile"`
	Sex           int    `json:"sex"`
	SubId         string `json:"sub_id"`
	IsValid       int    `json:"is_valid"`
	Code          int    `json:"code"`
}

// SmsResponse 发短信结果
type SmsResponse struct {
	Response struct {
		SendStatusSet []struct {
			SerialNo       string `json:"SerialNo"`
			PhoneNumber    string `json:"PhoneNumber"`
			Fee            int    `json:"Fee"`
			SessionContext string `json:"SessionContext"`
			Code           string `json:"Code"`
			Message        string `json:"Message"`
			IsoCode        string `json:"IsoCode"`
		} `json:"SendStatusSet"`

		RequestID string `json:"RequestId"`
	} `json:"Response"`
}
