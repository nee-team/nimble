package dto

// UserInfo 用户基础信息
type UserInfo struct {
	IdCard           string `json:"idcard"`
	TrueName         string `json:"truename"`
	Mobile           string `json:"mobile"`
	SubId			 string `json:"sub_id"`
	IsPersonVerified int `json:"is_person_verified"`
}
