package dto

// LoginResponse 登录接口返回数据
type LoginResponse struct {
	Ticket     string `json:"ticket"`
	Expire     int32  `json:"expire"`
	SubId      string `json:"sub_id"`
	Enterprise string `json:"enterprise"`
}
