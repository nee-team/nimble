package dto

// VolunteerInfo 批量查询返回的志愿者信息
type VolunteerInfo struct {
	SubID      string `gorm:"sub_id" json:"sub_id"`
	TrueName   string `gorm:"column:truename" json:"truename"`
	Enterprise string `gorm:"column:company_flag" json:"enterprise"`
	BG         int    `gorm:"column:bg_id" json:"bg" hashids:"true"`
	Department int    `gorm:"column:dept_id" json:"department" hashids:"true"`
}

// VolunteersInfo 批量查询返回的志愿者信息
type VolunteersInfo struct {
	SubID      string `gorm:"sub_id" json:"sub_id"`
	TrueName   string `gorm:"column:truename" json:"truename"`
	Enterprise string `gorm:"column:company_flag" json:"enterprise"`
	BG         string `gorm:"column:bg_id" json:"bg" hashids:"true"`
	Department string `gorm:"column:dept_id" json:"department" hashids:"true"`
}
