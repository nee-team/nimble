/*************************************************
**
**
** 框架相关文件，请不要修改此文件
**
** rasse 项目文件现在分为框架文件和业务文件两部分。
**   - 框架文件 尽量不要修改，后续业务如需更新rasse版本，可随时从code平台拉取最新代码合并到各业务仓库即可。
**   - 业务文件 可自行修改，不受限制
**
** 如有特殊需求需要修改此文件，升级rasse时请自行合并框架文件并处理冲突。
** 如果是普遍的需求需要修改此文件，欢迎向(http://git.code.oa.com/fip-team/rasse.git)提交pr或issue
**
**
*************************************************/

package sysapi

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

// Host 获取当前主机名
// +get /__sys/host
func Host() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		host, _ := os.Hostname()
		ctx.String(http.StatusOK, host)
	}
}
