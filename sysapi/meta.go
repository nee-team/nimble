package sysapi

import (
	"net/http"

	"gitee.com/nee-team/nimble/app"
	"github.com/gin-gonic/gin"
)

// +get /__sys/meta
func Meta() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.JSON(
			http.StatusOK,
			&gin.H{
				"app": &gin.H{
					"domain":  app.Instance.Domain,
					"project": app.Instance.Project,
					"module":  app.Instance.Module,
					"url":     "", // self root url
				},
				"rasse": &gin.H{
					"version": &gin.H{
						"tag":         app.RasseTag,
						"commit_id":   app.RasseCommitId,
						"commit_time": app.RasseCommitTime,
						"build_time":  app.RasseBuildTime,
						"go_version":  app.RasseGoVersion,
					}, // rasse 的框架版本
				},
			})
	}
}
