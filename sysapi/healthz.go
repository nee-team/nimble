package sysapi

import (
	"net/http"
	"time"

	"gitee.com/nee-team/nimble/health"
	"github.com/gin-gonic/gin"
)

// Live 无任何逻辑，用于表征服务是否可以被正常调用
// +get /__sys/livez
// +get /__sys/healthz
func Live() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		withNoCache(ctx)
		ctx.String(http.StatusOK, "ok")
	}
}

// Ready 健康状况显示
// +get /__sys/readyz
func Ready() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		withNoCache(ctx)
		isOk, results := health.CheckAll()
		if isOk {
			ctx.JSON(http.StatusOK, results)
		} else {
			ctx.JSON(http.StatusInternalServerError, results)
		}
	}
}

func withNoCache(ctx *gin.Context) {
	ctx.Header("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate, value")
	ctx.Header("Expires", "Thu, 01 Jan 1970 00:00:00 GMT")
	ctx.Header("Last-Modified", time.Now().UTC().Format(http.TimeFormat))
}
