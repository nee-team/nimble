package config

// 配置先于日志模块加载，因此在此处不使用 xlog 模块

// import (
// 	"Nee/rasse/extension/xroutine"
// 	"errors"
// 	"fmt"
// 	"os"
// 	"path"

// 	"Nee/rasse/ip"
// 	"github.com/spf13/viper"
// )

// var (
// 	ErrCallbackNotImp    = errors.New("callback function not implement")
// 	ErrGroupNotExist     = errors.New("rainbow group not exist")
// 	ErrRainbowEnvNotExit = errors.New("rainbow env not exist")
// )

// //配置变更值
// type ConfChangeVal struct {
// 	//新变更的项
// 	NewItems map[string]interface{}
// }

// // Rainbow
// type Rainbow struct {
// 	connectionString string
// 	appID            string
// 	groupName        string
// 	configAPI        *confapi.ConfAPI
// 	configOptions    []types.AssignGetOption
// 	viper            *viper.Viper
// 	OnChange         ConfigChangeFunc
// }

// // NewRainbow 创建七彩石配置实例
// func NewRainbow(connectionString string, appID string, groupName string, userID string, userKey string, OnChange ConfigChangeFunc) (*Rainbow, error) {
// 	fmt.Printf("[Rainbow New] ConnectionString: %s, appId: %s, groupName: %s\n", connectionString, appID, groupName)
// 	if connectionString == "" || appID == "" || groupName == "" {
// 		return nil, ErrRainbowEnvNotExit
// 	}
// 	rainbow := &Rainbow{
// 		connectionString: connectionString,
// 		appID:            appID,
// 		groupName:        groupName,
// 		OnChange:         OnChange,
// 	}
// 	// 初始化配置信息, 默认应用下 rainbow_cache 是缓存目录
// 	initOpts := make([]types.AssignInitOption, 0)
// 	initOpts = append(initOpts, types.ConnectStr(connectionString))
// 	initOpts = append(initOpts, types.AppID(appID))
// 	initOpts = append(initOpts, types.IsUsingLocalCache(true))
// 	initOpts = append(initOpts, types.IsUsingFileCache(true))
// 	initOpts = append(initOpts, types.FileCachePath("./rainbow_cache/"))

// 	logPath := "./logs"
// 	logFile := path.Join(logPath, "rainbow_sdk.log")

// 	// rainbow agent不会自动创建日志目录, 如果目录不存在，提前创建
// 	if _, err := os.Stat(logPath); os.IsNotExist(err) {
// 		os.MkdirAll(logPath, os.ModePerm)
// 	}
// 	initOpts = append(initOpts, types.LogName(logFile))

// 	initOpts = append(initOpts, types.LogLevel(log.LOG_LEVEL_INFO))
// 	// 如果userId和userKey都设置了，那么自动开启签名
// 	if userID != "" && userKey != "" {
// 		initOpts = append(initOpts, types.OpenSign(true))
// 		initOpts = append(initOpts, types.UserID(userID))
// 		initOpts = append(initOpts, types.UserKey(userKey))
// 		initOpts = append(initOpts, types.HmacWay("sha1"))
// 	}
// 	configAPI, err := confapi.New(initOpts...)

// 	if err != nil {
// 		fmt.Errorf("[Rainbow New] %s\n", err.Error())
// 		return nil, err
// 	}

// 	rainbow.configAPI = configAPI

// 	//获取ip地址
// 	lip, err := ip.GetIntranetIP()
// 	if err != nil {
// 		fmt.Errorf(err.Error())
// 		return nil, err
// 	}
// 	getOpts := make([]types.AssignGetOption, 0)
// 	getOpts = append(getOpts, types.WithAppID(appID))
// 	getOpts = append(getOpts, types.WithGroup(groupName))
// 	getOpts = append(getOpts, types.WithIP(lip))

// 	// 添加客户端自定义信息
// 	getOpts = append(getOpts, types.AddClientInfo("framework", "Rasse"))
// 	getOpts = append(getOpts, types.AddClientInfo("type", "Golang"))
// 	//TODO: 是否需要加版本信息

// 	rainbow.configOptions = getOpts
// 	return rainbow, nil
// }

// func (r *Rainbow) Adapter(v *viper.Viper) error {
// 	confMapVal, err := r.configAPI.GetGroup(r.configOptions...)
// 	if err != nil {
// 		fmt.Errorf("[Rainbow.GetGroup]%s\n", err.Error())
// 		return ErrGroupNotExist
// 	}
// 	r.viper = v

// 	cfgmap := map[string]interface{}(confMapVal)
// 	v.MergeConfigMap(cfgmap)

// 	return nil
// }

// // Watch 用来添加配置回调
// func (r Rainbow) Watch() error {
// 	watch := watch.Watcher{
// 		GetOptions: types.GetOptions{
// 			AppID: r.appID,
// 			Group: r.groupName,
// 		},
// 		CB: func(oldVal watch.Result, newVal []*config.KeyValueItem) error {
// 			cfgmap := make(map[string]interface{})
// 			for _, configVal := range newVal {
// 				if configVal.Group == r.groupName {
// 					for _, newVal := range configVal.KeyValues {
// 						fmt.Printf("[Rainbow.Watch] %s: %s\n", newVal.Key, newVal.Value)
// 						cfgmap[newVal.Key] = newVal.Value
// 					}
// 				}
// 			}
// 			if len(cfgmap) > 0 {
// 				r.viper.MergeConfigMap(cfgmap)
// 			}
// 			//触发变更回到
// 			if len(cfgmap) > 0 && r.OnChange != nil {
// 				xroutine.SafeRoutine(func(i ...interface{}) {
// 					changeArgs := ConfChangeVal{
// 						NewItems: cfgmap,
// 					}
// 					r.OnChange(changeArgs)
// 				})
// 			}
// 			return nil
// 		},
// 	}
// 	err := r.configAPI.AddWatcher(watch)
// 	if err != nil {
// 		fmt.Errorf("[Rainbow.Watch] %s\n", err.Error())
// 	}

// 	return nil
// }
