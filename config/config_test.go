package config

import (
	"testing"

	"github.com/spf13/viper"
)

func TestViperGet(t *testing.T) {
	Init()
	if viper.GetInt("host.port") == 80 {
		t.Log("the same host port:80")
	} else {
		t.Error("different from the default host port")
	}
}

func TestRainbow(t *testing.T) {
	Init()
	if viper.GetInt("host.port") == 80 {
		t.Log("the same host port:80")
	} else {
		t.Error("different from the default host port")
	}
}

func TestRainbowChange(t *testing.T) {
	// Init(&RainbowInfo{
	// 	// 设置一个变量，避免因为所有变量都为空而导致未加载Rainbow
	// 	Url:     os.Getenv("RAINBOW_URL"),
	// 	AppId:   os.Getenv("RAINBOW_APPID"),
	// 	Group:   os.Getenv("RAINBOW_GROUP"),
	// 	UserId:  os.Getenv("RAINBOW_USERID"),
	// 	UserKey: os.Getenv("RAINBOW_USERKEY"),
	// 	OnChange: func(changeVal ConfChangeVal) {
	// 		for key, val :=range changeVal.NewItems {
	// 			fmt.Printf("[Rainbow.Change] %s: %s\n", key, val)
	// 		}
	// 	},
	// })
	Init()
	if viper.GetInt("host.port") == 80 {
		t.Log("the same host port:80")
	} else {
		t.Error("different from the default host port")
	}
}
