package config

// 配置先于日志模块加载，因此在此处不使用 xlog 模块

import (
	"github.com/spf13/viper"
)

// Init 初始化配置
// func Init(info *RainbowInfo) error {
func Init() error {
	//默认使用全局viper实例
	v := viper.GetViper()

	//读取环境变量
	useEnv(v)

	//使用几个启动项的默认值
	useDefault(v)

	// 启动Rainbow逻辑, 只要有一个信息填写了就被认为是要使用Rainbow的，就需要验证逻辑
	// isRainbow := info != nil && !(info.Url == "" && info.AppId == "" && info.Group == "" && info.UserId == "" && info.UserKey == "")
	// if isRainbow == true {
	// 	rainbow, err := NewRainbow(info.Url, info.AppId, info.Group, info.UserId, info.UserKey, info.OnChange)
	// 	if err != nil {
	// 		panic(errors.New("[Config Init:Rainbow New] " + err.Error()))
	// 	}
	// 	// Rainbow适配Viper框架
	// 	err = rainbow.Adapter(v)
	// 	if err != nil {
	// 		panic(errors.New("[Config Init:Rainbow Adapter] " + err.Error()))
	// 	}
	// 	//默认开启回调监控
	// 	err = rainbow.Watch()
	// 	if err != nil {
	// 		panic(errors.New("[Config Init:Rainbow Watch] " + err.Error()))
	// 	}
	// } else {
	// 	// 如果Rainbow没有成功开启，扫描和读取本地文件
	// 	useConfigFile(v)
	// }

	useConfigFile(v)
	dynamicConfig(v)
	return nil
}
