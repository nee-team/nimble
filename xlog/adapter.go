package xlog

import (
	"fmt"
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type ZapAdapter struct {
	*LogSettings
	logger *zap.Logger
	sugar  *zap.SugaredLogger
}

func (adapter *ZapAdapter) copyWithCatalog(catalog string) *ZapAdapter {
	a := *adapter
	a.CataLog = catalog
	a.sugar = a.logger.Sugar().Named(catalog)
	return &a
}

func (adapter *ZapAdapter) setMaxFileSize(size int) {
	adapter.MaxFileSize = size
}

func (adapter *ZapAdapter) setMaxBackups(n int) {
	adapter.MaxBackups = n
}

func (adapter *ZapAdapter) setMaxAge(age int) {
	adapter.MaxAge = age
}

func (adapter *ZapAdapter) setCompress(compress bool) {
	adapter.Compress = compress
}

func (adapter *ZapAdapter) setCaller(caller bool) {
	adapter.Caller = caller
}

func NewZapAdapter(path string, level string, catalog string, maxFileSize, maxBackups, maxAge int) *ZapAdapter {
	return &ZapAdapter{
		LogSettings: &LogSettings{
			Path:        path,
			Level:       level,
			CataLog:     catalog,
			MaxFileSize: maxFileSize,
			MaxBackups:  maxBackups,
			MaxAge:      maxAge,
			Compress:    true,
			Caller:      true,
		},
	}
}

// createLumberjackHook 创建LumberjackHook，其作用是为了将日志文件切割，压缩
func (adapter *ZapAdapter) createLumberjackHook() *lumberjack.Logger {
	return &lumberjack.Logger{
		Filename:   adapter.Path,
		MaxSize:    adapter.MaxFileSize,
		MaxBackups: adapter.MaxBackups,
		MaxAge:     adapter.MaxAge,
		Compress:   adapter.Compress,
	}
}

// ParseLevel 解析日志级别
func ParseLevel(lv string) (zapcore.Level, error) {
	var level zapcore.Level
	switch lv {
	case "debug":
		level = zap.DebugLevel
	case "info":
		level = zap.InfoLevel
	case "warn":
		level = zap.WarnLevel
	case "error":
		level = zap.ErrorLevel
	case "dpanic":
		level = zap.DPanicLevel
	case "panic":
		level = zap.PanicLevel
	case "fatal":
		level = zap.FatalLevel
	default:
		return 0, fmt.Errorf("parse log level failed for :%s", lv)
	}
	return level, nil
}

func (adapter *ZapAdapter) Build() {
	w := zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(adapter.createLumberjackHook()))

	level, err := ParseLevel(adapter.Level)
	// 默认使用info级别
	if err != nil {
		level = zapcore.InfoLevel
	}

	conf := zap.NewProductionEncoderConfig()
	conf.EncodeTime = zapcore.ISO8601TimeEncoder
	cnf := zapcore.NewJSONEncoder(conf)
	core := zapcore.NewCore(cnf, w, level)

	adapter.logger = zap.New(core)
	if adapter.Caller {
		adapter.logger = adapter.logger.WithOptions(zap.AddCaller(), zap.AddCallerSkip(2))
	}
	adapter.sugar = adapter.logger.Sugar().Named(adapter.CataLog)
}

func (adapter *ZapAdapter) SetCataLog(catalog string) *zap.SugaredLogger {
	return adapter.sugar.Named(catalog)
}

func (adapter *ZapAdapter) Debug(args ...interface{}) {
	adapter.sugar.Debug(args...)
}

func (adapter *ZapAdapter) Info(args ...interface{}) {
	adapter.sugar.Info(args...)
}

func (adapter *ZapAdapter) Warn(args ...interface{}) {
	adapter.sugar.Warn(args...)
}

func (adapter *ZapAdapter) Error(args ...interface{}) {
	adapter.sugar.Error(args...)
}

func (adapter *ZapAdapter) Panic(args ...interface{}) {
	adapter.sugar.Panic(args...)
}

func (adapter *ZapAdapter) Fatal(args ...interface{}) {
	adapter.sugar.Fatal(args...)
}

func (adapter *ZapAdapter) Debugf(template string, args ...interface{}) {
	adapter.sugar.Debugf(template, args...)
}

func (adapter *ZapAdapter) Infof(template string, args ...interface{}) {
	adapter.sugar.Infof(template, args...)
}

func (adapter *ZapAdapter) Warnf(template string, args ...interface{}) {
	adapter.sugar.Warnf(template, args...)
}

func (adapter *ZapAdapter) Errorf(template string, args ...interface{}) {
	adapter.sugar.Errorf(template, args...)
}

func (adapter *ZapAdapter) Panicf(template string, args ...interface{}) {
	adapter.sugar.Panicf(template, args...)
}

func (adapter *ZapAdapter) Fatalf(template string, args ...interface{}) {
	adapter.sugar.Fatalf(template, args...)
}

func (adapter *ZapAdapter) Debugw(msg string, keysAndValues ...interface{}) {
	adapter.sugar.Debugw(msg, keysAndValues...)
}

func (adapter *ZapAdapter) Infow(msg string, keysAndValues ...interface{}) {
	adapter.sugar.Infow(msg, keysAndValues...)
}

func (adapter *ZapAdapter) Warnw(msg string, keysAndValues ...interface{}) {
	adapter.sugar.Warnw(msg, keysAndValues...)
}

func (adapter *ZapAdapter) Errorw(msg string, keysAndValues ...interface{}) {
	adapter.sugar.Errorw(msg, keysAndValues...)
}

func (adapter *ZapAdapter) Panicw(msg string, keysAndValues ...interface{}) {
	adapter.sugar.Panicw(msg, keysAndValues...)
}

func (adapter *ZapAdapter) Fatalw(msg string, keysAndValues ...interface{}) {
	adapter.sugar.Fatalw(msg, keysAndValues...)
}
