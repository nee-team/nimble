package xlog

import (
	"fmt"

	"gitee.com/nee-team/nimble/xcontext"

	"go.uber.org/zap"
)

type ILogger interface {
	Debug(args ...interface{})
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Panic(args ...interface{})
	Fatal(args ...interface{})
	Debugf(template string, args ...interface{})
	Infof(template string, args ...interface{})
	Warnf(template string, args ...interface{})
	Errorf(template string, args ...interface{})
	Panicf(template string, args ...interface{})
	Fatalf(template string, args ...interface{})
	Debugw(msg string, keysAndValues ...interface{})
	Infow(msg string, keysAndValues ...interface{})
	Warnw(msg string, keysAndValues ...interface{})
	Errorw(msg string, keysAndValues ...interface{})
	Panicw(msg string, keysAndValues ...interface{})
	Fatalw(msg string, keysAndValues ...interface{})
	WithContext(context xcontext.XContext) ILogger
	WithFields(args map[string]interface{}) ILogger
}

const (
	DefaultLevel       = "info"
	DefaultPath        = "./logs"
	DefaultFileName    = "app.log"
	DefaultCataLog     = "app"
	DefaultCaller      = false
	DefaultMaxFileSize = 1024 // 日志文件大小限制默认值，单位MB，超过该大小会自动压缩归档
	DefaultMaxBackups  = 3    // 日志归档文件个数默认值，超过后会滚动清理，仅当MaxBackups与MaxAge均为0时不再自动清理日志文件
	DefaultMaxAge      = 7    // 日志归档文件保留天数默认值，超过后会滚动清理，仅当MaxBackups与MaxAge均为0时不再自动清理日志文件
)

type LogSettings struct {
	FileName    string // 文件名称
	Path        string // 文件绝对地址，如：/home/homework/neso/file.log
	Level       string // 日志输出的级别
	CataLog     string // 日志类别
	MaxFileSize int    // 日志文件大小的最大值，单位(M)
	MaxBackups  int    // 最多保留备份数
	MaxAge      int    // 日志文件保存的时间，单位(天)
	Compress    bool   // 是否压缩
	Caller      bool   // 日志是否需要显示调用位置
}
type xLogger struct {
	*zap.SugaredLogger
}

var logger *ZapAdapter

//==========================logger初始化相关beging===============================
// Init init logger
func Init(settings *LogSettings) error {
	logger = NewZapAdapter(fmt.Sprintf("%s/%s", settings.Path, settings.FileName), settings.Level, settings.CataLog,
		settings.MaxFileSize, settings.MaxBackups, settings.MaxAge)
	logger.setCaller(logger.Caller)
	logger.Build()
	return nil
}

// Sync flushes buffer, if any
func Sync() {
	if logger == nil {
		return
	}
	logger.logger.Sync()
}

// SetCataLog 获取带catalog的日志对象
// Deprecated: addCaller参数现在由配置项log.caller指定,不再支持单独为catalog设置，建议改用WithCataLog方法
func SetCataLog(catalog string, addCaller bool) *ZapAdapter {
	return WithCataLog(catalog)
}

// WithCataLog 获取带catalog的日志对象
func WithCataLog(catalog string) *ZapAdapter {
	if logger == nil {
		return nil
	}
	innerLogger := logger.copyWithCatalog(catalog)
	return innerLogger
}

//==========================logger初始化相关end==================================

//==========================xLogger实现ILogger相关方法being======================
func (log *xLogger) WithFields(args map[string]interface{}) ILogger {
	var fields = make([]interface{}, 0)
	for k, v := range args {
		fields = append(fields, k)
		fields = append(fields, v)
	}
	log.SugaredLogger = log.SugaredLogger.With(fields...)
	return log
}

func (log *xLogger) WithContext(context xcontext.XContext) ILogger {
	args := xcontext.ToMap(context)
	var fields = make([]interface{}, 0)
	for k, v := range args {
		fields = append(fields, k)
		fields = append(fields, v)
	}
	log.SugaredLogger = log.SugaredLogger.With(fields...)
	return log
}

//==========================zapAdapter实现ILogger相关方法being================
func (adapter *ZapAdapter) With(args ...interface{}) ILogger {
	return &xLogger{SugaredLogger: adapter.sugar.With(args...)}
}

func (adapter *ZapAdapter) WithContext(ctx xcontext.XContext) ILogger {
	var fields = getFields(ctx)
	return &xLogger{SugaredLogger: adapter.sugar.With(fields...)}
}

// func (adapter *zapAdapter) WithFields(args ...interface{}) *zap.SugaredLogger {
// 	return adapter.sugar.With(args...)
// }

func (adapter *ZapAdapter) WithFields(args map[string]interface{}) ILogger {
	var fields = make([]interface{}, 0)
	for k, v := range args {
		fields = append(fields, k)
		fields = append(fields, v)
	}
	return &xLogger{SugaredLogger: adapter.sugar.With(fields...)}
}

func getFields(ctx xcontext.XContext) []interface{} {
	args := xcontext.ToMap(ctx)
	var fields = make([]interface{}, 0)
	for k, v := range args {
		fields = append(fields, k)
		fields = append(fields, v)
	}
	return fields
}

//==========================zapAdapter实现ILogger相关方法end================

func WithContext(context xcontext.XContext) ILogger {
	var fields = getFields(context)
	return logger.With(fields...)
}

// Debug 使用方法：log.Debug("test")
func Debug(args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Debug(args...)
}

// Debugf 使用方法：log.Debugf("test:%s", err)
func Debugf(template string, args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Debugf(template, args...)
}

// Debugw 使用方法：log.Debugw("test", "field1", "value1", "field2", "value2")
func Debugw(msg string, keysAndValues ...interface{}) {
	if logger == nil {
		return
	}

	logger.Debugw(msg, keysAndValues...)
}

func Info(args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Info(args...)
}

func Infof(template string, args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Infof(template, args...)
}

func Infow(msg string, keysAndValues ...interface{}) {
	if logger == nil {
		return
	}

	logger.Infow(msg, keysAndValues...)
}

func Warn(args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Warn(args...)
}

func Warnf(template string, args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Warnf(template, args...)
}

func Warnw(msg string, keysAndValues ...interface{}) {
	if logger == nil {
		return
	}

	logger.Warnw(msg, keysAndValues...)
}

func Error(args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Error(args...)
}

func Errorf(template string, args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Errorf(template, args...)
}

func Errorw(msg string, keysAndValues ...interface{}) {
	if logger == nil {
		return
	}

	logger.Errorw(msg, keysAndValues...)
}

func Panic(args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Panic(args...)
}

func Panicf(template string, args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Panicf(template, args...)
}

func Panicw(msg string, keysAndValues ...interface{}) {
	if logger == nil {
		return
	}

	logger.Panicw(msg, keysAndValues...)
}

func Fatal(args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Fatal(args...)
}

func Fatalf(template string, args ...interface{}) {
	if logger == nil {
		return
	}

	logger.Fatalf(template, args...)
}

func Fatalw(msg string, keysAndValues ...interface{}) {
	if logger == nil {
		return
	}

	logger.Fatalw(msg, keysAndValues...)
}
