package xlog

import (
	"testing"

	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/xcontext"
)

func TestWithContext(t *testing.T) {
	var ctx = xcontext.NewTestContext()
	Init(&LogSettings{
		Level:    config.GetStringOrDefault("log.level", DefaultLevel),
		Path:     config.GetStringOrDefault("log.path", DefaultPath),
		FileName: config.GetStringOrDefault("log.filename", DefaultFileName),
		CataLog:  config.GetStringOrDefault("log.catalog", DefaultCataLog),
		Caller:   config.GetBoolOrDefault("log.caller", DefaultCaller),
	})
	logger.WithContext(ctx).Info("hello world")
	WithContext(ctx).Info("hello world2")
	Sync()
}

func TestDebug(t *testing.T) {
	Init(&LogSettings{
		Level:    config.GetStringOrDefault("log.level", DefaultLevel),
		Path:     config.GetStringOrDefault("log.path", DefaultPath),
		FileName: config.GetStringOrDefault("log.filename", DefaultFileName),
		CataLog:  config.GetStringOrDefault("log.catalog", DefaultCataLog),
		Caller:   config.GetBoolOrDefault("log.caller", DefaultCaller),
	})
	Debug("test test test")
	Debugf("test")
	Debugw("test", "a", "b")

	Info("test test test")
	Infof("test")
	Infow("test", "a", "b")

	Warn("test test test")
	Warnf("test")
	Warnw("test", "a", "b")

	Error("test test test")
	Errorf("test")
	Errorw("test", "a", "b")

	Sync()
}

func BenchmarkInfo(b *testing.B) {
	b.ReportAllocs()
	Init(&LogSettings{
		Level:    config.GetStringOrDefault("log.level", DefaultLevel),
		Path:     config.GetStringOrDefault("log.path", DefaultPath),
		FileName: config.GetStringOrDefault("log.filename", DefaultFileName),
		CataLog:  config.GetStringOrDefault("log.catalog", DefaultCataLog),
		Caller:   config.GetBoolOrDefault("log.caller", DefaultCaller),
	})
	for i := 0; i < b.N; i++ {
		Info(`{"level":"debug","ts":"2020-04-14T11:58:19.342+0800","logger":"serve","msg":"","res_body":"","referer":"","client_ip":"::1","method":"GET","req":"","res_headers":"","user_agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4101.0 Safari/537.36 Edg/83.0.474.0","st
ack":"","host":"josonyang-PC5","path":"/api/__sys/version","code":200,"latency":0}`)
	}
}
