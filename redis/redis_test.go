package redis

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"

	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/extension/xstring"

	"github.com/spf13/viper"

	"github.com/gomodule/redigo/redis"
)

func initForTest() Options {
	config.Init()
	options := Options{
		Address:        viper.GetString("redis.address"),
		Db:             viper.GetInt("redis.db"),
		Password:       viper.GetString("redis.password"),
		ConnectTimeout: viper.GetDuration("redis.timeout.connect"),
		ReadTimeout:    viper.GetDuration("redis.timeout.read"),
		WriteTimeout:   viper.GetDuration("redis.timeout.write"),
		IdleTimeout:    viper.GetDuration("redis.timeout.idle"),
		MaxIdle:        viper.GetInt("redis.max.idle"),
		MaxActive:      viper.GetInt("redis.max.active"),
	}

	Init(options)
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))

	return options
}

func Test_Redis(t *testing.T) {
	initForTest()

	//string
	sKey := "test_redis_string_key"
	str := "test_redis_string_value"
	err := SetString(sKey, str, 100)
	if err != nil {
		t.Errorf("SetString error %v", err.Error())
	}
	str2, err := GetString(sKey)
	if err != nil {
		t.Errorf("GetString error %v", err.Error())
	}
	fmt.Printf("GetString ok, get %v", str2)
	if str != str2 {
		t.Errorf("GetString error want %v but get %v", str, str2)
	}

	rep, err := Do("GET", sKey)
	if rep == nil {
		t.Errorf("GetString error want %v but get %v", str, rep)
	}
	rep1, err := redis.String(Do("GET", sKey))
	if rep1 != str {
		t.Errorf("GetString error want %v but get %v", str, rep1)
	}
	//del
	err = Delete(sKey)
	if err != nil {
		t.Errorf("Delete error %v", err.Error())
	}
	delStr, err := GetString(sKey)
	if err == redis.ErrNil {
		fmt.Printf("Delete ok, get nil %v", err.Error())
	} else if err != nil {
		t.Errorf("GetString error %v", err.Error())
	} else {
		if len(delStr) > 0 {
			t.Errorf("Delete error get str %v", delStr)
		}
	}

	// object
	type PersonModel struct {
		Name string
		Age  int64
	}
	type Demo struct {
		ID     int64
		Name   string
		Person PersonModel
	}

	oKey := "test_redis_object_key"
	demo := Demo{
		ID:   rand.Int63(),
		Name: xstring.GetUUID(),
		Person: PersonModel{
			Name: xstring.GetUUID(),
			Age:  rand.Int63(),
		},
	}
	err = SetObject(oKey, demo, 100)
	if err != nil {
		t.Errorf("SetObject error %v", err.Error())
	}
	var rDemo Demo
	err = GetObject(oKey, &rDemo)
	if err != nil {
		t.Errorf("GetObject error %v", err.Error())
	}
	fmt.Printf("GetString ok, get %v", rDemo)
	if rDemo.ID != demo.ID || rDemo.Person.Age != demo.Person.Age || rDemo.Person.Name != demo.Person.Name {
		t.Errorf("GetObject error want %v but get %v", demo, rDemo)
	}

	// Exists
	has, err := Exists(oKey)
	if err != nil {
		t.Errorf(" Exists err %v ", err.Error())
	}
	if !has {
		t.Errorf(" Exists ok, key: %s was %v ", oKey, has)
	}

	// Keys
	var prefix = "test_redis_"
	var pattern = prefix + "*"
	keys, err := Keys(pattern)
	if err != nil {
		t.Errorf(" Keys err %v ", err.Error())
	}
	var retKeys []string
	if keys != nil && len(keys) > 0 {
		for i := 0; i < len(keys); i++ {
			gotKey := string(keys[i].([]byte))
			if !strings.HasPrefix(gotKey, prefix) {
				t.Errorf("got key does not has prefix %s", prefix)
			}
			retKeys = append(retKeys, gotKey)
		}
	} else {
		t.Errorf("should got keys which has prefix %s", prefix)
	}

	// TTL
	ttl, err := TTL(oKey)
	if err != nil {
		t.Errorf(" TTL err %v ", err.Error())
	}
	if ttl <= 0 {
		t.Errorf(" ttl failed, key: %s ttl %v ", oKey, ttl)
	}

	// Incr
	seed := time.Now().Unix()
	SetObject("incr_key", seed, 1000)
	inc, err := Incr("incr_key", 1000)
	inc, err = Incr("incr_key", 1000)
	inc, err = Incr("incr_key", 1000)
	if err != nil {
		t.Errorf(" incr err %v ", err.Error())
	}
	if inc != seed+3 {
		t.Errorf(" incr ok, key: %s inc:%v ", "incr_key", inc)
	}

	// Decr
	SetObject("decr_key", seed, 1000)
	decr, err := Decr("decr_key", 1000)
	decr, err = Decr("decr_key", 1000)
	if err != nil {
		t.Errorf(" decr err %v ", err.Error())
	}
	if decr != seed-2 {
		t.Errorf(" decr ok, key: %s decr:%v ", "decr_key", decr)
	}

	// hash
	hashID := "test_redis_hash_id_" + xstring.GetUUID()
	hKey := "hash_key1_" + xstring.GetUUID()
	hValue := xstring.GetUUID()
	err = HSet(hashID, hKey, hValue, 10000)
	if err != nil {
		t.Errorf(" hset err %v ", err.Error())
	}

	err = HSet(hashID, hKey+"2", hValue+"2", 10000)
	if err != nil {
		t.Errorf(" hset err %v ", err.Error())
	}

	err = HSet(hashID, hKey+"obj", demo, 10000)
	if err != nil {
		t.Errorf(" hsetobj err %v ", err.Error())
	}

	// hget
	hgetVal, err := HGetString(hashID, hKey)
	if err != nil {
		t.Errorf(" hgetstring err %v ", err.Error())
	} else {
		if hgetVal != hValue {
			t.Errorf(" hgetstring ok %v %v", hKey, hgetVal)
		}
	}
	hgetVal, err = HGetString(hashID, hKey+"2")
	if err != nil {
		t.Errorf(" hgetstring err %v ", hKey+"2")
	} else {
		if hgetVal != hValue+"2" {
			t.Errorf(" hgetstring ok %v %v", hKey+"2", hgetVal)
		}
	}

	// hgetobject
	gDemo := Demo{}
	err = HGetObject(hashID, hKey+"obj", &gDemo)
	if err != nil {
		t.Errorf(" hgetobject err %v %v", hKey+"obj", err.Error())
	} else {
		if gDemo.ID != demo.ID || gDemo.Person.Age != demo.Person.Age || gDemo.Person.Name != demo.Person.Name {
			t.Errorf("GetObject error want %v but get %v", demo, gDemo)
		}
	}

	// hmset
	nDemo := Demo{
		ID:   rand.Int63(),
		Name: xstring.GetUUID(),
		Person: PersonModel{
			Name: xstring.GetUUID(),
			Age:  rand.Int63(),
		},
	}
	nDemo2 := Demo{
		ID:   rand.Int63(),
		Name: xstring.GetUUID(),
		Person: PersonModel{
			Name: xstring.GetUUID(),
			Age:  rand.Int63(),
		},
	}
	hmsetVal1 := xstring.GetUUID()
	err = HMSet(hashID, []interface{}{hKey + "_m1", hmsetVal1, hKey + "_m2", nDemo, hKey + "_m3", nDemo2}, 10000)
	if err != nil {
		t.Errorf(" hmset err %v %v", hKey+"_m1", err.Error())
	}

	// hmget
	hmvals, err := HMGet(hashID, hKey+"_m1", hKey+"_m2")
	if err != nil {
		t.Errorf(" hmget err %v %v", hKey+"_m1", err.Error())
	} else if len(hmvals) != 2 {
		t.Error("hmget ok but can not get matched values")
	} else {
		v1, _ := redis.String(hmvals[0], err)
		if v1 != hmsetVal1 {
			t.Errorf("hmget ok expect %v but got %v", hmsetVal1, v1)
		}

		tmp, _ := redis.Bytes(hmvals[1], err)
		var v2 Demo
		json.Unmarshal(tmp, &v2)

		if v2.ID != nDemo.ID || v2.Person.Age != nDemo.Person.Age || v2.Person.Name != nDemo.Person.Name {
			t.Errorf("GetObject error want %v but get %v", nDemo, v2)
		}
	}

	// hkeys
	hkeys, err := HKeys(hashID)
	if err != nil {
		t.Errorf(" HKeys err %v %v", hashID, err.Error())
	} else {
		if len(hkeys) != 6 {
			t.Errorf(" HKeys ok %v %v", hashID, hkeys)
		}
	}

	// hvals
	hvals, err := HVals(hashID)
	if err != nil {
		t.Errorf(" HVals err %v %v", hashID, err.Error())
	} else {
		if len(hvals) != 6 {
			t.Errorf(" HVals ok %v %v", hashID, hvals)
		}
	}

	// hgetall
	hgetall, err := HGetAll(hashID)
	if err != nil {
		t.Errorf(" hgetall err %v %v", hashID, err.Error())
	} else {
		if len(hgetall) != 12 {
			t.Errorf(" hgetall ok %v %v", hashID, hgetall)
		}

		for i := 0; i < 12; i += 2 {
			key, _ := redis.String(hgetall[i], err)
			val, _ := redis.Bytes(hgetall[i+1], err)

			// TODO: 目前只抽查了两个情况
			if key == hKey {
				v1 := string(val)
				if v1 != hValue {
					t.Errorf(" hgetall ok, expect %v but %v", hValue, v1)
				}
			} else if key == hKey+"_m2" {
				var v2 Demo
				json.Unmarshal(val, &v2)

				if v2.ID != nDemo.ID || v2.Person.Age != nDemo.Person.Age || v2.Person.Name != nDemo.Person.Name {
					t.Errorf("GetObject error want %v but get %v", nDemo, v2)
				}
			}
		}
	}

	// hexists
	hexists, err := HExists(hashID, hKey+"_m1")
	if err != nil {
		t.Errorf(" hexists err %v %v", hashID, err.Error())
	} else {
		if !hexists {
			t.Errorf(" hexists ok %v %v", hashID, hexists)
		}
	}

	// Hlen
	hcont, err := HLen(hashID)
	if err != nil {
		t.Errorf(" Hlen err %v %v", hashID, err.Error())
	} else {
		if hcont != 6 {
			t.Errorf(" Hlen ok %v %v", hashID, hcont)
		}
	}

	// HDel
	delCont, err := HDel(hashID, hKey+"_m1", hKey+"_m2")
	if err != nil {
		t.Errorf(" HDel err %v %v", hashID, err.Error())
	} else {
		if delCont != 2 {
			t.Errorf(" HDel ok %v %v", hashID, delCont)
		}
	}
}

func Test_Redis2(t *testing.T) {
	options := initForTest()

	//string
	sKey := "test_redis_string_key_" + xstring.GetUUID()
	str := xstring.GetUUID()
	err := SetString(sKey, str, 100)
	if err != nil {
		t.Errorf("SetString error %v", err.Error())
	}
	str2, err := GetString(sKey)
	if err != nil {
		t.Errorf("GetString error %v", err.Error())
	}
	fmt.Printf("GetString ok, get %v", str2)
	if str != str2 {
		t.Errorf("GetString error want %v but get %v", str, str2)
	}

	redis2 := New(options)
	rep1, err := redis.String(redis2.Do("GET", sKey))
	if rep1 != str2 {
		t.Errorf("GetString error want %v but get %v", rep1, str2)
	}
	if str != str2 {
		t.Errorf("GetString error want %v but get %v", str, str2)
	}
}
