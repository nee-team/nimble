package redis

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"testing"
)

// need cl5
func Test_CKV(t *testing.T) {

	t.Log("WARNING: CVK NOT TESTED.")
	return

	options := Options{
		Address:        "",
		Password:       "",
		Db:             0,
		ConnectTimeout: 30,
		IdleTimeout:    300,
		MaxActive:      20,
		MaxIdle:        1,
		ReadTimeout:    10,
		WriteTimeout:   10,
	}

	Init(options)

	//string
	sKey := "test_tedis_string_key"
	str := "test_tedis_string_value"
	err := SetString(sKey, str, 100)
	if err != nil {
		t.Errorf("SetString error %v", err.Error())
	}
	str2, err := GetString(sKey)
	if err != nil {
		t.Errorf("GetString error %v", err.Error())
	}
	fmt.Printf("GetString ok, get %v", str2)
	if str != str2 {
		t.Errorf("GetString error want %v but get %v", str, str2)
	}

	//del
	err = Delete(sKey)
	if err != nil {
		t.Errorf("Delete error %v", err.Error())
	}
	delStr, err := GetString(sKey)
	if err == redis.ErrNil {
		fmt.Printf("Delete ok, get nil %v", err.Error())
	} else if err != nil {
		t.Errorf("GetString error %v", err.Error())
	} else {
		if len(delStr) > 0 {
			t.Errorf("Delete error get str %v", delStr)
		}
	}

	// object
	type Demo struct {
		ID   int
		Name string
	}
	oKey := "test_tedis_object_key"
	demo := Demo{
		ID:   1001,
		Name: "redis_object_test",
	}
	err = SetObject(oKey, demo, 100)
	if err != nil {
		t.Errorf("SetObject error %v", err.Error())
	}
	var rDemo Demo
	err = GetObject(oKey, &rDemo)
	if err != nil {
		t.Errorf("GetObject error %v", err.Error())
	}
	fmt.Printf("GetString ok, get %v", rDemo)
	if rDemo.ID != demo.ID {
		t.Errorf("GetObject error want %v but get %v", demo, rDemo)
	}
}
