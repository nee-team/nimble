package redis

import (
	"gitee.com/nee-team/nimble/xlog"

	redigo "github.com/gomodule/redigo/redis"
)

// Delete 删除key
func (cache *Cache) Delete(key string) error {
	conn := cache.pool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", key)
	if err != nil {
		xlog.Infof("redis del error: %s %s", key, err.Error())
		return err
	}
	return nil
}

// Exists 是否存在Key
func (cache *Cache) Exists(key string) (bool, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	has, err := redigo.Bool(conn.Do("EXISTS", key))
	if err != nil {
		xlog.Infof("redis exists error: %s %s", key, err.Error())
		return false, err
	}
	return has, nil
}

// Keys 查找Keys
func (cache *Cache) Keys(pattern string) ([]interface{}, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	vals, err := redigo.Values(conn.Do("KEYS", pattern))
	if err != nil {
		xlog.Infof("redis keys error: %s %s", pattern, err.Error())
		return nil, err
	}
	return vals, nil
}

// TTL 查询Key的过期时间，单位：秒。 -2 key不存在， -1 无限期
func (cache *Cache) TTL(key string) (int64, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	ttl, err := redigo.Int64(conn.Do("TTL", key))
	if err != nil {
		xlog.Infof("redis ttl error: %s %s", key, err.Error())
		return -2, err
	}
	return ttl, nil
}
