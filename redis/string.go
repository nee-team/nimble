package redis

import (
	"math"

	"gitee.com/nee-team/nimble/xlog"

	redigo "github.com/gomodule/redigo/redis"
)

func (cache *Cache) GetString(key string) (string, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	str, err := redigo.String(conn.Do("GET", key))
	if err == nil {
		return str, nil
	}
	xlog.Infof("redis get : %s %s", key, err.Error())
	return "", err
}

// SetString 设置字符串Key值, 如果需要持续不过期 expire=math.MaxInt32
func (cache *Cache) SetString(key string, strVal string, expire int) error {
	conn := cache.pool.Get()
	defer conn.Close()

	err := conn.Send("SET", key, strVal)
	if err != nil {
		xlog.Infof("redis set : %s %s", key, err.Error())
		return err
	}
	//expired不等于MaxInt32需要设置过期
	if expire != math.MaxInt32 {
		err = conn.Send("EXPIRE", key, expire)
		if err != nil {
			xlog.Infof("redis set : %s %s", key, err.Error())
			return err
		}
	}
	err = conn.Flush()
	if err != nil {
		xlog.Infof("redis set : %s %s", key, err.Error())
		return err
	}

	return nil
}

// Incr 值加一
func (cache *Cache) Incr(key string, expire int) (int64, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	v, err := redigo.Int64(conn.Do("INCR", key))
	if err != nil {
		xlog.Infof("redis incr : %s %s", key, err.Error())
		return 0, err
	}
	if expire > 0 {
		err = conn.Send("EXPIRE", key, expire)
		if err != nil {
			xlog.Infof("redis incr expire : %s %s", key, err.Error())
			return 0, err
		}
	}

	return v, nil
}

// Decr 值减一
func (cache *Cache) Decr(key string, expire int) (int64, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	v, err := redigo.Int64(conn.Do("DECR", key))
	if err != nil {
		xlog.Infof("redis decr : %s %s", key, err.Error())
		return 0, err
	}
	if expire > 0 {
		err = conn.Send("EXPIRE", key, expire)
		if err != nil {
			xlog.Infof("redis decr expire : %s %s", key, err.Error())
			return 0, err
		}
	}

	return v, nil
}
