package redis

import (
	"encoding/json"

	"gitee.com/nee-team/nimble/xlog"

	redigo "github.com/gomodule/redigo/redis"
)

// HGetString hash获取值
func (cache *Cache) HGetString(hashID string, key string) (string, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	str, err := redigo.String(conn.Do("HGET", hashID, key))
	if err == nil {
		return str, nil
	}
	xlog.Infof("redis hgetstring: %s %s %s", hashID, key, err.Error())
	return "", err
}

// HGetObject hash获取值
func (cache *Cache) HGetObject(hashID string, key string, v interface{}) error {
	conn := cache.pool.Get()
	defer conn.Close()

	reply, err := conn.Do("HGET", hashID, key)
	if err == nil {
		err := json.Unmarshal(reply.([]byte), v)
		if err != nil {
			xlog.Infof("redis hgetobject: %s %s", key, err.Error())
			return err
		}
		return nil
	}
	xlog.Infof("redis hgetobject: %s %s", key, err.Error())
	return err
}

// HSet Hash设置值
func (cache *Cache) HSet(hashID string, key string, val interface{}, expire int) error {
	conn := cache.pool.Get()
	defer conn.Close()

	var valstr string
	switch val.(type) {
	case string:
		valstr = val.(string)
	default:
		mVal, err := json.Marshal(val)
		if err != nil {
			xlog.Infof("redis set error: %s %s", key, err.Error())
			return err
		} else {
			valstr = string(mVal)
		}
	}

	err := conn.Send("HSET", hashID, key, valstr)
	if err != nil {
		xlog.Infof("redis hset: %s %s %s", hashID, key, err.Error())
		return err
	}
	if expire > 0 {
		err = conn.Send("EXPIRE", hashID, expire)
		if err != nil {
			xlog.Infof("redis hset: %s %s %s", hashID, key, err.Error())
			return err
		}
	}
	err = conn.Flush()
	if err != nil {
		xlog.Infof("redis hset: %s %s %s", hashID, key, err.Error())
		return err
	}
	return nil
}

// HMSet 批量设置hash值
func (cache *Cache) HMSet(hashID string, keyVals []interface{}, expire int) error {
	conn := cache.pool.Get()
	defer conn.Close()

	var args = []interface{}{hashID}
	var valstr string
	for i, length := 0, len(keyVals); i < length; i++ {
		if i%2 == 1 {
			switch keyVals[i].(type) {
			case string:
				valstr = keyVals[i].(string)
			default:
				mVal, err := json.Marshal(keyVals[i])
				if err != nil {
					xlog.Infof("redis hmset error: %v %s", keyVals[i], err.Error())
					return err
				} else {
					valstr = string(mVal)
				}
			}
			args = append(args, valstr)
		} else {
			args = append(args, keyVals[i])
		}
	}

	err := conn.Send("HMSET", args...)
	if err != nil {
		xlog.Infof("redis hmset: %s %s", hashID, err.Error())
		return err
	}
	if expire > 0 {
		err = conn.Send("EXPIRE", hashID, expire)
		if err != nil {
			xlog.Infof("redis hmset: %s %s", hashID, err.Error())
			return err
		}
	}
	err = conn.Flush()
	if err != nil {
		xlog.Infof("redis hmset: %s %s", hashID, err.Error())
		return err
	}
	return nil
}

// HMGet 批量获取hash值
func (cache *Cache) HMGet(hashID string, keys []interface{}) ([]interface{}, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	var args = []interface{}{hashID}
	args = append(args, keys...)
	vals, err := conn.Do("HMGET", args...)
	if err == nil {
		return vals.([]interface{}), nil
	}
	xlog.Infof("redis hmget: %s %s", hashID, err.Error())
	return nil, err
}

// HKeys 获取keys
func (cache *Cache) HKeys(hashID string) ([]string, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	vals, err := conn.Do("HKEYS", hashID)
	if err == nil {
		var keys []string
		for i, length := 0, len(vals.([]interface{})); i < length; i++ {
			keys = append(keys, string(vals.([]interface{})[i].([]byte)))
		}
		return keys, nil
	}
	xlog.Infof("redis hkeys: %s %s", hashID, err.Error())
	return nil, err
}

// HVals 获取所有值
func (cache *Cache) HVals(hashID string) ([]interface{}, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	vals, err := conn.Do("HVALS", hashID)
	if err == nil {
		return vals.([]interface{}), nil
	}
	xlog.Infof("redis hvals: %s %s", hashID, err.Error())
	return nil, err
}

// HGetAll 获取所有Keys和所有Values
func (cache *Cache) HGetAll(hashID string) ([]interface{}, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	vals, err := conn.Do("HGETALL", hashID)
	if err == nil {
		return vals.([]interface{}), nil
	}
	xlog.Infof("redis hgetall: %s %s", hashID, err.Error())
	return nil, err
}

// HExists 检查Hash的key是否存在
func (cache *Cache) HExists(hashID string, key string) (bool, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	ex, err := redigo.Bool(conn.Do("HEXISTS", hashID, key))
	if err == nil {
		return ex, nil
	}
	xlog.Infof("redis hexists: %s %s %s", hashID, key, err.Error())
	return false, err
}

// HLen 返回Hash的数量
func (cache *Cache) HLen(hashID string) (int64, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	cnt, err := redigo.Int64(conn.Do("HLEN", hashID))
	if err == nil {
		return cnt, nil
	}
	xlog.Infof("redis hlen: %s %s", hashID, err.Error())
	return 0, err
}

// HDel 删除hash键
func (cache *Cache) HDel(hashID string, keys ...interface{}) (int, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	var args = []interface{}{hashID}
	args = append(args, keys...)
	dc, err := redigo.Int(conn.Do("HDEL", args...))
	if err != nil {
		xlog.Infof("redis hdel: %s %s %s", hashID, keys, err.Error())
		return 0, err
	}
	return dc, nil
}
