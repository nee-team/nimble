package redis

import (
	"encoding/json"
	"strings"

	"gitee.com/nee-team/nimble/xlog"
)

func (cache *Cache) GetObject(key string, v interface{}) error {
	str, err := cache.GetString(key)
	if err == nil {
		err := json.NewDecoder(strings.NewReader(str)).Decode(v)
		if err != nil {
			xlog.Infof("redis get: %s %s", key, err.Error())
			return err
		}
		return nil
	}
	xlog.Infof("redis get: %s %s", key, err.Error())
	return err
}

func (cache *Cache) SetObject(key string, v interface{}, expire int) error {
	if arrJson, err := json.Marshal(v); err == nil {
		strJson := string(arrJson)
		err = cache.SetString(key, strJson, expire)
		if err != nil {
			xlog.Infof("redis set: %s %s", key, err.Error())
		}
		return err
	} else {
		xlog.Infof("redis set: %s %s", key, err.Error())
		return err
	}
}
