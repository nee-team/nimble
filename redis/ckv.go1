package redis

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"git.code.oa.com/components/l5"
	redigo "github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
)

var once sync.Once
var l5Api *l5.Api

func initl5Api() *l5.Api {
	once.Do(func() {
		//just init once
		l5api, err := l5.NewDefaultApi()
		if err != nil {
			l5Api = nil
			panic(err)
		}
		l5Api = l5api
	})
	return l5Api
}

func NewCKV(options Options) *Cache {
	// 初始化
	options.Assign()
	l5api := initl5Api()

	// 建立连接池
	redisClient := &redigo.Pool{
		MaxIdle:     options.MaxIdle,
		MaxActive:   options.MaxActive,
		IdleTimeout: options.IdleTimeout * time.Second,
		Wait:        true,
		Dial: func() (redigo.Conn, error) {

			// parse mod:cmd
			var mod int32
			var cmd int32
			{
				arr := strings.Split(options.Address, ":")
				if len(arr) == 2 {
					if tmp, err := strconv.Atoi(arr[0]); err == nil {
						mod = int32(tmp)
					}
					if tmp, err := strconv.Atoi(arr[1]); err == nil {
						cmd = int32(tmp)
					}
				}
			}

			if !(mod > 0 && cmd > 0) {
				return nil, errors.New("can not get MOD:CMD of L5 for ckv")
			}

			srv, err := l5api.GetServerBySid(mod, cmd)
			if err != nil {
				return nil, err
			}
			source := fmt.Sprintf("%s:%d", srv.Ip(), srv.Port())
			con, err := redigo.Dial("tcp", source,
				redigo.DialPassword(options.Password),
				redigo.DialDatabase(options.Db),
				redigo.DialConnectTimeout(options.ConnectTimeout*time.Second),
				redigo.DialReadTimeout(options.ReadTimeout*time.Second),
				redigo.DialWriteTimeout(options.WriteTimeout*time.Second))
			if err != nil {
				return nil, err
			}
			return con, nil
		},
		TestOnBorrow: func(c redigo.Conn, t time.Time) error {
			// less than IdleTimeout is ok
			if time.Since(t) < options.IdleTimeout*time.Second {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}

	c := &Cache{pool: redisClient, address: options.Address}

	// 添加到检测器
	checkTargets = append(checkTargets, c)

	return c
}
