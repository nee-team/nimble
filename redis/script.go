package redis

import (
	"gitee.com/nee-team/nimble/xlog"

	redigo "github.com/gomodule/redigo/redis"
)

// redis 脚本
type Script struct {
	redigoScript *redigo.Script
}

// NewScript 创建redis脚本
func NewScript(keyCount int, src string) *Script {
	return &Script{
		redigoScript: redigo.NewScript(keyCount, src),
	}
}

func (script *Script) Do(cache *Cache, keysAndArgs ...interface{}) (interface{}, error) {
	conn := cache.pool.Get()
	defer conn.Close()

	ret, err := script.redigoScript.Do(conn, keysAndArgs...)
	if err == nil {
		return ret, nil
	}
	xlog.Errorf("redis scriptdo: %s", err.Error())
	return "", err
}
