package redis

import (
	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/health"
)

// checkTargets 健康检查器的检查目标
var checkTargets = make([]*Cache, 0)

// Checker Redis 健康检查器
type Checker struct{}

// GetName 获取名称
func (c *Checker) GetName() string {
	return "redis"
}

// Enabled 是否启用
func (c *Checker) Enabled() bool {
	return config.GetBoolOrDefault("health.redis.check", true)
}

// Check 检查可用状态
func (c *Checker) Check() (bool, map[string]string, string) {
	// 由于 slice 非并发安全，因此复制到临时集合，避免使用多实例时冲突
	checking := make([]*Cache, len(checkTargets))
	copy(checking, checkTargets)

	for _, c := range checking {
		if c == nil || c.pool == nil {
			continue
		}

		conn := c.pool.Get()
		defer conn.Close()

		err := conn.Err()
		if err != nil {
			return false, map[string]string{"address": c.address}, err.Error()
		}
	}

	return true, nil, ""
}

// 作为内置检查项注册
func init() {
	health.Register(&Checker{})
}
