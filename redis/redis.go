package redis

import (
	"regexp"
	"time"

	redigo "github.com/gomodule/redigo/redis"
)

type Cache struct {
	pool    *redigo.Pool
	address string
}

type Options struct {
	// host:port 形式的IP端口地址，或者 MOD:CMD 形式的 L5 地址
	Address        string
	Db             int
	Password       string
	ConnectTimeout time.Duration
	ReadTimeout    time.Duration
	WriteTimeout   time.Duration
	IdleTimeout    time.Duration
	MaxIdle        int
	MaxActive      int
}

// 提供 redis 包的默认实例
// 若需要连接多个 redis，可自行调用 New() 方法创建
var stdCache *Cache

var defaultOptions = Options{
	Db:             0,
	Password:       "",
	ConnectTimeout: 30,
	IdleTimeout:    300,
	MaxActive:      20,
	MaxIdle:        10,
	ReadTimeout:    10,
	WriteTimeout:   10,
}

func Init(options Options) error {
	re, _ := regexp.Compile(`^\d+\:\d+$`)
	if re.MatchString(options.Address) {

	} else {
		stdCache = New(options)
	}
	return nil
}

func New(options Options) *Cache {
	// 初始化
	options.Assign()
	// 建立连接池
	redigoClient := &redigo.Pool{
		MaxIdle:     options.MaxIdle,
		MaxActive:   options.MaxActive,
		IdleTimeout: options.IdleTimeout * time.Second,
		Wait:        true,
		Dial: func() (redigo.Conn, error) {
			// pool.get() call this function if not available connection
			con, err := redigo.Dial("tcp",
				options.Address,
				redigo.DialPassword(options.Password),
				redigo.DialDatabase(options.Db),
				redigo.DialConnectTimeout(options.ConnectTimeout*time.Second),
				redigo.DialReadTimeout(options.ReadTimeout*time.Second),
				redigo.DialWriteTimeout(options.WriteTimeout*time.Second))
			if err != nil {
				return nil, err
			}
			return con, nil
		},
		TestOnBorrow: func(c redigo.Conn, t time.Time) error {
			// less than IdleTimeout is ok
			if time.Since(t) < options.IdleTimeout*time.Second {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}

	c := &Cache{pool: redigoClient, address: options.Address}

	// 添加到检测器
	checkTargets = append(checkTargets, c)

	return c
}

func (options *Options) Assign() {
	if options.Db <= 0 {
		options.Db = defaultOptions.Db
	}
	if options.ConnectTimeout == 0 {
		options.ConnectTimeout = defaultOptions.ConnectTimeout
	}
	if options.IdleTimeout == 0 {
		options.IdleTimeout = defaultOptions.IdleTimeout
	}
	if options.MaxActive == 0 {
		options.MaxActive = defaultOptions.MaxActive
	}
	if options.MaxIdle == 0 {
		options.MaxIdle = defaultOptions.MaxIdle
	}
	if options.ReadTimeout == 0 {
		options.ReadTimeout = defaultOptions.ReadTimeout
	}
	if options.WriteTimeout == 0 {
		options.WriteTimeout = defaultOptions.WriteTimeout
	}
}
func (cache *Cache) Do(commandName string, args ...interface{}) (reply interface{}, err error) {
	conn := cache.pool.Get()
	defer conn.Close()

	return conn.Do(commandName, args...)
}

// Send writes the command to the client's output buffer.
func (cache *Cache) Send(commandName string, args ...interface{}) error {
	conn := cache.pool.Get()
	defer conn.Close()

	return conn.Send(commandName, args...)
}

// Flush flushes the output buffer to the redigo server.
func (cache *Cache) Flush() error {
	conn := cache.pool.Get()
	defer conn.Close()

	return conn.Flush()
}

// Receive receives a single reply from the redigo server
func (cache *Cache) Receive() (reply interface{}, err error) {
	conn := cache.pool.Get()
	defer conn.Close()

	return conn.Receive()
}
