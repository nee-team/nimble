package redis

// GetStdCache 获取默认的redis缓存实例
func GetStdCache() *Cache {
	return stdCache
}

func GetObject(key string, v interface{}) error {
	return stdCache.GetObject(key, v)
}

func SetObject(key string, v interface{}, expire int) error {
	return stdCache.SetObject(key, v, expire)
}

func GetString(key string) (string, error) {
	return stdCache.GetString(key)
}

func SetString(key string, strVal string, expire int) error {
	return stdCache.SetString(key, strVal, expire)
}

func Delete(key string) error {
	return stdCache.Delete(key)
}

func Exists(key string) (bool, error) {
	return stdCache.Exists(key)
}

func Keys(pattern string) ([]interface{}, error) {
	return stdCache.Keys(pattern)
}

func TTL(key string) (int64, error) {
	return stdCache.TTL(key)
}

func Incr(key string, expire int) (int64, error) {
	return stdCache.Incr(key, expire)
}

func Decr(key string, expire int) (int64, error) {
	return stdCache.Decr(key, expire)
}

func Do(commandName string, args ...interface{}) (reply interface{}, err error) {
	return stdCache.Do(commandName, args...)
}

// Send writes the command to the client's output buffer.
func Send(commandName string, args ...interface{}) error {
	return stdCache.Send(commandName, args...)
}

// Flush flushes the output buffer to the Redis server.
func Flush() error {
	return stdCache.Flush()
}

// Receive receives a single reply from the Redis server
func Receive() (reply interface{}, err error) {
	return stdCache.Receive()
}

// Hash 操作
// HGetString hash获取值
func HGetString(hashID string, key string) (string, error) {
	return stdCache.HGetString(hashID, key)
}

// HGetObject hash获取值
func HGetObject(hashID string, key string, v interface{}) error {
	return stdCache.HGetObject(hashID, key, v)
}

// HSet Hash设置值
func HSet(hashID string, key string, val interface{}, expire int) error {
	return stdCache.HSet(hashID, key, val, expire)
}

// HMSet 批量设置hash值
func HMSet(hashID string, keyVals []interface{}, expire int) error {
	return stdCache.HMSet(hashID, keyVals, expire)
}

// HMGet 批量获取hash值
func HMGet(hashID string, keys ...interface{}) ([]interface{}, error) {
	return stdCache.HMGet(hashID, keys)
}

// HKeys 获取keys
func HKeys(hashID string) ([]string, error) {
	return stdCache.HKeys(hashID)
}

// HVals 获取所有值
func HVals(hashID string) ([]interface{}, error) {
	return stdCache.HVals(hashID)
}

// HGetAll 获取所有Keys和所有Values
func HGetAll(hashID string) ([]interface{}, error) {
	return stdCache.HGetAll(hashID)
}

// HExists 检查Hash的key是否存在
func HExists(hashID string, key string) (bool, error) {
	return stdCache.HExists(hashID, key)
}

// HLen 返回Hash的数量
func HLen(hashID string) (int64, error) {
	return stdCache.HLen(hashID)
}

// HDel 删除hash键
func HDel(hashID string, keys ...interface{}) (int, error) {
	return stdCache.HDel(hashID, keys...)
}
