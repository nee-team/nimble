package xcontext

import (
	"github.com/google/uuid"
)

// NewTestContext 构造测试XContext
func NewTestContext() XContext {
	ctx := NewEmptyContext()
	ctx.Trace = RasseTrace(uuid.New().String())
	ctx.Biz = BizTrace(uuid.New().String())
	ctx.User = UserInfo{
		UserId:   uuid.New().String(),
		UserName: uuid.New().String(),
	}
	apm := make(map[string]string)
	for _, v := range DEFAULT_MESH_TRACE_KEYS {
		apm[v] = uuid.New().String()
	}
	ctx.Mesh = apm
	ctx.Lang = LangInfo{Code: DEFAULT_LANG}
	return ctx
}
