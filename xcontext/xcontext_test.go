package xcontext

import (
	"testing"
)

func TestToMap(t *testing.T) {
	var ctx = NewTestContext()
	var fields = ToMap(ctx)
	var trace = ctx.GetRasseTrace()
	if value, ok := fields[RASSE_TRACE_KEY]; ok {
		if value != string(trace) {
			t.Errorf("跟踪信息数据不一致！want:%s,but:%s", trace, value)
		}
	} else {
		t.Errorf("跟踪信息不存在！")
	}
	var chainId = ctx.GetBizTrace()
	if value, ok := fields[BIZ_TRACE_KEY]; ok {
		if value != string(chainId) {
			t.Errorf("业务跟踪信息数据不一致！want:%s,but:%s", chainId, value)
		}
	} else {
		t.Errorf("业务跟踪信息不存在！")
	}
	var user = ctx.GetUserInfo()
	if value, ok := fields[USERID_KEY]; ok {
		if value != user.UserId {
			t.Errorf("用户信息ID数据不一致！ want:%s,but %s", user.UserId, value)
		}
	} else {
		t.Errorf("用户信息ID信息不存在！")
	}

	if value, ok := fields[USERNAME_KEY]; ok {
		if value != user.UserName {
			t.Errorf("用户信息名称数据不一致！ want：%s,but %s", user.UserName, value)
		}
	} else {
		t.Errorf("用户信息名称信息不存在！")
	}

	var apm = ctx.GetMeshTrace()
	for k, v := range apm {
		if value, ok := fields[k]; ok {
			if value != v {
				t.Errorf("链路跟踪信息%s数据不一致！ want:%s,but %s", k, v, value)
			}
		} else {
			t.Errorf("链路跟踪信息%s信息不存在！", k)
		}
	}

}

func TestEmptyContextToMap(t *testing.T) {
	xctx := NewEmptyContext()
	ToMap(xctx)
}
