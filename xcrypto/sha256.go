package xcrypto

import (
	"crypto/sha256"
	"encoding/hex"
)

// Sha256 计算 sha256 加密
func Sha256(str string) string {
	h := sha256.New()
	h.Write([]byte(str))
	bs := h.Sum(nil)
	encodedStr := hex.EncodeToString(bs)
	return encodedStr
}
