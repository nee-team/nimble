package xtime

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/guregu/null"
	"github.com/spf13/cast"
)

// FromMicrosoftTimeJson 微软表示的时间(如/Date(1598234774000+0800)/)转time
func FromMicrosoftTimeJson(dt string) null.Time {
	if dt == "" {
		return null.Time{}
	}
	dt = strings.TrimPrefix(dt, `/Date(`)
	dt = strings.TrimSuffix(dt, `+0800)/`)
	iDt, err := strconv.Atoi(dt)
	if nil != err {
		return null.Time{}
	}
	return null.TimeFrom(cast.ToTime(iDt / 1000))
}

// ToMicrosoftTime 转微软表示的时间(如/Date(1598234774000+0800)/)
func ToMicrosoftTimeJson(t null.Time) string {
	if t.IsZero() {
		return ""
	} else {
		return fmt.Sprintf("/Date(%d+0800)/", t.Time.Unix()*1000)
	}
}
