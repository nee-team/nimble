package collection

import (
	"testing"
)

func TestContainsWhere(t *testing.T) {
	type item struct {
		Key   string `json:"key"`
		Value string `json:"value"`
	}

	type args struct {
		collection interface{}
		compare    func(interface{}) bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "simple slice - 1",
			args: args{
				collection: []int{1, 2, 3, 4},
				compare: func(obj interface{}) bool {
					return obj == 1
				},
			}, want: true,
		}, {
			name: "simple slice - 2",
			args: args{
				collection: []int{1, 2, 3, 4},
				compare: func(obj interface{}) bool {
					return obj == 5
				},
			}, want: false,
		}, {
			name: "complex slice - 1",
			args: args{
				collection: []item{{Key: "key1", Value: "value1"}, {Key: "key2", Value: "value2"}},
				compare: func(obj interface{}) bool {
					tmp, ok := obj.(item)
					return ok && tmp.Key == "key1"
				},
			}, want: true,
		},
		{
			name: "complex slice - 2",
			args: args{
				collection: []item{{Key: "key1", Value: "value1"}, {Key: "key2", Value: "value2"}},
				compare: func(obj interface{}) bool {
					tmp, ok := obj.(item)
					return ok && tmp.Key == "key3"
				},
			}, want: false,
		},
		{
			name: "complex slice - 3",
			args: args{
				collection: []map[string]string{{"key1": "key1", "value1": "value1"}, {"key2": "key2", "value2": "value2"}},
				compare: func(obj interface{}) bool {
					tmp, ok := obj.(map[string]string)
					return ok && tmp["key1"] == "key1" && tmp["value1"] == "value1"
				},
			}, want: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContainsWhere(tt.args.collection, tt.args.compare); got != tt.want {
				t.Errorf("ContainsWhere() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContains(t *testing.T) {
	type args struct {
		collection interface{}
		obj        interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
		{
			name: "simple slice - 1",
			args: args{[4]int{1, 2, 3, 4}, 1}, want: true,
		}, {
			name: "simple slice - 2",
			args: args{[]int{1, 2, 3, 4}, 5}, want: false,
		},
		{
			name: "complex slice - 1",
			args: args{
				map[string]string{"key1": "value1", "key2": "value2"},
				"key1",
			}, want: true,
		},
		{
			name: "complex slice - 2",
			args: args{
				map[string]string{"existskey": "value1", "key2": "value2"},
				"existskey",
			}, want: true,
		},
		{
			name: "complex slice - 3",
			args: args{
				map[string]string{"key1": "value1", "key2": "value2"},
				"notexistskey",
			}, want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.collection, tt.args.obj); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}
