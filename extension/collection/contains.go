package collection

import (
	"fmt"
	"reflect"
)

//ContainsWhere 根据比较函数判断obj是否在collection中，collection支持的类型 array,slice,map
func ContainsWhere(collection interface{}, isMatch func(element interface{}) bool) bool {
	targetValue := reflect.ValueOf(collection)
	fmt.Println(targetValue.Kind())
	var _type = reflect.TypeOf(collection).Kind()
	switch _type {
	case reflect.Slice, reflect.Array, reflect.Map:
		for i := 0; i < targetValue.Len(); i++ {
			if isMatch(targetValue.Index(i).Interface()) {
				return true
			}
		}
	}

	return false
}

//Contains 判断obj是否在collection中，collection支持的类型 array,slice,map. collection 为 map 时将根据 key 进行匹配。
func Contains(collection interface{}, target interface{}) bool {
	targetValue := reflect.ValueOf(collection)
	var _type = reflect.TypeOf(collection).Kind()
	switch _type {
	case reflect.Slice, reflect.Array:
		for i := 0; i < targetValue.Len(); i++ {
			if targetValue.Index(i).Interface() == target {
				return true
			}
		}
	case reflect.Map:
		if targetValue.MapIndex(reflect.ValueOf(target)).IsValid() {
			return true
		}
	}

	return false
}

func FirstOrNil(collection interface{}, isMatch func(element interface{}) bool) interface{} {
	targetValue := reflect.ValueOf(collection)
	switch reflect.TypeOf(collection).Kind() {
	case reflect.Slice, reflect.Array, reflect.Map:
		for i := 0; i < targetValue.Len(); i++ {
			if isMatch(targetValue.Index(i).Interface()) {
				return targetValue.Index(i).Interface()
			}
		}
	}

	return nil
}
