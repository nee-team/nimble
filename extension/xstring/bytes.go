package xstring

import "unsafe"

/*
@version v1.0
@author charleli
@copyright Copyright (c) 2018 Tencent Corporation, All Rights Reserved
@license http://opensource.org/licenses/gpl-license.php GNU Public License

You may not use this file except in compliance with the License.

Most recent version can be found at:
http://git.code.oa.com/going_proj/going_proj

Please see README.md for more information.
*/

// Bytes2Str byte数组直接转成string对象，不发生内存copy, benchmark比常规转换性能提升数倍，适合[]byte只读的情况
func Bytes2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
