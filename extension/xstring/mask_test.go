package xstring

import "testing"

func TestMaskText(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"no-mask", args{"1301234"}, "1301234"},
		{"phone", args{"13000001234"}, "130****1234"},
		{"id_no", args{"510000000000000000"}, "510***********0000"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MaskText(tt.args.str); got != tt.want {
				t.Errorf("MaskText() = %v, want %v", got, tt.want)
			}
		})
	}
}
