package xpath

import (
	"os"
	"os/user"
)

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// RootDir 应用所在根目录
func RootDir() string {
	wd, _ := os.Getwd()
	return wd
}

// HomeDir 当前用户的 $HOME 目录
func HomeDir() string {
	usr, err := user.Current()
	if err != nil {
		return "~"
	}
	return usr.HomeDir
}
