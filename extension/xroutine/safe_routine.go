package xroutine

import (
	"errors"
	"fmt"
	"runtime/debug"

	"gitee.com/nee-team/nimble/xlog"
)

// SafeRoutine 调用 recover() 安全执行 goroutine，避免内部 panic 导致进程崩溃。若内部发生 panic，则会打印到日志中。
//
// 注意：该方法对 log.Fatal 系列方法无效！
//
// - `f` goroutine 中要执行的函数（建议不要在该函数中使用上下文参数，而通过 `args` 传入，避免闭包问题）
// - `args` goroutine 中要传入的参数
func SafeRoutine(f func(...interface{}), args ...interface{}) {
	SafeRoutineChan(nil, f, args...)
}

// SafeRoutineChan 调用 recover() 安全执行 goroutine，避免内部 panic 导致进程崩溃。
// 若内部发生 panic，则会写入 `chan` ，随后关闭该通道。
//
// 执行完毕未发生 panic 也会关闭通道。
//
// 注意：该方法对 log.Fatal 系列方法无效！
//
// - `c` 要获取内部错误的通道
//
// - `f` goroutine 中要执行的函数（建议不要在该函数中使用上下文参数，而通过 `args` 传入，避免闭包问题）
//
// - `args` goroutine 中要传入的参数
func SafeRoutineChan(c chan<- error, f func(...interface{}), args ...interface{}) {
	go func(params ...interface{}) {
		defer func() {
			// routine finished
			if r := recover(); r != nil {
				if c == nil {
					xlog.Errorf("Recovered in SafeRoutineChan: %v: %s\n", r, debug.Stack())
					return
				}
				// find out exactly what the error was and set err
				var err error
				switch x := r.(type) {
				case string:
					err = errors.New(x)
				case error:
					err = x
				default:
					// Fallback err (per specs, error strings should be lowercase w/o punctuation
					err = fmt.Errorf("unknown error from SafeRoutineChan: %v", x)
				}

				xlog.Errorf("stacktrace from SafeRoutineChan: \n" + string(debug.Stack()))
				c <- err
			} else {
				if c != nil {
					close(c)
				}
			}
		}()

		f(params...)
	}(args...)
}
