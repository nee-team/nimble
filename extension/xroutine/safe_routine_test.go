package xroutine

import (
	"fmt"

	"reflect"
	"testing"
)

func TestSafeRoutine(t *testing.T) {
	type args struct {
		f    func(...interface{})
		args []interface{}
	}
	tests := []struct {
		name     string
		args     args
		expected interface{}
	}{
		// TODO: Add test cases.
		{"Empty", args{f: func(...interface{}) {}, args: []interface{}{}}, nil},
		{"SingleArg", args{f: func(args ...interface{}) {
			if len(args) != 1 ||
				reflect.TypeOf(args[0]).Name() != "int" ||
				args[0].(int) != 1 {
				panic("args pass error")
			}
		}, args: []interface{}{1}}, nil},
		{"MultiArg", args{f: func(args ...interface{}) {
			if len(args) != 2 ||
				reflect.TypeOf(args[0]).Name() != "int" ||
				reflect.TypeOf(args[1]).Name() != "string" ||
				args[0].(int) != 1 ||
				args[1].(string) != "2" {
				panic("args pass error")
			}
		}, args: []interface{}{1, "2"}}, nil},
		{"NoPanic", args{f: func(params ...interface{}) { fmt.Printf("no panic: %s\n", params...) }, args: []interface{}{"a", "b"}}, nil},
		{"Safe", args{f: func(...interface{}) { panic("got string") }, args: []interface{}{}}, "got string"},
		{"SafeError", args{f: func(...interface{}) { panic(fmt.Errorf("got error")) }, args: []interface{}{}}, "got error"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := make(chan error, 1)
			SafeRoutineChan(c, tt.args.f, tt.args.args...)
			obj := <-c

			if obj != nil && obj != tt.expected && obj.Error() != tt.expected {
				t.Errorf("%s expected:%v,get:%v", tt.name, tt.expected, obj.Error())
			}
		})
	}
}
