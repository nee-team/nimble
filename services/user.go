package services

import (
	"encoding/json"
	"errors"
	"strings"

	"gitee.com/nee-team/nimble/models/dto"
	"gitee.com/nee-team/nimble/models/mo"
	"gitee.com/nee-team/nimble/orm"
	"gitee.com/nee-team/nimble/redis"
	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xlog"

	"github.com/spf13/viper"
)

// GetUserInfoService 获取用户信息
type GetUserInfoService struct {
	Context xcontext.XContext
	Logger  xlog.ILogger
}

// GetUserInfo 获取用户信息
func (svr *GetUserInfoService) GetUserInfo(subid, appflag string) (*dto.UserInfo, error) {
	subId := subid
	appFlag := appflag

	// 检查redis中是否有subid信息
	// redisSubIdPrefix := viper.GetString("personac.userinfo")
	// cachekey := redisSubIdPrefix + subId
	// tmp := &dto.UserInfo{}
	// err := redis.GetObject(cachekey, tmp)
	// if err == nil {
	// 	svr.Logger.Infof("redis缓存中有subID的信息：%s，对应的数据为： %+v", subId, tmp)
	// 	if tmp.SubId != "" {
	// 		return tmp, nil
	// 	}
	// }

	// 从数据库中取数据
	userInfo, err := svr.getUserInfoFromDb(subId, appFlag)
	if err != nil {
		return nil, err
	}
	if userInfo == nil {
		return nil, errors.New("获取用户信息错误")
	}

	// 将查询数据写入缓存
	// expire := viper.GetInt("redis.expire.userinfo")
	// _ = redis.SetObject(cachekey, userInfo, expire)

	jsonstring, _ := json.Marshal(userInfo)
	svr.Logger.Infof("personac:redis中没有subid：%s的信息，读库后信息：%s", subId, string(jsonstring))

	return userInfo, nil
}

// translationSelectDB 多次查询多个表，返回用户基础信息
func (svr *GetUserInfoService) getUserInfoFromDb(subId string, appFlag string) (*dto.UserInfo, error) {
	//存在此用户信息，可以联表查询或者查多张表返回用户信息
	//0 xx_user_login
	var userData mo.VolunteerUserLogin
	{
		conn := orm.NewDB(svr.Context)
		where := map[string]interface{}{"sub_id": subId}
		err := conn.Table(appFlag + "_user_login").Where(where).First(&userData).Error
		if err != nil {
			svr.Logger.Infof("personac:subid:%s, 获取 %s_user_login表数据：%+v，出现的错误：%+v", subId, appFlag, userData, err)
			return nil, err
		}
		if userData.SubID == "" {
			return nil, errors.New("sadsa")
		}
	}
	var userbase = &mo.UserBase{}
	{
		conn := orm.NewDB(svr.Context)
		err := conn.Where("sub_id = ?", userData.SubID).Limit(1).Find(&userbase).Error
		if err != nil {
			svr.Logger.Infof("personac:subid:%s, 获取 %s_user_login表数据：%+v，出现的错误：%+v", subId, appFlag, userData, err)
			return nil, err
		}
		if userData.SubID == "" {
			return nil, errors.New("sadsa")
		}
	}
	var person = &mo.Person{}
	{
		conn := orm.NewDB(svr.Context)
		err := conn.Where("person_id = ?", userbase.PersonID).Limit(1).Find(&person).Error
		if err != nil {
			svr.Logger.Infof("personac:subid:%s, 获取 %s_user_login表数据：%+v，出现的错误：%+v", subId, appFlag, userData, err)
			return nil, err
		}
		if userData.SubID == "" {
			return nil, errors.New("sadsa")
		}
	}

	// 3 解密并组装数据
	detruename := person.Truename
	deidcard := person.Idcard
	demobile := person.Mobile

	truename2 := []rune(detruename)
	len0 := len(truename2)
	if len0 == 2 {
		detruename = "*" + string(truename2[1:])
	}
	if len0 == 3 {
		detruename = "**" + string(truename2[2:])
	}
	if len0 >= 4 {
		len1 := len0 - 1
		detruename = strings.Repeat("*", len1-1) + string(truename2[len1:])
	}

	if len(deidcard) >= 16 {
		deidcard = deidcard[:1] + "***************" + deidcard[17:]
	} else {
		deidcard = deidcard[:1] + "*************" + deidcard[14:]
	}

	if len(demobile) >= 10 {
		demobile = demobile[:3] + "******" + demobile[9:]
	}
	userInfo := &dto.UserInfo{
		SubId:            subId,
		IsPersonVerified: userData.IsPersonVerified,
		Mobile:           demobile,
		TrueName:         detruename,
		IdCard:           deidcard,
	}

	return userInfo, nil
}

// GetUserStatus 可以用此函数替换获取用户状态的方法[已废弃]
// 最初是想通过identity用户接口来获取信息，但这个接口的信息是经过rsa加密过的，目前我们是有私钥可以解密，以后一定有私钥吗？还涉及到超时问题
func GetUserStatus(subId string) (*dto.UserInfo, error) {
	// 检查redis中是否有subid信息
	redisSubIdPrefix := viper.GetString("redis.prefix.userinfo")
	cachekey := redisSubIdPrefix + subId
	user := &dto.UserInfo{}
	userStr, err := redis.GetString(cachekey)
	if err == nil && userStr != "" {
		err = json.Unmarshal([]byte(userStr), &user)
		if err != nil {
			return nil, err
		}
		return user, nil
	}
	return nil, err
}
