package services

import (
	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xlog"
)

// NewGetUserInfoService 创建GetUserInfoService实例
func NewGetUserInfoService(rasseContext xcontext.XContext) *GetUserInfoService {
	var getUserInfoService GetUserInfoService
	getUserInfoService.Context = rasseContext
	getUserInfoService.Logger = xlog.WithCataLog("GetUserInfoService").WithContext(rasseContext)
	return &getUserInfoService
}
