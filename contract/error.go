package contract

import "fmt"

// KnownError 错误类
type KnownError struct {
	ErrorCode int
	Message   string
	Err       error
}

func (se *KnownError) Error() string {
	return fmt.Sprintf("%s: %s", se.Message, se.Err.Error())
}
