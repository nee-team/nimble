package xgin

import (
	"fmt"
	"net/http"
	"reflect"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitee.com/nee-team/nimble/xcontext"
)

var mock *ginContextMock

type ginContextMock struct {
	RasseTrace xcontext.RasseTrace
	User       xcontext.UserInfo
	BizTrace   xcontext.BizTrace
	Lang       xcontext.LangInfo
	MeshTrace  xcontext.MeshTrace
	Protocol   xcontext.ProtocolType
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/fullcontext", fullInfoContext())
	r.GET("/emptycontext", emptyInfoContext())
	return r
}

func fullInfoContext() gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		var xctx = NewContext(ginCtx)
		var user = xctx.GetUserInfo()
		if !reflect.DeepEqual(user, mock.User) {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取用户信息错误！want userID = %s,userName = %s,but userID = %s,userName = %s",
				mock.User.UserId, mock.User.UserName, user.UserId, user.UserName))
			return
		}
		var trace = xctx.GetRasseTrace()
		if trace != mock.RasseTrace {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取跟踪信息错误！want TraceID = %s, but TraceID = %s",
				mock.RasseTrace, trace))
			return
		}
		var chainID = xctx.GetBizTrace()
		if chainID != mock.BizTrace {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取业务跟踪信息错误！want chainID = %s, but chainID = %s",
				mock.BizTrace, chainID))
			return
		}
		var apm = xctx.GetMeshTrace()
		if len(apm) != len(mock.MeshTrace) {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取链路跟踪信息缺失！want 有%d 个链路信息, but 只获取到%d个信息",
				len(mock.MeshTrace), len(apm)))
			return
		}
		var lang = xctx.GetLang()
		if lang != mock.Lang {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取语言信息错误！want %s, but %s",
				mock.Lang, lang))
			return
		}

		for k, v := range mock.MeshTrace {
			av, ok := apm[k]
			if ok {
				if av != v {
					ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取链路跟踪信息错误！want key：%s,value:%s, but key:%s,value:%s",
						k, v, k, av))
					return
				}
			} else {
				ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取链路跟踪信息不存在！want key：%s but not exists！",
					k))
				return
			}
		}

		var ctx = xctx.GetRawContext()
		if !reflect.DeepEqual(ctx, ginCtx) {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("获取原始上下文不一致！"))
			return
		}

		var protoco = xctx.GetProtocol()
		if protoco != xcontext.Gin {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("gin 上下文的协议必须为Gin！"))
			return
		}
		ginCtx.JSON(http.StatusOK, "")
	}
}

func emptyInfoContext() gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		var xctx = NewContext(ginCtx)
		var traceInfo = xctx.GetRasseTrace()
		if traceInfo == "" {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("RasseTrace 不能为空！"))
			return
		}
		var chainInfo = xctx.GetBizTrace()

		if chainInfo == "" {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("BizTrace 不能为空！"))
			return
		}

		var lang = xctx.GetLang().Code
		if lang == "" {
			ginCtx.JSON(http.StatusInternalServerError, fmt.Sprintf("多语言信息不能为空！"))
			return
		}

		ginCtx.JSON(http.StatusOK, "")
	}
}

func initGinMockData() *ginContextMock {

	var mock = &ginContextMock{}
	mock.RasseTrace = xcontext.RasseTrace(uuid.New().String())
	mock.BizTrace = xcontext.BizTrace(uuid.New().String())
	mock.User = xcontext.UserInfo{
		UserId:   "37456",
		UserName: "xiaoming",
	}
	mock.Lang = xcontext.LangInfo{Code: "en-US"}
	apm := make(map[string]string)
	for _, v := range xcontext.DEFAULT_MESH_TRACE_KEYS {
		apm[v] = uuid.New().String()
	}
	mock.MeshTrace = apm

	var app = setupRouter()
	go app.Run()
	return mock
}
