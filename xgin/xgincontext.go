package xgin

import (
	"time"

	"github.com/gin-gonic/gin"

	"gitee.com/nee-team/nimble/xcontext"
)

type xGinContext struct {
	protocol   xcontext.ProtocolType
	rasseTrace xcontext.RasseTrace
	meshTrace  xcontext.MeshTrace
	user       xcontext.UserInfo
	lang       xcontext.LangInfo
	bizTrace   xcontext.BizTrace
	raw        *gin.Context
}

// 构造gin的xcontext
func NewContext(ginContext *gin.Context) xcontext.XContext {
	var ctx xGinContext
	ctx.raw = ginContext
	ctx.protocol = xcontext.Gin
	ctx.initUserInfo()
	ctx.initMeshInfo()
	ctx.initRasseTraceInfo()
	// ctx.initLang()
	ctx.initBizTraceInfo()
	return &ctx
}

// private Init
// 初始化链路跟踪信息
func (ctx *xGinContext) initMeshInfo() {
	var info = make(xcontext.MeshTrace)
	headerKeys := xcontext.GetMeshKeys()
	for _, v := range headerKeys {
		value := ctx.raw.GetHeader(v)
		if value != "" {
			info[v] = value
		}
	}
	ctx.meshTrace = info
}

func (ctx *xGinContext) initRasseTraceInfo() {
	ctx.rasseTrace = xcontext.RasseTrace(GetTraceID(ctx.raw))
}

func (ctx *xGinContext) initBizTraceInfo() {
	ctx.bizTrace = xcontext.BizTrace(GetBizTraceID(ctx.raw))
}

// func (ctx *xGinContext) initLang() {
// 	InitLang(ctx.raw)
// 	ctx.lang = xcontext.LangInfo{Code: GetLang(ctx.raw)}
// }

// 初始化 用户信息
func (ctx *xGinContext) initUserInfo() {
	var info xcontext.UserInfo
	var staff = GetCurrentUser(ctx.raw)
	if staff != nil {
		info.UserId = staff.StaffID
		info.UserName = staff.StaffName
	}
	ctx.user = info
}

// ===== rasseContext start ======
func (ctx *xGinContext) GetRawContext() interface{} {
	return ctx.raw
}

func (ctx *xGinContext) GetRasseTrace() xcontext.RasseTrace {
	return ctx.rasseTrace
}

func (ctx *xGinContext) GetLang() xcontext.LangInfo {
	return ctx.lang
}

func (ctx *xGinContext) GetMeshTrace() xcontext.MeshTrace {
	return ctx.meshTrace
}

func (ctx *xGinContext) GetProtocol() xcontext.ProtocolType {
	return ctx.protocol
}

func (ctx *xGinContext) GetUserInfo() xcontext.UserInfo {
	return ctx.user
}

func (ctx *xGinContext) GetBizTrace() xcontext.BizTrace {
	return ctx.bizTrace
}

func (ctx *xGinContext) Get(key string) (value interface{}, exist bool) {
	return ctx.raw.Get(key)
}

func (ctx *xGinContext) Set(key string, value interface{}) {
	ctx.raw.Set(key, value)
}

func (ctx *xGinContext) Deadline() (deadline time.Time, ok bool) {
	deadline, ok = ctx.raw.Deadline()
	return
}

func (ctx *xGinContext) Done() <-chan struct{} {
	return ctx.raw.Done()
}

func (ctx *xGinContext) Err() error {
	return ctx.raw.Err()
}

func (ctx *xGinContext) Value(key interface{}) interface{} {
	return ctx.raw.Value(key)
}

// === rasseContext end
