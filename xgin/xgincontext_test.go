package xgin

import (
	"net/http"
	"testing"

	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xhttp"
	"gitee.com/nee-team/nimble/xlog"
)

func TestNewContextWithFullInfo(t *testing.T) {

	mock = initGinMockData()
	fields := initHeader(mock)
	err := initLog()
	if err != nil {
		t.Errorf(err.Error())
	}
	xhttp.Init("invoke")
	rep, err := xhttp.New().Get("http://localhost:8080/fullcontext").AddHeaders(fields).Request()
	if err != nil {
		t.Errorf(err.Error())
	}
	if rep.StatusCode != http.StatusOK {
		t.Errorf(rep.Content())
	}
}

func initLog() error {
	defer xlog.Sync()

	err := xlog.Init(&xlog.LogSettings{
		Level:    config.GetStringOrDefault("log.level", xlog.DefaultLevel),
		Path:     config.GetStringOrDefault("log.path", xlog.DefaultPath),
		FileName: config.GetStringOrDefault("log.filename", xlog.DefaultFileName),
		CataLog:  config.GetStringOrDefault("log.catalog", xlog.DefaultCataLog),
		Caller:   config.GetBoolOrDefault("log.caller", xlog.DefaultCaller),
	})
	if err == nil {
		xhttp.Init("invoke")
	}
	return err
}

func initHeader(mock *ginContextMock) map[string]string {

	var fields = make(map[string]string)
	fields[xcontext.RASSE_TRACE_KEY] = string(mock.RasseTrace)
	fields[xcontext.BIZ_TRACE_KEY] = string(mock.BizTrace)
	for k, v := range mock.MeshTrace {
		fields[k] = v
	}
	fields[xcontext.USERID_KEY] = mock.User.UserId
	fields[xcontext.USERNAME_KEY] = mock.User.UserName
	fields[xcontext.LANG_KEY] = mock.Lang.Code
	return fields

}

func TestNewContextWithEmptyInfo(t *testing.T) {
	mock = initGinMockData()
	err := initLog()
	rep, err := xhttp.New().Get("http://localhost:8080/emptycontext").Request()
	if err != nil {
		t.Errorf(err.Error())
	}
	if rep.StatusCode != http.StatusOK {
		t.Errorf(rep.Content())
	}
}
