package xgin

import (
	"runtime"

	"gitee.com/nee-team/nimble/extension/xstring"
	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xhttp"

	"github.com/gin-gonic/gin"

	"gitee.com/nee-team/nimble/contract"
)

const (
	ctxKeyPref = xcontext.RASSE_PREFIX

	// auth
	CtxKey_AuthTypeKey = ctxKeyPref + "AuthType"
	CtxKey_User        = xcontext.USER_KEY
	CtxKey_UserID      = xcontext.USERID_KEY
	CtxKey_UserName    = xcontext.USERNAME_KEY
	// servelog 堆栈
	CtxKey_Stack = ctxKeyPref + "Stack"
	// servelog 服务产生的真实错误信息
	CtxKey_RealError = ctxKeyPref + "RealError"
	CtxKey_TraceID   = xcontext.RASSE_TRACE_KEY
	CtxKey_ChainID   = xcontext.BIZ_TRACE_KEY
	// Deprecated: Use CtxKey_TraceID instead.
	CtxKey_OldTraceID = "rasse-trace-id"
	CtxKey_QueryLang  = "lang"
	// 参考规范：https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Language
	CtxKey_HeaderLang = xhttp.LangHeaderKey
	CtxKey_CookieLang = xcontext.LANG_KEY
	CtxKey_Lang       = xcontext.LANG_KEY
)

// SetCurrentUser 将指定用户信息保存到请求上下文
func SetCurrentUser(ctx *gin.Context, s *contract.Staff) {
	ctx.Set(CtxKey_User, s)
}

// GetCurrentUser 获取当前登录用户
func GetCurrentUser(ctx *gin.Context) *contract.Staff {
	if user, ok := ctx.Keys[CtxKey_User]; ok && user != nil {
		return user.(*contract.Staff)
	} else {
		var userId = ctx.GetHeader(CtxKey_UserID)
		var userName = ctx.GetHeader(CtxKey_UserName)
		if userId != "" || userName != "" {
			return &contract.Staff{
				StaffID:   userId,
				StaffName: userName,
			}
		}
	}

	return nil
}

func SetStack(ctx *gin.Context, stack string) {
	ctx.Set(CtxKey_Stack, stack)
}

func GetStack(ctx *gin.Context) string {
	if str, set := ctx.Get(CtxKey_Stack); set {
		return str.(string)
	}

	// 记录堆栈
	stack := make([]byte, 1024*8)
	stack = stack[:runtime.Stack(stack, false)]
	str := string(stack)
	SetStack(ctx, str)
	return str
}

func InitTraceID(ctx *gin.Context) {
	traceIDVal := ctx.GetHeader(CtxKey_TraceID)
	// 此处做兼容处理
	if traceIDVal == "" {
		traceIDVal = ctx.GetHeader(CtxKey_OldTraceID)
	}
	if traceIDVal == "" {
		traceIDVal = xstring.GetUUID()
	}
	ctx.Set(CtxKey_TraceID, traceIDVal)
}

func GetTraceID(ctx *gin.Context) string {
	if v, exists := ctx.Get(CtxKey_TraceID); exists {
		return v.(string)
	}

	return ""
}

func InitBizTraceID(ctx *gin.Context) {
	traceIDVal := ctx.GetHeader(CtxKey_ChainID)
	if traceIDVal == "" {
		traceIDVal = xstring.GetUUID()
	}
	ctx.Set(CtxKey_ChainID, traceIDVal)
}

func GetBizTraceID(ctx *gin.Context) string {
	if v, exists := ctx.Get(CtxKey_ChainID); exists {
		return v.(string)
	}

	return ""
}

// url>cookie> header > default
// func InitLang(ctx *gin.Context) {
// 	var err error
// 	lang := ctx.Query(CtxKey_QueryLang)
// 	if lang == "" {
// 		lang, err = ctx.Cookie(CtxKey_CookieLang)
// 		if err != nil && err != http.ErrNoCookie {
// 			xlog.Error(err.Error())
// 		}
// 	}
// 	if lang == "" {
// 		lang = ctx.GetHeader(CtxKey_HeaderLang)
// 	}
// 	if lang == "" {
// 		lang = i18n.GetDefaultLang()
// 	}
// 	if lang == "" {
// 		lang = xcontext.DEFAULT_LANG
// 	}
// 	ctx.Set(CtxKey_Lang, lang)
// }

func GetLang(ctx *gin.Context) string {
	if v, exists := ctx.Get(CtxKey_Lang); exists {
		return v.(string)
	}
	return xcontext.DEFAULT_LANG
}

// GetCurrentAuthType 获取当前请求采用的鉴权方法
// 如果禁用了鉴权组件(mw.auth.enabled=false),则总是返回空字符串
// 如果是某个路径关闭了鉴权功能(mode: "off"),返回"off"
func GetCurrentAuthType(ctx *gin.Context) string {
	authTypeObj, exist := ctx.Get(CtxKey_AuthTypeKey)
	if !exist {
		return ""
	}

	authType, ok := authTypeObj.(string)
	if !ok {
		return ""
	}
	return authType
}
