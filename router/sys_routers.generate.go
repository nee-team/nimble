package router

import (
	"gitee.com/nee-team/nimble/sysapi"
	"github.com/gin-gonic/gin"
)

// UseRouters 向 Gin 注册业务路由
func SysRegister() func(eng *gin.Engine, hostPath string) error {
	return func(eng *gin.Engine, hostPath string) error {
		rg := eng.Group("__sys")
		rg.Handle("GET", "/healthz", sysapi.Live())
		rg.Handle("GET", "/host", sysapi.Host())
		rg.Handle("GET", "/livez", sysapi.Live())
		rg.Handle("GET", "/meta", sysapi.Meta())
		rg.Handle("GET", "/readyz", sysapi.Ready())
		return nil
	}
}
