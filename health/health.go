package health

import (
	"sync"
)

var mu sync.Mutex

// items 健康检查清单项，包括框架内置检查项和通过 Register 方法注册进来的业务自定义检查项
var items = []Checker{}

// Result 检查项结果
type Result struct {
	Name    string            `json:"name"`
	OK      bool              `json:"ok"`
	Tags    map[string]string `json:"tags,omitempty"`    // 检查结果的附加信息
	Message string            `json:"message,omitempty"` // 检查结果描述
}

// Register 注册健康检查项
func Register(ci Checker) {
	mu.Lock()
	defer mu.Unlock()
	items = append(items, ci)
}

// CheckAll 对所有检查项发起检查
func CheckAll() (bool, []Result) {
	isOk := true
	rst := make([]Result, 0)
	for _, c := range items {
		if !c.Enabled() {
			continue
		}
		name := c.GetName()
		ok, value, msg := c.Check()
		isOk = isOk && ok
		rst = append(rst, Result{Name: name, OK: ok, Tags: value, Message: msg})
	}
	return isOk, rst
}
