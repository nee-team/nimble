package health

// Checker 用于表征一个业务健康度的检查项
type Checker interface {
	GetName() string
	Enabled() bool
	Check() (isOk bool, tags map[string]string, message string) // 实现具体的检查逻辑
}
