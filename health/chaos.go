package health

import (
	"fmt"
	"math/rand"

	"github.com/spf13/viper"
)

// ChaosChecker 故障注入器，用户随机地向健康检查注入失败检查项
type ChaosChecker struct{}

// GetName 获取名称
func (c *ChaosChecker) GetName() string {
	return "chaos"
}

// Enabled 是否启用
func (c *ChaosChecker) Enabled() bool {
	return true
}

// Check 检查可用状态
func (c *ChaosChecker) Check() (bool, map[string]string, string) {
	percent := viper.GetInt("health.chaos.percent")
	if percent != 0 {
		if percent >= 100 || percent < 0 {
			return false, map[string]string{"percent": fmt.Sprintf("%v", percent)}, "chaos enabled"
		}
		r := rand.Intn(100)
		if r < percent {
			return false, map[string]string{"percent": fmt.Sprintf("%v", percent), "value": fmt.Sprintf("%v", r)}, "chaos injected"
		}
	}
	return true, nil, ""
}

// 作为内置检查项注册
func init() {
	Register(&ChaosChecker{})
}
