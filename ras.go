package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"os"
)

//生成RSA私钥和公钥，保存到文件中
func GenerateRSAKey(bits int) {
	//GenerateKey函数使用随机数据生成器random生成一对具有指定字位数的RSA密钥
	//Reader是一个全局、共享的密码用强随机数生成器
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		panic(err)
	}
	//保存私钥
	//通过x509标准将得到的ras私钥序列化为ASN.1 的 DER编码字符串
	X509PrivateKey := x509.MarshalPKCS1PrivateKey(privateKey)
	//使用pem格式对x509输出的内容进行编码
	//创建文件保存私钥
	privateFile, err := os.Create("private.pem")
	if err != nil {
		panic(err)
	}
	defer privateFile.Close()
	//构建一个pem.Block结构体对象
	privateBlock := pem.Block{Type: "RSA Private Key", Bytes: X509PrivateKey}
	//将数据保存到文件
	pem.Encode(privateFile, &privateBlock)
	//保存公钥
	//获取公钥的数据
	publicKey := privateKey.PublicKey
	//X509对公钥编码
	X509PublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		panic(err)
	}
	//pem格式编码
	//创建用于保存公钥的文件
	publicFile, err := os.Create("public.pem")
	if err != nil {
		panic(err)
	}
	defer publicFile.Close()
	//创建一个pem.Block结构体对象
	publicBlock := pem.Block{Type: "RSA Public Key", Bytes: X509PublicKey}
	//保存到文件
	pem.Encode(publicFile, &publicBlock)
}

//RSA加密
func RSA_Encrypt(plainText []byte, path string) []byte {
	//打开文件
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	//读取文件的内容
	info, _ := file.Stat()
	buf := make([]byte, info.Size())
	file.Read(buf)
	//pem解码
	block, _ := pem.Decode(buf)
	//x509解码
	publicKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panic(err)
	}
	//类型断言
	publicKey := publicKeyInterface.(*rsa.PublicKey)
	//对明文进行加密
	cipherText, err := rsa.EncryptPKCS1v15(rand.Reader, publicKey, plainText)
	if err != nil {
		panic(err)
	}
	//返回密文
	return cipherText
}

//RSA解密
func RSA_Decrypt(cipherText []byte, path string) []byte {
	//打开文件
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	//获取文件内容
	info, _ := file.Stat()
	buf := make([]byte, info.Size())
	file.Read(buf)
	//pem解码
	block, _ := pem.Decode(buf)
	//X509解码
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		panic(err)
	}
	//对密文进行解密
	plainText, _ := rsa.DecryptPKCS1v15(rand.Reader, privateKey, cipherText)
	//返回明文
	return plainText
}

// 私钥生成
//openssl genrsa -out rsa_private_key.pem 1024
var privateKey = []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQD6eb7uCpIQXIg4R7na5bh6u9nSO4Zp4S7AbKpgv+EvngTGng3P
/rhTgxfv0yrr7w6YZSv6JtaXkCCNfYP/BP7PhEevTXH5DBjp0wc9GCTTc3MnPON3
NIlQb7gXXQLx4FkSdNA+hgdrG3s/9oiTVT0iAIovKHKYU1ol8ZceXrwcUwIDAQAB
AoGAQndyKhrV/c+AOmcWM7dICBG3UKmJFqmxzVBIuhnQ+ODW5Znlkm9GnKqp/HMt
7aPnXJtkWyJZSajuan2HPHIn54UO5p/XN4WGwiBcw0rxi3xYdu6DdUrAZKqOVeTQ
MHOH885n4RYhJsMm7RyuaBHGgqItzXZZ97NKfMNQkGd1xIECQQD+Ca4FFbrhK9LY
MWWnG5Nd2K0eJ9PLhIMMN0aG3cuCkyfmuhBlc0wM0utun5j9BJ1wvnm9CCnvANEq
/6i0XAPBAkEA/GkFs9mYYJAIjVzFxE+fHrdLky3e0W8mZx/VA85eu/oQrNtKuoUB
VSSJLSMn8f6pQ42YLtIiec7uNTlqgdkVEwJBAKKN7xyx2vNa54APm8xiiNn0XFJ/
ibchA/o9JJQIOMFFCLNLPFKuhGtwS9Ztqae93EDYoW2kW7DkBPROw9UlTAECQQCS
whmPta/UTUq7rrpKZyUUfeySObR5P1Ar26VGHkKUt1PkvWhYxKa+s4yS0wMRwEj4
PybB6mojOr7j8WtM7kRDAkBO37AjTtdzVxX5p2Hnd76crr5vbRfXYH0YMcCVHPEY
ZwzqkiW0RzdvBYArm7ABXodxXYPG0x0Kit8i1sPJF1kB
-----END RSA PRIVATE KEY-----
`)

// 公钥: 根据私钥生成
//openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
var publicKey = []byte(`
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD6eb7uCpIQXIg4R7na5bh6u9nS
O4Zp4S7AbKpgv+EvngTGng3P/rhTgxfv0yrr7w6YZSv6JtaXkCCNfYP/BP7PhEev
TXH5DBjp0wc9GCTTc3MnPON3NIlQb7gXXQLx4FkSdNA+hgdrG3s/9oiTVT0iAIov
KHKYU1ol8ZceXrwcUwIDAQAB
-----END PUBLIC KEY-----
`)

// RsaEncrypt 加密
func RsaEncrypt(origData []byte) ([]byte, error) {
	//解密pem格式的公钥
	block, _ := pem.Decode(publicKey)
	if block == nil {
		return nil, errors.New("public key error")
	}
	// 解析公钥
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	// 类型断言
	pub := pubInterface.(*rsa.PublicKey)
	//加密
	return rsa.EncryptPKCS1v15(rand.Reader, pub, origData)
}

// RsaDecrypt 解密
func RsaDecrypt(ciphertext []byte) ([]byte, error) {
	//解密
	block, _ := pem.Decode(privateKey)
	if block == nil {
		return nil, errors.New("private key error!")
	}
	//解析PKCS1格式的私钥
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	// 解密
	return rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
}
