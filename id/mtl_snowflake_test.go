package id

import (
	"fmt"
	"git.code.oa.com/fip-team/mtl-snowflake"

	"os"
	"testing"
)

func TestInitWithSeetings(t *testing.T) {
	os.Setenv(MachineIDEnvName, "0")

	//未初始化,不能生成
	_, err := Generate()
	if err == nil {
		t.Fatal("未初始化")
		return
	}

	machineID, err := GetMachineIDFromEnvs()
	if err != nil {
		t.Fatal("获取machineid失败")
		return
	}
	err = InitWithSeetings(machineID, generator.Settings{
		TimeBit:      41,
		MachineIDBit: 10,
		TimelineBit:  0,
		SeqBit:       12,
		Epoch:        generator.DefaultEpoch,
	})
	if err != nil {
		t.Fatal(err.Error())
	}

	// 不允许多次初始化
	err = Init(machineID)
	if err == nil {
		t.Fatal("id被多次初始化")
		return
	}

	const newMachineID = 12
	// 更新machineID
	err = SetMachineID(newMachineID)
	if err != nil {
		t.Fatal("更新machineID失败")
		return
	}

	id, err := Generate()
	if err != nil {
		t.Fatal("生成ID出错")
		return
	}
	decID, err := Decompose(id)

	if err != nil {
		t.Fatal("decompose失败")
		return
	}

	if decID.MachineID != newMachineID {
		t.Fatalf("更新machineID失败,got:%d,want:%d", decID.MachineID, newMachineID)
		return
	}

	for i := 0; i < 1e4; i++ {
		// 数字形式(int64)：560780571450613760
		newID, err := Generate()
		if err != nil {
			t.Fatal("生成ID出错")
			return
		}

		// 为方便阅读，可将数字格式id转换成可阅读格式：时间+序号：2019090419014733273728
		readableID, err := ToReadable(newID)
		fmt.Println(newID, readableID)
	}
}
