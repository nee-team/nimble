package id

import (
	"fmt"
	"os"
	"strconv"
	"sync"

	generator "git.code.oa.com/fip-team/mtl-snowflake"
	"gitee.com/nee-team/nimble/xlog"
	"github.com/pkg/errors"
)

const (
	MachineIDEnvName = "SNOWFLAKE_MACHINEID" //machineID环境变量名
)
const (
	ErrMsgUnInitialized = "id生成器未初始化，请先调用Init方法"
	ErrMsgReInitialize  = "已经调用过初始化函数Init/InitWithSeetings，请勿重复调用"
)

var (
	mutex       sync.Mutex
	initialized bool = false //是否已初始化
	idGen       *generator.IDGenerator
)

// GetMachineIDFromEnvs 通过环境变量获取machineID
func GetMachineIDFromEnvs() (int64, error) {
	envMachineID := os.Getenv(MachineIDEnvName)
	xlog.Infof("通过环境变量\"%s\"获取到machineid:%s", MachineIDEnvName, envMachineID)
	machineID, err := strconv.ParseInt(envMachineID, 10, 64)
	if err != nil {
		errMsg := fmt.Sprintf("解析machineID失败，可通过环境变量\"%s\"设置(非负整数)，当前值为：%s", MachineIDEnvName, envMachineID)
		xlog.Errorf(errMsg)
		return -1, errors.New(errMsg)
	}
	if machineID < 0 {
		errMsg := fmt.Sprintf("machineID不能为负，可通过环境变量\"%s\"设置(非负整数)，当前值为：%s", MachineIDEnvName, envMachineID)
		xlog.Errorf(errMsg)
		return -1, errors.New(errMsg)
	}
	return machineID, nil
}

// Init 初始化id生成器
//   - TimeBit=41 可使用64年
//   - MachineIDBit=9 最多512个节点
//   - TimelineBit=1  两条时间线，能解决常见的时间回退问题
//   - SeqBit=12 1毫秒内最多生成4096个序号
//   - Epoch=1433865600000000000(2015.6.10 00:00:00) 基准时间(unix nano)
//   - machineID 可通过GetMachineIDFromEnvs方法获取。
func Init(machineID int64) error {
	return InitWithSeetings(machineID, *generator.DefaultSettings)
}

// InitWithSeetings
// 使用自定义配置初始化id生成器
func InitWithSeetings(machineID int64, settings generator.Settings) error {
	return initWithSeetings(machineID, settings)
}

// initWithSeetings
// 使用自定义配置初始化id生成器
func initWithSeetings(machineID int64, settings generator.Settings) error {
	mutex.Lock()
	defer mutex.Unlock()

	if initialized == true {
		errMsg := fmt.Sprintf(ErrMsgReInitialize)
		xlog.Error(errMsg)
		return errors.New(errMsg)
	}

	newIDGen, err := generator.NewGeneratorWithSettings(machineID, settings)
	if err != nil {
		xlog.Error(err.Error())
		return err
	}
	idGen = newIDGen
	initialized = true
	return nil
}

// Generate 生成新id，满足
//   - 分布式全局唯一
//   - 趋势递增
func Generate() (int64, error) {
	if idGen == nil {
		xlog.Error(ErrMsgUnInitialized)
		return -1, errors.New(ErrMsgUnInitialized)
	}
	return idGen.Generate()
}

// ToReadable 将int64类型的id转换成时间+序号格式，如：2019090419014733273728
func ToReadable(id int64) (string, error) {
	if idGen == nil {
		xlog.Error(ErrMsgUnInitialized)
		return "", errors.New(ErrMsgUnInitialized)
	}
	return idGen.ToReadable(id), nil
}

// GetMachineID 节点编号
func GetMachineID() (int64, error) {
	if idGen == nil {
		xlog.Error(ErrMsgUnInitialized)
		return -1, errors.New(ErrMsgUnInitialized)
	}
	return idGen.GetMachineID(), nil
}

// SetMachineID 重新设置节点编号
func SetMachineID(machineID int64) error {
	mutex.Lock()
	defer mutex.Unlock()

	if idGen == nil {
		xlog.Error(ErrMsgUnInitialized)
		return errors.New(ErrMsgUnInitialized)
	}

	// 1. 获取旧实例的配置
	settings := idGen.GetSettings()

	// 2. 生成新的实例
	newIDGen, err := generator.NewGeneratorWithSettings(machineID, settings)
	if err != nil {
		xlog.Error(err.Error())
		return err
	}

	// 3. 替换旧的实例
	idGen = newIDGen

	return nil
}

// GetSettings 初始化配置
func GetSettings() (generator.Settings, error) {
	if idGen == nil {
		xlog.Error(ErrMsgUnInitialized)
		return generator.Settings{}, errors.New(ErrMsgUnInitialized)
	}
	return idGen.GetSettings(), nil
}

// Decompose 将id解析成time、seq等部分
func Decompose(id int64) (*generator.IDCompose, error) {
	if idGen == nil {
		xlog.Error(ErrMsgUnInitialized)
		return nil, errors.New(ErrMsgUnInitialized)
	}
	return idGen.Decompose(id), nil
}
