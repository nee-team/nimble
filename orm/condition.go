package orm

import (
	"reflect"

	multierror "github.com/hashicorp/go-multierror"
)

// Table 设置表名
// tableName 表名
func (t *FiDB) Table(tableName string) *FiDB {
	if t.hasTable == true {
		panic("Table方法已经被调用")
	}
	t.hasTable = true

	clone := t.db.Table(tableName)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// Select 返回column字段
// db.Select("name, age").Find(&users)
//// SELECT name, age FROM users;

// db.Select([]string{"name", "age"}).Find(&users)
//// SELECT name, age FROM users;

// db.Table("users").Select("COALESCE(age,?)", 42).Rows()
//// SELECT COALESCE(age,'42') FROM users;
func (t *FiDB) Select(query interface{}, args ...interface{}) *FiDB {
	if t.hasSelect == true {
		panic("已经调用过Select方法")
	}
	t.hasSelect = true

	clone := t.db.Select(query, args...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// 获取第一个匹配记录
// db.Where("name = ?", "jinzhu").First(&user)
// SELECT * FROM users WHERE name = 'jinzhu' limit 1;

// 获取所有匹配记录
// db.Where("name = ?", "jinzhu").Find(&users)
//// SELECT * FROM users WHERE name = 'jinzhu';
// db.Where("name <> ?", "jinzhu").Find(&users)

// IN
// db.Where("name in (?)", []string{"jinzhu", "jinzhu 2"}).Find(&users)

// LIKE
// db.Where("name LIKE ?", "%jin%").Find(&users)

// AND
// db.Where("name = ? AND age >= ?", "jinzhu", "22").Find(&users)

// Time
// db.Where("updated_at > ?", lastWeek).Find(&users)
// db.Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&users)

// 当使用struct查询时，GORM将只查询那些具有值的字段
// SELECT * FROM `mpv_order_filter_data`  WHERE (`category_id` = 2) AND (`category_name` = '优先审核')
// qa.Where(&model.OrderFilterData{CategoryID: 2,CategoryName:"优先审核"}).Find(&items)
// qa.Where(map[string]interface{}{"category_id": 2, "category_name": "优先审核"}).Find(&items)
func (t *FiDB) Where(query interface{}, args ...interface{}) *FiDB {
	kind := reflect.TypeOf(query).Kind()
	if kind != reflect.String {
		//panic("错误的Where格式")
	}

	clone := t.db.Where(query, args...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// 执行原生 SQL 时，不支持与其它方法的链式操作
// Scan
// type Result struct {
// 	Name string
// 	Age  int
// }

// var result Result
// db.Raw("SELECT name, age FROM users WHERE name = ?", 3).Scan(&result)
func (t *FiDB) Raw(sql string, values ...interface{}) *FiDB {
	if t.hasRaw == true {
		panic("已经调用过Raw方法")
	}
	t.hasRaw = true

	clone := t.db.Raw(sql, values...)

	t.RowsAffected = clone.RowsAffected

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// Joins specify Joins conditions
// rows, err := db.Table("users").Select("users.name, emails.email").Joins("left join emails on emails.user_id = users.id").Rows()
// db.Table("users").Select("users.name, emails.email").Joins("left join emails on emails.user_id = users.id").Scan(&results)
// db.Joins("JOIN emails ON emails.user_id = users.id AND emails.email = ?", "jinzhu@example.org").Joins("JOIN credit_cards ON credit_cards.user_id = users.id").Where("credit_cards.number = ?", "411111111111").Find(&user)
func (t *FiDB) Joins(query string, args ...interface{}) *FiDB {
	clone := t.db.Joins(query, args...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// Group specify the group method on the find
// rows, err := db.Table("orders").Select("date(created_at) as date, sum(amount) as total").Group("date(created_at)").Rows()
// rows, err := db.Table("orders").Select("date(created_at) as date, sum(amount) as total").Group("date(created_at)").Having("sum(amount) > ?", 100).Rows()
// db.Table("orders").Select("date(created_at) as date, sum(amount) as total").Group("date(created_at)").Having("sum(amount) > ?", 100).Scan(&results)
func (t *FiDB) Group(query string) *FiDB {
	if t.hasGroup == true {
		panic("已经调用过Group方法")
	}
	t.hasGroup = true

	clone := t.db.Group(query)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// Having specify HAVING conditions for GROUP BY
// rows, err := db.Table("orders").Select("date(created_at) as date, sum(amount) as total").Group("date(created_at)").Having("sum(amount) > ?", 100).Rows()
// db.Table("orders").Select("date(created_at) as date, sum(amount) as total").Group("date(created_at)").Having("sum(amount) > ?", 100).Scan(&results)
func (t *FiDB) Having(query interface{}, values ...interface{}) *FiDB {
	clone := t.db.Having(query, values...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// 获取模型的记录数
// db.Where("name = ?", "jinzhu").Or("name = ?", "jinzhu 2").Find(&users).Count(&count)
//// SELECT * from USERS WHERE name = 'jinzhu' OR name = 'jinzhu 2'; (users)
//// SELECT count(*) FROM users WHERE name = 'jinzhu' OR name = 'jinzhu 2'; (count)

// db.Model(&User{}).Where("name = ?", "jinzhu").Count(&count)
//// SELECT count(*) FROM users WHERE name = 'jinzhu'; (count)

// db.Table("deleted_users").Count(&count)
//// SELECT count(*) FROM deleted_users;
func (t *FiDB) Count(value interface{}) *FiDB {
	if t.hasCount == true {
		panic("已经存在Count方法")
	}
	t.hasCount = true

	clone := t.db.Count(value)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// db.Limit(3).Find(&users)
//// SELECT * FROM users LIMIT 3;

// Cancel limit condition with -1
// db.Limit(10).Find(&users1).Limit(-1).Find(&users2)
//// SELECT * FROM users LIMIT 10; (users1)
//// SELECT * FROM users; (users2)
func (t *FiDB) Limit(limit interface{}) *FiDB {
	if t.hasLimit == true {
		panic("已经调用过Limit方法")
	}
	t.hasLimit = true

	clone := t.db.Limit(limit)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// 指定在开始返回记录之前要跳过的记录数
// db.Offset(3).Find(&users)
//// SELECT * FROM users OFFSET 3;

// Cancel offset condition with -1
// db.Offset(10).Find(&users1).Offset(-1).Find(&users2)
//// SELECT * FROM users OFFSET 10; (users1)
//// SELECT * FROM users; (users2)
func (t *FiDB) Offset(offset interface{}) *FiDB {
	if t.hasOffset == true {
		panic("已经调用过Offset方法")
	}
	t.hasOffset = true

	clone := t.db.Offset(offset)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// db.Order("age desc, name").Find(&users)
//// SELECT * FROM users ORDER BY age desc, name;

// Multiple orders
// db.Order("age desc").Order("name").Find(&users)
//// SELECT * FROM users ORDER BY age desc, name;

// ReOrder
// db.Order("age desc").Find(&users1).Order("age", true).Find(&users2)
//// SELECT * FROM users ORDER BY age desc; (users1)
//// SELECT * FROM users ORDER BY age; (users2)
func (t *FiDB) Order(value interface{}, reorder ...bool) *FiDB {
	clone := t.db.Order(value, reorder...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// Set set setting by name, which could be used in callbacks, will clone a new db, and update its setting
// "gorm:insert_option", "ON CONFLICT"  存在则更新，不存在则插入
// "gorm:query_option", "FOR UPDATE"    查询锁
func (t *FiDB) Set(name string, value interface{}) *FiDB {
	clone := t.db.InstantSet(name, value)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// db.Where("role = ?", "admin").Or("role = ?", "super_admin").Find(&users)
//// SELECT * FROM users WHERE role = 'admin' OR role = 'super_admin';

// Struct
// db.Where("name = 'jinzhu'").Or(User{Name: "jinzhu 2"}).Find(&users)
//// SELECT * FROM users WHERE name = 'jinzhu' OR name = 'jinzhu 2';

// Map
// db.Where("name = 'jinzhu'").Or(map[string]interface{}{"name": "jinzhu 2"}).Find(&users)
func (t *FiDB) Or(query interface{}, args ...interface{}) *FiDB {
	clone := t.db.Or(query, args...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// db.Not("name", "jinzhu").First(&user)
// SELECT * FROM users WHERE name <> "jinzhu" LIMIT 1;

// Not In
// db.Not("name", []string{"jinzhu", "jinzhu 2"}).Find(&users)
// SELECT * FROM users WHERE name NOT IN ("jinzhu", "jinzhu 2");

// Not In slice of primary keys
// db.Not([]int64{1,2,3}).First(&user)
//// SELECT * FROM users WHERE id NOT IN (1,2,3);

// Plain SQL
// db.Not("name = ?", "jinzhu").First(&user)
//// SELECT * FROM users WHERE NOT(name = "jinzhu");

// Struct
// db.Not(User{Name: "jinzhu"}).First(&user)
//// SELECT * FROM users WHERE name <> "jinzhu";
func (t *FiDB) Not(query interface{}, args ...interface{}) *FiDB {
	clone := t.db.Not(query, args...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// Model specify the model you would like to run db operations
//    // update all users's name to `hello`
//    db.Model(&User{}).Update("name", "hello")
//    // if user's primary key is non-blank, will use it as condition, then will only update the user's name to `hello`
//    db.Model(&user).Update("name", "hello")
func (t *FiDB) Model(entityPtr interface{}) *FiDB {
	if t.hasModel == true {
		panic("已经调用Model方法")
	}
	t.hasModel = true

	clone := t.db.Model(entityPtr)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}
