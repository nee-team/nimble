package orm

import (
	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/health"

	"github.com/jinzhu/gorm"
)

// Checker 健康检查器
type Checker struct{}

// GetName 获取名称
func (c *Checker) GetName() string {
	return "orm"
}

// Enabled 是否启用
func (c *Checker) Enabled() bool {
	return config.GetBoolOrDefault("health.orm.check", true)
}

// Check 检查可用状态
func (c *Checker) Check() (bool, map[string]string, string) {
	if dbInstances.mdb != nil && len(dbInstances.mdb) > 0 {

		// 由于 map 非并发安全，因此复制到临时容器，避免较慢的检查过程阻塞 dbInstances.m 的使用
		checking := make(map[string]*gorm.DB)
		for key, value := range dbInstances.mdb {
			checking[key] = value
		}

		// TODO: 可优化：最近短时间内有正常的数据库操作也可以认为状态可用，无需单独再建立连接，消耗资源
		for dbName, item := range checking {
			err := item.DB().Ping()
			if err != nil {
				return false, map[string]string{"dbname": dbName}, err.Error()
			}
		}
	}

	return true, nil, ""
}

// 作为内置检查项注册
func init() {
	health.Register(&Checker{})
}
