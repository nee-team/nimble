package orm

import (
	"database/sql/driver"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"
	"unicode"

	"gitee.com/nee-team/nimble/xlog"

	"github.com/spf13/cast"
)

var (
	sqlRegexp                = regexp.MustCompile(`\?`)
	numericPlaceHolderRegexp = regexp.MustCompile(`\$\d+`)
)

// NowFunc 当前
var NowFunc = func() time.Time {
	return time.Now()
}

// isPrintable
func isPrintable(s string) bool {
	for _, r := range s {
		if !unicode.IsPrint(r) {
			return false
		}
	}
	return true
}

// sql日志格式
var LogFormatter = func(values ...interface{}) (messages []interface{}) {
	if len(values) > 1 {
		var (
			sql             string
			formattedValues []string
			level           = values[0]
			currentTime     = NowFunc() // "\n\033[33m[" + NowFunc().Format("2006-01-02 15:04:05") + "]\033[0m"
			source          = values[1] // fmt.Sprintf("\033[35m(%v)\033[0m", values[1])
		)

		// 将标识传递到后续的 PrintLn 方法
		messages = []interface{}{level, source, currentTime}

		// level should be sql or log, set in: *gorm.DB.log, *gorm.DB.slog
		// see: https://github.com/jinzhu/gorm/blob/master/main.go
		if level == "sql" {
			// duration
			//messages = append(messages, fmt.Sprintf(" \033[36;1m[%.2fms]\033[0m ",
			//float64(values[2].(time.Duration).Nanoseconds()/1e4)/100.0))
			messages = append(messages, float64(values[2].(time.Duration).Nanoseconds()/1e4)/100.0)

			for _, value := range values[4].([]interface{}) {
				indirectValue := reflect.Indirect(reflect.ValueOf(value))
				if indirectValue.IsValid() {
					value = indirectValue.Interface()
					if t, ok := value.(time.Time); ok {
						if t.IsZero() {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", "0000-00-00 00:00:00"))
						} else {
							formattedValues = append(formattedValues,
								fmt.Sprintf("'%v'", t.Format("2006-01-02 15:04:05")))
						}
					} else if b, ok := value.([]byte); ok {
						if str := string(b); isPrintable(str) {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", str))
						} else {
							formattedValues = append(formattedValues, "'<binary>'")
						}
					} else if r, ok := value.(driver.Valuer); ok {
						if value, err := r.Value(); err == nil && value != nil {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
						} else {
							formattedValues = append(formattedValues, "NULL")
						}
					} else {
						switch value.(type) {
						case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, float32, float64, bool:
							formattedValues = append(formattedValues, fmt.Sprintf("%v", value))
						default:
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
						}
					}
				} else {
					formattedValues = append(formattedValues, "NULL")
				}
			}

			sql = values[3].(string)
			strValues := strings.Join(formattedValues, ", ")
			messages = append(messages, sql)
			messages = append(messages, strValues)
			//messages = append(messages, fmt.Sprintf(" \n\033[36;31m[%v]\033[0m ",
			//strconv.FormatInt(values[5].(int64), 10)+" rows affected or returned "))
			messages = append(messages, values[5].(int64))
		} else {
			messages = append(messages, "") // messages = append(messages, "\033[31;1m")
			messages = append(messages, values[2:]...)
			messages = append(messages, "") //messages = append(messages, "\033[0m")
		}
	}

	return
}

// logger
type logger interface {
	Print(v ...interface{})
}

// LogWriter log writer interface
type LogWriter interface {
	Println(v ...interface{})
}

// Logger default logger
type Logger struct {
	LogWriter
}

// Print format & print log
func (logger Logger) Print(values ...interface{}) {
	logger.Println(LogFormatter(values...)...)
}

// DBLogWriter 自定义LOG,增加trace_id
type DBLogWriter struct {
	props map[string]string
}

// SetProperty 设置logger属性记录
func (t *DBLogWriter) SetProperty(key, val string) {
	if t.props == nil {
		t.props = make(map[string]string)
	}
	t.props[key] = val
}

// Println 写日志
func (t *DBLogWriter) Println(v ...interface{}) {
	pieces := []interface{}(v)
	level := v[0]
	if level == "sql" {
		// see: LogFormatter, https://github.com/jinzhu/gorm/blob/master/logger.go
		//if level == "sql" {
		//	 [0] level
		//	 [1] source
		//	 [2] current time
		//	 [3] duration
		//	 [4] sql
		//	 [5] values
		//	 [6] rows affected
		//} else {
		//	 [0] level
		//	 [1] source
		//	 [2] current time
		//	 [3] \033[31;1m
		//	 [4] values[2:]...
		//	 [5] \033[0m
		//}
		var duration = cast.ToInt64(pieces[3])
		var affected = cast.ToInt64(pieces[6])
		fields := map[string]interface{}{
			"values":       pieces[5],
			"execute_time": pieces[2],
			"duration":     duration,
			"affected":     affected,
		}
		for k, v := range t.props {
			fields[k] = v
		}

		log := xlog.WithCataLog("orm").WithFields(fields)

		strType := reflect.TypeOf(pieces[4]).String()
		//mysql.MySQLError
		if strType == "*mysql.MySQLError" {
			log.Error(pieces[4])
		} else {
			log.Info(pieces[4])
		}
	} else {
		fields := make(map[string]interface{})
		for k, v := range t.props {
			fields[k] = v
		}
		log := xlog.WithCataLog("orm").WithFields(fields)
		log.Info(v...)
	}
}
