package orm

import (
	"fmt"
	"strconv"
	"time"

	"gitee.com/nee-team/nimble/orm/internal"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// Init 初始化
//
// dialect -别名,如mysql,oracle
// Init("mysql", "GO_TESTDB", "localhost", "root", "password", 3306)
//
// The returned DB is safe for concurrent use by multiple goroutines
// and maintains its own pool of idle connections. Thus, the Open
// function should be called just once. It is rarely necessary to
// close a DB.
func Init(conf *DbSettings) error {
	defaultKey = conf.DbName
	return InitDBWithDbName(conf, conf.DbName)
}

// InitDBWithDbName 注册多个数据库连接
func InitDBWithDbName(conf *DbSettings, dbName string) error {
	pwd, err := internal.Decrypt(conf.Password, conf.Key)
	if err != nil {
		return errors.Wrapf(err, "密码解密错误")
	}
	//如果是家里的数据库，注释上面部分
	// pwd := conf.Password
	internal.DbSetting.Dialect = conf.Dialect
	internal.DbSetting.DbName = conf.DbName
	internal.DbSetting.Host = conf.Host
	internal.DbSetting.User = conf.User
	internal.DbSetting.Password = pwd
	internal.DbSetting.Port = conf.Port

	// 初始化数据库可以在程序中动态调用，需要防止并发
	chk := CheckDbExists(dbName)
	if chk == true {
		return nil
	}

	maxIdel := 5
	if conf.MaxIdleConns > 0 {
		maxIdel = conf.MaxIdleConns
	}
	maxConn := 20
	if conf.MaxOpenConns > 0 {
		maxConn = conf.MaxOpenConns
	}
	lifeTime := 5
	if conf.ConnMaxLifetime > 0 {
		lifeTime = conf.ConnMaxLifetime
	}

	internal.DbSetting.MaxIdleConns = maxIdel
	internal.DbSetting.MaxOpenConns = maxConn

	p := strconv.Itoa(conf.Port)
	var source string
	switch conf.Dialect {
	case "mysql":
		source = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4,utf8&parseTime=true&loc=Local", conf.User, pwd, conf.Host, p, conf.DbName)
	case "mssql":
		source = fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s&connection+timeout=600&encrypt=disable", conf.User, pwd, conf.Host, p, conf.DbName)
	default:
		return fmt.Errorf("不支持的数据库类型：%s", conf.Dialect)
	}

	db, err := gorm.Open(conf.Dialect, source)
	if err != nil {
		return errors.Wrapf(err, "无法打开数据库连接")
	}

	db.Set("gorm:table_options", "CHARSET=utf8mb4")
	db.SingularTable(true)
	db.LogMode(true)
	db.DB().SetConnMaxLifetime(time.Duration(240) * time.Second)
	db.DB().SetMaxIdleConns(maxIdel)
	db.DB().SetMaxOpenConns(maxConn)

	// https://studygolang.com/topics/5576
	// go-sql-driver发生invalid connection(bad connection)解决方法
	// 因为beego没有调用db.SetConnMaxLifetime 这个方法，导致客户端保持了已经关闭的链接，
	// 修改 addAliasWthDB 方法，在方法里面，
	// 添加 db.SetConnMaxLifetime(time.Second)，time.Second 为数据库闲链接超时时间，
	// 而这个时间应该小于数据库设置闲时链接超时时间
	db.DB().SetConnMaxLifetime(time.Minute * time.Duration(lifeTime))

	// 防止全量更新和删除
	db.BlockGlobalUpdate(true)
	// 设置日志
	lg := new(DBLogWriter)
	db.SetLogger(Logger{lg})

	// 保存DB实例
	setDbInstance(dbName, db)

	return nil
}
