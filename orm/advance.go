package orm

import (
	"github.com/hashicorp/go-multierror"
	"github.com/jinzhu/gorm"
)

// UpdateItem 更新一行数据
// entityPtr 数据库实体 引用类型
// cols 待更新的数据库字段 e.g []string{"email", "address"}
// var item1 model.TbOrderFilterLog
// item1.ID = 1
// item1.UUID = "xxx"
// UPDATE `mpv_order_filter_log` SET `order_mode` = 'xxxx', `uuid` = 'xxx'  WHERE `id` = 1
// qa.UpdateItem(&item1,[]string{"uuid","order_mode"})
func (t *FiDB) UpdateItem(entityPtr interface{}, cols []string) error {
	if t.hasUpdateItem  {
		panic("已经调用UpdateItem方法")
	}
	t.hasUpdateItem = true

	Validate(entityPtr, "structPtr", false)

	clone := t.db.Model(entityPtr).Select(cols).UpdateColumns(entityPtr)

	if clone.Error != nil && clone.Error != gorm.ErrRecordNotFound {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	t.RowsAffected = clone.RowsAffected

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// InsertItem 插入一条记录
// entityPtr 数据实体 引用类型
// 例子: var user model.User
// qa := fiorm.NewQuery()
// qa.InsertItem(&user)
func (t *FiDB) InsertItem(entityPtr interface{}) error {
	if t.hasInsertItem {
		panic("已经调用InsertItem方法")
	}
	t.hasInsertItem = true

	Validate(entityPtr, "structPtr", true)

	clone := t.db.Create(entityPtr)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	t.RowsAffected = clone.RowsAffected

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// GetItemWhereFirst 根据条件查询一条数据
// entityPrt 待返回的数据实体 引用类型
// query 查询条件 e.g "name =? and address = ?"
// args 查询条件对应的值，字符串数组 "张三","深圳"
// 例子：
// 查看第一条符合条件的记录，按ID排序
// var item1 model.TbOrderFilterLog
// qa := fiorm.NewQuery()
// SELECT * FROM `mpv_order_filter_log`  WHERE (category_id = 2) ORDER BY `id` ASC LIMIT 1
// qa.GetItemWhereFirst(&item1,"category_id = ?",2)
func (t *FiDB) GetItemWhereFirst(entityPrt interface{}, query interface{}, args ...interface{}) error {
	if t.hasGetItemWhereFirst  {
		panic("已经调用GetItemWhereFirst方法")
	}
	t.hasGetItemWhereFirst = true

	clone := t.db.Where(query, args...).First(entityPrt)

	if clone.Error != nil && clone.Error != gorm.ErrRecordNotFound {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	t.RowsAffected = clone.RowsAffected

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// GetItemWhere 查询符合条件的多条记录
// var items []model.OrderFilterData
// qa := fiorm.NewQuery()
// SELECT * FROM `mpv_order_filter_data`  WHERE (category_id = 2)
// qa.GetItemWhere(&items,"category_id = ?",2)
func (t *FiDB) GetItemWhere(entitiesPrt interface{}, query interface{}, args ...interface{}) error {
	if t.hasGetItemWhere  {
		panic("已经调用GetItemWhere方法")
	}
	t.hasGetItemWhere = true

	clone := t.db.Where(query, args...).Find(entitiesPrt)

	if clone.Error != nil && clone.Error != gorm.ErrRecordNotFound {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	t.RowsAffected = clone.RowsAffected

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// BatchUpdate 批量更新,ID不能为空
// entities 待更新的实体数组slice
// cols 待更新的数据库字段 e.g []string{"email", "address"}
// 例子：var users []model.User
// 根据ID批量更新
//var items []model.TbOrderFilterLog
//
//var item1 model.TbOrderFilterLog
//item1.ID = 1
//item1.UUID = "xxx1"
//item1.FilterMode = "top"
//var item2 model.TbOrderFilterLog
//item2.ID = 2
//item2.UUID = "xxx2"
//
//items = append(items,item1)
//items = append(items,item2)
//
//qa := fiorm.NewQuery()
//// UPDATE `mpv_order_filter_log` SET  `uuid` = CASE id when 1 then 'xxx1' when 2 then 'xxx2' END, `filter_mode` = CASE id when 1 then 'top' when 2 then '' END WHERE id IN (1,2)
//qa.BatchUpdate(items,[]string{"uuid","filter_mode"})
func (t *FiDB) BatchUpdate(entities interface{}, cols []string) error {

	err := Validate(entities, "slice", false)
	if err != nil {
		return err
	}

	if len(cols) == 0 {
		panic("更新字段不能为空")
	}

	// 启动事务，防止部分更新
	if t.isTranction {
		tx := BeginTransactionWithDbName(t.dbName)
		t.tx = tx
		defer tx.EndTransaction()
		if t.customLogger != nil {
			tx.SetLoger(t.customLogger)
		}
		da := tx.NewQuery()
		da.batchUpdate(entities, cols)
		if da.Error != nil {
			t.Error = da.Error
		}
	} else {
		t.batchUpdate(entities, cols)
	}

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// DynamicBatchUpdate 批量更新自定义表
// tableName 表名
// entities 待更新的实体数组slice
// cols 待更新的数据库字段 e.g []string{"email", "address"}
// 例子：var users []CustomUser
// qa.DynamicBatchUpdate("mpv_user",users, []string{"desc", "address"})
func (t *FiDB) DynamicBatchUpdate(tableName string, entities interface{}, cols []string) error {

	err := Validate(entities, "slice", false)
	if err != nil {
		return err
	}

	if len(cols) == 0 {
		panic("更新字段不能为空")
	}

	// 启动事务，防止部分更新
	if t.isTranction {
		tx := BeginTransactionWithDbName(t.dbName)
		t.tx = tx
		defer tx.EndTransaction()
		if t.customLogger != nil {
			tx.SetLoger(t.customLogger)
		}
		da := tx.NewQuery()
		da.batchUpdateWithTableName(tableName, entities, cols)
		if da.Error != nil {
			t.Error = da.Error
		}
	} else {
		t.batchUpdateWithTableName(tableName, entities, cols)
	}

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// BatchInsert 批量插入
// entities 待更新的实体数组slice, ID不能为空
// 例子：var users []model.User
// qa.BatchInsert(users)
func (t *FiDB) BatchInsert(entities interface{}) error {

	err := Validate(entities, "slice", true)
	if err != nil {
		return err
	}

	if t.isTranction {
		tx := BeginTransactionWithDbName(t.dbName)
		defer tx.EndTransaction()
		if t.customLogger != nil {
			tx.SetLoger(t.customLogger)
		}
		da := tx.NewQuery()
		da.batchInsert(entities)
		if da.Error != nil {
			tx.Error = da.Error
			t.Error = da.Error
		}

	} else {
		t.batchInsert(entities)
	}

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// DynamicBatchInsert 批量新增自定义表
// tableName 表名
// entities 待更新的实体数组slice
// 例子：var users []CustomUser
// qa.DynamicBatchInsert("mpv_user",users)
func (t *FiDB) DynamicBatchInsert(tableName string, entities interface{}) error {

	err := Validate(entities, "slice", true)
	if err != nil {
		return err
	}

	if t.isTranction   {
		tx := BeginTransactionWithDbName(t.dbName)
		defer tx.EndTransaction()
		if t.customLogger != nil {
			tx.SetLoger(t.customLogger)
		}
		da := tx.NewQuery()
		da.batchInsertWithTableName(tableName, entities)
		if da.Error != nil {
			tx.Error = da.Error
			t.Error = da.Error
		}

	} else {
		t.batchInsertWithTableName(tableName, entities)
	}

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// ForUpdate 行锁
// 例子：var users []model.User
// da.Table("user").Where("name = ?", "张三"). ForUpdate().Find(&users)
// SQL: SELECT name, birthday FROM `user` WHERE (name = '张三') FOR UPDATE
func (t *FiDB) ForUpdate() *FiDB {
	if t.hasForUpdate {
		panic("已经调用过ForUpdate方法")
	}
	t.hasForUpdate = true

	clone := t.db.InstantSet("gorm:query_option", "FOR UPDATE")

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.db = clone

	return t
}

// ExecuteTextQuery 原生语法查询
// entitiesPrt 实体数组 引用类型
// sql 原生SQL
// values SQL占位符中的变量值
// 例子：var userList []model.UserView
// da.ExecuteTextQuery(&userList,
//    "SELECT u.id,u.name, dept_name FROM department t ,user u WHERE u.dept_id=t.id AND u.ID >=?",12)
func (t *FiDB) ExecuteTextQuery(entitiesPrt interface{}, sql string, values ...interface{}) error {
	if t.hasRaw {
		panic("已经调用过Raw方法")
	}
	t.hasRaw = true

	d := t.db.Raw(sql, values...)
	if d.Error != nil && d.Error != gorm.ErrRecordNotFound {
		t.Error = d.Error
	}

	d2 := d.Scan(entitiesPrt)
	if d2.Error != nil && d2.Error != gorm.ErrRecordNotFound {
		t.Error = d2.Error
	}

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}
	t.RowsAffected = d2.RowsAffected

	return t.Error
}

// 获取记录总行数
// tableName 表名
// query 查询语句
// values SQL占位符中的变量值
// 例子：count := da.GetRecordCount("user","name = ?","张三")
// SELECT count(*) FROM `user`  WHERE (name = '张三')
func (t *FiDB) GetRecordCount(tableName string, query interface{}, values ...interface{}) int64 {
	if t.hasGetRecordCount {
		panic("已经调用GetRecordCount方法")
	}
	t.hasGetRecordCount = true

	var count int64

	// SELECT count(*) FROM `user`  WHERE (name = 'wins')
	t.db.Table(tableName).Where(query, values...).Count(&count)

	return count
}
