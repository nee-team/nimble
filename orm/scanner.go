package orm

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

// ExecuteTable 返回map
func (t *FiDB) ScanMap(sql string, values ...interface{}) ([]map[string]string, error) {
	if t.hasRaw == true {
		panic("已经调用过Raw方法")
	}
	t.hasRaw = true

	var results []map[string]string
	rows, err := t.db.Raw(sql, values...).Rows()
	defer rows.Close()
	if err != nil && err != gorm.ErrRecordNotFound {
		panic(err)
	}
	cols, err := rows.Columns()
	if err != nil {
		panic(err)
	}

	rawResult := make([][]byte, len(cols))

	dest := make([]interface{}, len(cols))
	for i, _ := range rawResult {
		dest[i] = &rawResult[i]
	}

	for rows.Next() {
		m := make(map[string]string)

		err = rows.Scan(dest...)
		if err != nil && err != gorm.ErrRecordNotFound {
			fmt.Println("Failed to scan row", err)
			return nil, err
		}
		for i, colName := range cols {
			raw := rawResult[i]
			if raw == nil {
				m[colName] = ""
			} else {
				m[colName] = string(raw)
			}
		}
		results = append(results, m)
	}

	t.RowsAffected = int64(len(results))

	return results, err
}

// 传入SQL表达式，返回map
func (t *FiDB) ScanObject(sql string, values ...interface{}) ([]map[string]interface{}, error) {
	if t.hasRaw == true {
		panic("已经调用过Raw方法")
	}
	t.hasRaw = true

	var results []map[string]interface{}
	rows, err := t.db.Raw(sql, values...).Rows()
	defer rows.Close()
	if err != nil && err != gorm.ErrRecordNotFound {
		panic(err)
	}
	cols, err := rows.Columns()
	if err != nil {
		panic(err)
	}

	rawResult := make([][]byte, len(cols))

	dest := make([]interface{}, len(cols))
	for i, _ := range rawResult {
		dest[i] = &rawResult[i]
	}

	for rows.Next() {
		m := make(map[string]interface{})

		err = rows.Scan(dest...)
		if err != nil && err != gorm.ErrRecordNotFound {
			fmt.Println("Failed to scan row", err)
			return nil, err
		}
		for i, colName := range cols {
			raw := rawResult[i]
			if raw == nil {
				m[colName] = ""
			} else {
				m[colName] = string(raw)
			}
		}
		results = append(results, m)
	}

	t.RowsAffected = int64(len(results))

	return results, err
}
