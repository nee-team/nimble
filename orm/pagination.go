package orm

import (
	"math"
)

// Param 分页参数
type Param struct {
	Page    int      // 当前页
	Limit   int      // 每页记录数
	OrderBy []string // 排序 []string{"id asc","name desc"}
}

// Paginator 分页返回
type Paginator struct {
	TotalRecord int `json:"total_record"` // 总记录数
	TotalPage   int `json:"total_page"`   // 总页数
	Offset      int `json:"offset"`       // 偏移量
	Limit       int `json:"limit"`        // 每页记录数
	Page        int `json:"page"`         // 当前页
	PrevPage    int `json:"prev_page"`    // 前一页
	NextPage    int `json:"next_page"`    // 下一页
}

// Pagging 分页
func (t *FiDB) Pagging(tEntity interface{}, p *Param) *Paginator {
	if t.hasPagging == true {
		panic("已经调用过Pagging方法")
	}
	t.hasPagging = true

	if p.Page < 1 {
		p.Page = 1
	}
	if p.Limit == 0 {
		p.Limit = 10
	}
	if len(p.OrderBy) > 0 {
		for _, o := range p.OrderBy {
			t.db = t.db.Order(o)
		}
	}

	var paginator Paginator
	var count int
	var offset int

	t.db.Model(tEntity).Count(&count)

	if p.Page == 1 {
		offset = 0
	} else {
		offset = (p.Page - 1) * p.Limit
	}

	t.db.Limit(p.Limit).Offset(offset).Find(tEntity)

	paginator.TotalRecord = count
	paginator.Page = p.Page

	paginator.Offset = offset
	paginator.Limit = p.Limit
	paginator.TotalPage = int(math.Ceil(float64(count) / float64(p.Limit)))

	if p.Page > 1 {
		paginator.PrevPage = p.Page - 1
	} else {
		paginator.PrevPage = p.Page
	}

	if p.Page == paginator.TotalPage {
		paginator.NextPage = p.Page
	} else {
		paginator.NextPage = p.Page + 1
	}
	return &paginator
}
