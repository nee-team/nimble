package internal

import (
	"encoding/hex"

	"github.com/gtank/cryptopasta"
)

var key = "101232131012312aa34353463&^^%$#312&sdfsdf"

// DbSetting 数据库连接字符串属性
var DbSetting struct {
	Dialect      string
	DbName       string
	Host         string
	User         string
	Password     string
	Port         int
	MaxOpenConns int
	MaxIdleConns int
}

// Decrypt  解密
func Decrypt(text string, key string) (string, error) {
	k := &[32]byte{}
	copy(k[:], key)
	btext, _ := hex.DecodeString(text)
	ciphertext, err := cryptopasta.Decrypt(btext, k)
	if err != nil {
		return "", err
	}
	encodeText := string(ciphertext[:])
	return encodeText, nil
}

// Decrypt  加密
func Encrypt(text string, key string) (string, error) {
	k := &[32]byte{}
	copy(k[:], key)
	btext, _ := hex.DecodeString(text)
	ciphertext, err := cryptopasta.Encrypt(btext, k)
	if err != nil {
		return "", err
	}
	encodeText := string(ciphertext[:])
	return encodeText, nil
}
