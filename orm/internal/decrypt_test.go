package internal

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

// var key = "101232131012312aa34353463&^^%$#312&sdfsdf"
var key1 = "n@DHcSsu>BhaV/-"

// Decrypt  加密
func Test_Encrypt(t *testing.T) {
	Convey("TestEncrypt", t, func() {
		// Convey("数据库", func() {
		// 	var cipherText = "conwin@1234"
		// 	out_cipherText, err := Encrypt(cipherText, key1)
		// 	if err != nil {
		// 		fmt.Println(err)
		// 	} else {
		// 		fmt.Printf("%#v \r\n", out_cipherText)
		// 	}
		// })

		Convey("数据库", func() {
			var cipherText = "BFDo*4yssHbB"
			// var en_key = "n@DHcSsu>BhaV/-"
			out_cipherText, err := Encrypt(cipherText, key1)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Printf("%#v \r\n", out_cipherText)
			}
		})
	})
}

//解密
func Test_Decrypt(t *testing.T) {
	Convey("TestEncrypt", t, func() {
		Convey("数据库", func() {
			var cipherText = "897322ba737138e8b33d58261cf7350c43721c094d3d78f121a0184117a7ed5c563ca891ff65540c" 
			out_cipherText, err := Decrypt(cipherText, key1)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Printf("%#v \r\n", out_cipherText)
			}
		})
	})
}
