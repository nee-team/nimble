package orm

import (
	"fmt"
	"testing"
)

var key = "n@DHcSsu>BhaV/-"

func TestEncrypt(t *testing.T) {
	// var cipherText = "BFDo*4yssHbB"
	var cipherText = "conwin@1234"
	out_cipherText, err := Encrypt(cipherText, key)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%#v \r\n", out_cipherText)
	}

}
