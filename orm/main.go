package orm

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"strconv"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/hashicorp/go-multierror"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"

	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xlog"
)

var defaultKey string

// 连接实例池
var dbInstances = struct {
	sync.RWMutex
	mdb map[string]*gorm.DB
}{mdb: make(map[string]*gorm.DB)}

// DB 返回一个连接池的实例
// var db *gorm.DB

func getDbInstance(key string) *gorm.DB {
	dbInstances.RLock()
	defer dbInstances.RUnlock()

	db := dbInstances.mdb[key]
	return db
}

func CheckDbExists(key string) bool {
	dbInstances.RLock()
	defer dbInstances.RUnlock()

	_, ok := dbInstances.mdb[key]
	return ok
}

func defaultDb() *gorm.DB {
	dbInstances.RLock()
	defer dbInstances.RUnlock()

	dbInst, ok := dbInstances.mdb[defaultKey]

	if ok {
		return dbInst
	} else {
		panic("没有发现DB实例" + defaultKey)
	}
}

//保存Db实例
func setDbInstance(key string, db *gorm.DB) {
	dbInstances.Lock()
	defer dbInstances.Unlock()

	if _, ok := dbInstances.mdb[key]; ok == false {
		dbInstances.mdb[key] = db
	}
}

// RemoveDbByKey 移除DB实例
func RemoveDbByKey(key string) {
	dbInstances.RLock()
	defer dbInstances.RUnlock()

	_, ok := dbInstances.mdb[key]
	if ok == true {
		delete(dbInstances.mdb, key)
	}
}

// FiDB 数据表操作类
type FiDB struct {
	db               *gorm.DB // 局部
	dbName           string
	tx               *FiTX
	Error            error
	isTranction      bool  // 最外层是否已经开启事务，如果开启，批量插入或更新则取消
	RowsAffected     int64 // 受影响行数
	hasTable         bool  // 是否已经调用过Table()
	hasSelect        bool  // 是否已经调用过Select()
	hasScan          bool  // 是否已经调用过Scan()
	hasRaw           bool  // 是否已经调用过Raw()
	hasPluck         bool  //是否已经调用过Pluck()
	hasGroup         bool  //是否已经调用过Group()
	hasCount         bool  //是否已经调用过Count()
	hasLimit         bool  //是否已经调用过Limit()
	hasOffset        bool  //是否已经调用过Offset()
	hasFind          bool  //是否已经调用过Find()
	hasFirst         bool  //是否已经调用过First()
	hasTake          bool  //是否已经调用过Take()
	hasForUpdate     bool  //是否已经调用过ForUpdate()
	hasDelete        bool  //是否已经调用过Delete()
	hasModel         bool  //是否已经调用过Model()
	hasUpdateColumns bool  //是否已经调用过UpdateColumns()
	hasUpdateColumn  bool  //是否已经调用过UpdateColumn()
	hasUpdates       bool  //是否已经调用过Updates()
	hasUpdate        bool  //是否已经调用过Update()
	hasPagging       bool  //是否已经调用过Pagging()
	hasUpdateItem    bool  //是否已经调用过UpdateItem()
	hasInsertItem    bool  //是否已经调用过InsertItem()

	hasGetItemWhere      bool //是否已经调用过GetItemWhere()
	hasGetItemWhereFirst bool //是否已经调用过GetItemWhereFirst()
	hasGetRecordCount    bool //是否已经调用过GetRecordCount()

	customLogger logger // 自定义日志
}

// FiTX 数据表操作类
type FiTX struct {
	db             *gorm.DB // 局部
	dbName         string
	Error          error
	rollbackCalled bool // 校验是否调用了Rollback函数
	commitCalled   bool // 校验是否调用了Commit函数
	endTransaction bool //
	//事务关联上下文
	txCtx        FiTXCtx
	rasseContext xcontext.XContext
	// 事务结束调用函数
	EndFunc func(fiTXCtx FiTXCtx)

	customLogger logger // 自定义日志
}

// FiTXCtx 事务上下文数据
type FiTXCtx struct {
	// 启动事务打开连接时间
	ConnectingTime time.Time
	// 启动事务打开连接成功时间
	ConnectedTime time.Time
	// 事务结束提交时间
	ExecutingTime time.Time
	// 事务结束提交完成时间
	ExecutedTime time.Time
	// 事务处理结果(Commit/Rollback)
	TXResult string
}

// DbSettings 数据库连接字符串属性
type DbSettings struct {
	Dialect         string
	DbName          string
	Host            string
	User            string
	Password        string
	Port            int
	Key             string
	MaxOpenConns    int
	MaxIdleConns    int
	ConnMaxLifetime int // 分钟
}

func SetTablePrefix(prefix string) {
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return prefix + defaultTableName
	}
}

// BeginTransaction 开始事务
func BeginTransaction() *FiTX {
	var tx = new(FiTX)
	// 启动时间
	tx.txCtx.ConnectingTime = time.Now()
	tx.db = defaultDb().Begin()
	// 开始时间
	tx.txCtx.ConnectedTime = time.Now()
	tx.dbName = defaultKey
	return tx
}

func BeginDBTX(ctx xcontext.XContext) *FiTX {
	var tx = new(FiTX)
	// 启动时间
	tx.txCtx.ConnectingTime = time.Now()
	tx.db = defaultDb().Begin()
	// 开始时间
	tx.txCtx.ConnectedTime = time.Now()
	tx.dbName = defaultKey
	tx.rasseContext = ctx
	var logger = initLogByContext(ctx)
	tx.SetLoger(Logger{logger})
	return tx
}

func initLogByContext(ctx xcontext.XContext) LogWriter {
	var log = new(DBLogWriter)
	var fields = xcontext.ToMap(ctx)
	for k, v := range fields {
		log.SetProperty(k, v)
	}
	return log
}

// BeginTransactionWithDbName 创建多个数据库事务连接
func BeginTransactionWithDbName(dbName string) *FiTX {
	var tx = new(FiTX)
	// 启动时间
	tx.txCtx.ConnectingTime = time.Now()
	tx.db = getDbInstance(dbName).Begin()
	// 开始时间
	tx.txCtx.ConnectedTime = time.Now()
	tx.dbName = dbName
	return tx
}

func BeginNamedDBTX(dbName string, ctx xcontext.XContext) *FiTX {
	var tx = new(FiTX)
	// 启动时间
	tx.txCtx.ConnectingTime = time.Now()
	tx.db = getDbInstance(dbName).Begin()
	// 开始时间
	tx.txCtx.ConnectedTime = time.Now()
	tx.dbName = dbName
	tx.rasseContext = ctx
	var logger = initLogByContext(ctx)
	tx.SetLoger(Logger{logger})
	return tx
}

// region 设置日志组件
func (t *FiTX) SetLoger(log logger) {
	t.customLogger = log
	t.db.SetLogger(log)
}
func (t *FiDB) SetLoger(log logger) {
	t.customLogger = log
	t.db.SetLogger(log)
}

// endregion

// EndTransaction 结束事务
func (t *FiTX) EndTransaction() {
	if t.endTransaction == true {
		panic("不能重复执行EndTransaction")
	}

	if t.EndFunc != nil {
		// EndFunc 必须放置func里面(闭包),否则txCtx后面的无效,不会传递到EndFunc参数中
		defer func() {
			t.EndFunc(t.txCtx)
		}()
	}

	defer func() {
		{
			//整个耗时
			allDuration := t.txCtx.ExecutedTime.Sub(t.txCtx.ConnectingTime)
			txAllDuration := int(math.Ceil(float64(allDuration.Nanoseconds()) / 1000000.0))
			//连接耗时
			connDuration := t.txCtx.ConnectedTime.Sub(t.txCtx.ConnectingTime)
			txConnDuration := int(math.Ceil(float64(connDuration.Nanoseconds()) / 1000000.0))

			//从打开连接到提交完成耗时
			innerDuration := t.txCtx.ExecutingTime.Sub(t.txCtx.ConnectedTime)
			txInnerDuration := int(math.Ceil(float64(innerDuration.Nanoseconds()) / 1000000.0))

			//commit
			commitDuration := t.txCtx.ExecutedTime.Sub(t.txCtx.ExecutingTime)
			txCommittDuration := int(math.Ceil(float64(commitDuration.Nanoseconds()) / 1000000.0))

			fields := map[string]interface{}{
				"result":           t.txCtx.TXResult,
				"duration":         txAllDuration,
				"connecting_time":  t.txCtx.ConnectingTime,
				"connected_time":   t.txCtx.ConnectedTime,
				"executing_time":   t.txCtx.ExecutingTime,
				"executed_time":    t.txCtx.ExecutedTime,
				"connect_duration": txConnDuration,
				"commit_duration":  txCommittDuration,
				"inner_duration":   txInnerDuration,
			}

			log := xlog.WithCataLog("orm").WithFields(fields)
			if t.rasseContext != nil {
				log = log.WithContext(t.rasseContext)
			}
			log.Info(t.txCtx.TXResult)
		}
	}()

	//开始提交时间
	t.txCtx.ExecutingTime = time.Now()

	if r := recover(); r != nil {
		fmt.Println("recover...", r)
		t.Error = errors.New(fmt.Sprint(r))
		t.db.Rollback()
		//事务执行完成时间
		t.txCtx.ExecutedTime = time.Now()
		t.txCtx.TXResult = "Rollback"
		// 抛出异常到外层处理
		panic(r)
	}

	if t.Error != nil && t.Error != gorm.ErrRecordNotFound {
		fmt.Println("rollback...", t.Error)
		t.db.Rollback()
		t.txCtx.TXResult = "Rollback"
	} else {
		if t.rollbackCalled == true {
			fmt.Println("rollback...")
			t.db.Rollback()
			t.txCtx.TXResult = "Rollback"
		} else {
			fmt.Println("commit...")
			t.db.Commit()
			t.txCtx.TXResult = "Commit"
		}
	}
	//事务执行完成时间
	t.txCtx.ExecutedTime = time.Now()
	t.endTransaction = true
}

func (t *FiTX) RollbackTranction() {
	if t.commitCalled == true {
		panic("数据库已经提交，不允许Rollback操作")
	}
	t.rollbackCalled = true
}

func (t *FiTX) Commit() {
	if t.rollbackCalled == true {
		panic("数据库已经回滚，不允许提交操作")
	}
	t.commitCalled = true
}

// NewQuery 获取一个查询会话（只作用于一次查询）
func (t *FiTX) NewQuery() *FiDB {
	var da = new(FiDB)
	da.db = t.db.New()
	da.isTranction = true
	da.tx = t
	return da
}

// Query 获取一个新的数据库连接
func NewQueryWithDbName(dbName string) *FiDB {
	var da = new(FiDB)
	da.db = getDbInstance(dbName).New()
	da.dbName = dbName
	da.tx = new(FiTX)
	return da
}

// NewQuery 获取一个查询会话（只作用于一次查询）
func NewQuery() *FiDB {
	var da = new(FiDB)
	da.db = defaultDb().New()
	da.dbName = defaultKey
	da.tx = new(FiTX)
	return da
}

// NewQuery 获取一个查询会话（只作用于一次查询）
func NewDB(ctx xcontext.XContext) *FiDB {
	var da = new(FiDB)
	da.db = defaultDb().New()
	da.dbName = defaultKey
	da.tx = new(FiTX)
	da.tx.rasseContext = ctx
	var logger = initLogByContext(ctx)
	da.SetLoger(Logger{logger})
	return da
}

// Query 获取一个新的数据库连接
func NewNamedDB(dbName string, ctx xcontext.XContext) *FiDB {
	var da = new(FiDB)
	da.db = getDbInstance(dbName).New()
	da.dbName = dbName
	da.tx = new(FiTX)
	da.tx.rasseContext = ctx
	var logger = initLogByContext(ctx)
	da.SetLoger(Logger{logger})
	return da
}

// WithLogTraceID 向 sql 日志输出 trace_id（自定义信息）
func (db *FiDB) WithLogTraceID(traceID string) *FiDB {
	return db.WithLogFields(map[string]string{"trace_id": traceID})
}

// WithLogFields 向 sql 日志输出自定义信息
func (db *FiDB) WithLogFields(fields map[string]string) *FiDB {
	lg := db.GetLogWriter()
	for k, v := range fields {
		lg.SetProperty(k, v)
	}
	return db
}

// GetLogWriter 获取日志输出对象。用户可以调用该对象的 SetProperty 方法为日志输出自定义信息
func (db *FiDB) GetLogWriter() *DBLogWriter {
	if db.customLogger == nil {
		lg := new(DBLogWriter)
		db.SetLoger(Logger{lg})
		return lg
	} else {
		return db.customLogger.(Logger).LogWriter.(*DBLogWriter)
	}
}

// NewRecord 创建记录
// user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
// db.NewRecord(user) // => 主键为空返回`true`
func (t *FiDB) NewRecord(entity interface{}) bool {
	return t.db.NewRecord(entity)
}

// RecordNotFound check if returning ErrRecordNotFound error
// var item model.TbOrderFilterLog
// qa := fiorm.NewQuery()
// qa.GetItemWhereFirst(&item,"id = ?",157)
// fmt.Println(qa.RecordNotFound())
func (t *FiDB) RecordNotFound() bool {
	return t.db.RecordNotFound()
}

// Update 更新单个字段
// var item model.TbOrderFilterLog
// qa := fiorm.NewQuery()
//// UPDATE `mpv_order_filter_log` SET `order_mode` = 'xxx'  WHERE (id = 157)
// qa.Model(&item).Where("id = ?",157).Update("order_mode","xxx")
func (t *FiDB) Update(attrs ...interface{}) error {
	if t.hasUpdate == true {
		panic("已经调用Update方法")
	}
	t.hasUpdate = true

	clone := t.db.Update(attrs...)
	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}
	t.RowsAffected = clone.RowsAffected
	t.db = clone
	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}
	return t.Error
}

// 使用`map`更新多个属性，只会更新这些更改的字段
// db.Model(&user).Updates(map[string]interface{}{"name": "hello", "age": 18, "actived": false})
//// UPDATE users SET name='hello', age=18, actived=false, updated_at='2013-11-17 21:34:10' WHERE id=111;

// 使用`struct`更新多个属性，只会更新这些更改的和非空白字段
// db.Model(&user).Updates(User{Name: "hello", Age: 18})
//// UPDATE users SET name='hello', age=18, updated_at = '2013-11-17 21:34:10' WHERE id = 111;

// 警告:当使用struct更新时，FORM将仅更新具有非空值的字段
// 对于下面的更新，什么都不会更新为""，0，false是其类型的空白值
// db.Model(&user).Updates(User{Name: "", Age: 0, Actived: false})

// 如果您只想在更新时更新或忽略某些字段，可以使用Select, Omit
// db.Model(&user).Select("name").Updates(map[string]interface{}{"name": "hello", "age": 18, "actived": false})
//// UPDATE users SET name='hello', updated_at='2013-11-17 21:34:10' WHERE id=111;

// db.Model(&user).Omit("name").Updates(map[string]interface{}{"name": "hello", "age": 18, "actived": false})
//// UPDATE users SET age=18, actived=false, updated_at='2013-11-17 21:34:10' WHERE id=111;

// db.Table("users").Where("id IN (?)", []int{10, 11}).Updates(map[string]interface{}{"name": "hello", "age": 18})
//// UPDATE users SET name='hello', age=18 WHERE id IN (10, 11);

// 使用struct更新仅适用于非零值，或使用map[string]interface{}
// db.Model(User{}).Updates(User{Name: "hello", Age: 18})
//// UPDATE users SET name='hello', age=18;

// 使用`RowsAffected`获取更新记录计数
// db.Model(User{}).Updates(User{Name: "hello", Age: 18}).RowsAffected
func (t *FiDB) Updates(value interface{}) error {
	if t.hasUpdates == true {
		panic("已经调用Updates方法")
	}
	t.hasUpdates = true
	clone := t.db.Updates(value)
	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}
	t.RowsAffected = clone.RowsAffected
	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}
	return t.Error
}

// UpdateColumn update attributes without callbacks, refer: https://jinzhu.github.io/gorm/crud.html#update
// 更新单个属性，类似于`Update`
// db.Model(&user).UpdateColumn("name", "hello")
//// UPDATE users SET name='hello' WHERE id = 111;
func (t *FiDB) UpdateColumn(attrs ...interface{}) error {
	if t.hasUpdateColumn == true {
		panic("已经调用UpdateColumn方法")
	}
	t.hasUpdateColumn = true
	// TODO 校验错误(ID=0,空记录,格式错误)
	clone := t.db.UpdateColumn(attrs...)
	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}
	t.RowsAffected = clone.RowsAffected
	t.db = clone
	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}
	return t.Error
}

// UpdateColumns update attributes without callbacks, refer: https://jinzhu.github.io/gorm/crud.html#update
// 更新多个属性，与“更新”类似
// db.Model(&user).UpdateColumns(User{Name: "hello", Age: 18})
//// UPDATE users SET name='hello', age=18 WHERE id = 111;
func (t *FiDB) UpdateColumns(entity interface{}) error {
	if t.hasUpdateColumns == true {
		panic("已经调用UpdateColumns方法")
	}
	t.hasUpdateColumns = true

	clone := t.db.UpdateColumns(entity)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.RowsAffected = clone.RowsAffected

	t.db = clone

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// Create insert the value into database
// user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
// db.Create(&user)
func (t *FiDB) Create(entityPtr interface{}) error {

	Validate(entityPtr, "structPtr", true)

	clone := t.db.Create(entityPtr)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.RowsAffected = clone.RowsAffected

	t.db = clone

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// Delete delete value match given conditions, if the value has primary key, then will including the primary key as condition
// 删除存在的记录
// db.Delete(&email)
//// DELETE from emails where id=10;
// db.Where("email LIKE ?", "%jinzhu%").Delete(Email{})
//// DELETE from emails where email LIKE "%jinhu%";

// db.Delete(Email{}, "email LIKE ?", "%jinzhu%")
//// DELETE from emails where email LIKE "%jinhu%";

// qa.Where("id = ?",1).Delete(&item)
// DELETE FROM `mpv_order_filter_data`  WHERE (id = 1)
func (t *FiDB) Delete(row interface{}, where ...interface{}) error {
	if t.hasDelete == true {
		panic("已经调用过Delete方法")
	}
	t.hasDelete = true

	clone := t.db.Delete(row, where...)

	if clone.Error != nil {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.RowsAffected = clone.RowsAffected

	t.db = clone

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// 根据ID删除多条记录
// var items []model.TbOrderFilterLog
// DELETE FROM `mpv_order_filter_log`  WHERE (`mpv_order_filter_log`.`id` IN (1,2))
// qa.DeleteItemsByID(items)
func (t *FiDB) DeleteItemsByID(entities interface{}) error {
	kind := reflect.TypeOf(entities).Kind()
	if kind != reflect.Slice {
		panic("错误，参数不是数组")
	}

	raws := reflect.ValueOf(entities)
	if raws.Len() == 0 {
		return errors.New("没有记录")
	}

	var ids []int64
	for i := 0; i < raws.Len(); i++ {
		val := reflect.ValueOf(reflect.ValueOf(entities).Index(i).Interface())
		id := val.FieldByName("ID").Interface()

		idStr := fmt.Sprintf("%v", id)
		id64, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			panic("ID 值转换错误" + idStr)
		}
		if id64 == 0 {
			panic("ID 不能为0")
		}
		ids = append(ids, id64)
	}

	db1 := t.db.Where(ids).Delete(entities)
	if db1.Error != nil {
		t.Error = db1.Error
	}
	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// DeleteItem 删除一条或多条数据

// 删除一条数据
// var item1 model.TbOrderFilterLog
// item1.ID = 1
// DELETE FROM `mpv_order_filter_log`  WHERE `id` = 1
// qa.DeleteItem(&item1)

// 删除多条数据
// 必须要有where语句
// DELETE FROM `mpv_order_filter_log`  WHERE (category_id = 33)
// qa.Where("category_id = ?",33).DeleteItem(items)
func (t *FiDB) DeleteItem(entityPtrOrSlice interface{}) error {
	// 如果ID为空，不能删除
	kind := reflect.TypeOf(entityPtrOrSlice).Kind()
	if kind == reflect.Ptr {
		// 单行数据
		Validate(entityPtrOrSlice, "structPtr", false)
	} else {
		Validate(entityPtrOrSlice, "slice", false)
	}

	db1 := t.db.Delete(entityPtrOrSlice)
	if db1.Error != nil {
		t.Error = db1.Error
	}
	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// Exec execute raw sql
// sql := "update mpv_order_filter_log set order_mode='xxx' where id =3"
//// update mpv_order_filter_log set order_mode='xxx' where id =3
// qa.Exec(sql)
func (t *FiDB) Exec(sql string, values ...interface{}) error {
	if t.Error != nil {
		panic(t.Error)
	}

	clone := t.db.Exec(sql, values...)

	if clone.Error != nil && clone.Error != gorm.ErrRecordNotFound {
		t.Error = multierror.Append(t.Error, clone.Error)
	}

	t.RowsAffected = clone.RowsAffected

	t.db = clone

	if t.Error != nil {
		t.tx.Error = multierror.Append(t.tx.Error, t.Error)
	}

	return t.Error
}

// CreateTable create table for models
func CreateTable(models ...interface{}) {
	defaultDb().CreateTable(models...)
}

func CreateTableWithKey(dbName string, models ...interface{}) {
	getDbInstance(dbName).CreateTable(models...)
}

func GetTableStructInfo(tableName string) []TableStruct {
	var infoList []TableStruct

	qa := NewQuery()
	qa.ExecuteTextQuery(&infoList, "desc "+tableName)

	return infoList
}
