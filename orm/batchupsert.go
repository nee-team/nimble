package orm

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/hashicorp/go-multierror"
	"github.com/jinzhu/gorm"
)

// batchUpdate 批量更新
func (t *FiDB) batchUpdate(rows interface{}, cols []string) {
	t.upsert(rows, cols, "")
}

// batchInsert 批量插入
func (t *FiDB) batchInsert(rows interface{}) {
	t.upsert(rows, nil, "")
}

// batchUpdate 批量更新
func (t *FiDB) batchUpdateWithTableName(tbName string, rows interface{}, cols []string) {
	t.upsert(rows, cols, tbName)
}

// batchInsert 批量插入
func (t *FiDB) batchInsertWithTableName(tbName string, rows interface{}) {
	t.upsert(rows, nil, tbName)
}

func (t *FiDB) upsert(rows interface{}, cols []string, tbName string) {

	raws := reflect.ValueOf(rows)
	first := raws.Index(0).Interface()
	var st = new(gorm.ModelStruct)

	var tableName string
	if tbName == "" {
		st, tableName = t.getTableStruct(first)
	} else {
		st, _ = t.getTableStruct(first)
		tableName = tbName
	}

	// 分页处理，每次插入500条
	total := raws.Len()
	start := 0
	end := 500
	stop := false
	var values []interface{}
	var sqlStr string
	var totalEffectRow int64

	for j := 0; j < total; j++ {
		if !stop && start != total {
			if cols == nil {
				sqlStr, values, stop = generateSQL(st, tableName, rows, cols, start, end)
			} else {
				sqlStr, values, stop = GenerateUpdateSQL(st, tableName, rows, cols, start, end)
			}

			d := t.db.Exec(sqlStr, values...)
			totalEffectRow += d.RowsAffected
			if d.Error != nil {
				t.Error = multierror.Append(t.Error, d.Error)
			}
			start = end
			end = end + 500
		} else {
			break
		}
	}

	t.RowsAffected = totalEffectRow
}

func generateSQL(st *gorm.ModelStruct, tableName string, rows interface{}, cols []string, start int, end int) (string, []interface{}, bool) {
	var (
		column     string
		needColumn = true
		values     = []interface{}{}
		raw        = reflect.ValueOf(rows)
		sql        = "INSERT INTO %s ( %s ) VALUES "
	)

	fieldInfo := st.StructFields
	total := raw.Len()
	stop := false
	if end > total {
		end = total
		stop = true
	}

	for i := start; i < end; i++ {
		val := reflect.ValueOf(raw.Index(i).Interface())
		for val.Kind() == reflect.Ptr {
			val = val.Elem()
		}

		line := "("
		tp := reflect.Indirect(val).Type()
		for i := 0; i < val.NumField(); i++ {
			c := getFiled(fieldInfo, tp.Field(i).Name)
			if c == "" {
				continue
			}
			if needColumn {
				column += "`" + c + "`" + ","
			}
			line += "?,"
			values = append(values, val.Field(i).Interface())
		}
		line = strings.TrimSuffix(line, ",")
		line += "),"
		sql += line
		needColumn = false
	}

	sql = strings.TrimSuffix(sql, ",")
	column = strings.TrimSuffix(column, ",")

	// 构造更新语句
	if cols != nil {
		updateSQL := " on duplicate key update "
		for _, val := range cols {
			updateSQL += val + "=values(" + val + "),"
		}
		updateSQL = strings.TrimSuffix(updateSQL, ",")
		sql += updateSQL
	}

	return fmt.Sprintf(sql, tableName, column), values, stop
}

func (t *FiDB) getTableStruct(row interface{}) (*gorm.ModelStruct, string) {
	scope := t.db.NewScope(row)
	return scope.GetModelStruct(), scope.QuotedTableName()
}

func getPrimaryKeyName(fields []*gorm.StructField) (string, string, error) {
	var keys []string
	var colNames []string
	for _, f := range fields {
		if f.IsPrimaryKey {
			colNames = append(colNames, f.Name)
			keys = append(keys, f.DBName)
		}
	}

	if len(keys) == 1 {
		return keys[0], colNames[0], nil
	} else if len(keys) == 0 {
		return "", "", errors.New("没有发现主键")
	} else {
		return "", "", errors.New("批量操作不能有多个主键")
	}
}

func getFiled(fields []*gorm.StructField, name string) string {
	for _, f := range fields {
		if name == f.Name {
			if f.IsIgnored {
				return ""
			}
			return f.DBName
		}
	}
	return ""
}

/*
func (t *FiDB) BatchInsertMap(objArr []interface{}) error {
    // If there is no data, nothing to do.
    if len(objArr) == 0 {
        return nil
    }

    mainObj := objArr[0]
    mainScope := t.db.NewScope(mainObj)
    mainFields := mainScope.Fields()
    quoted := make([]string, 0, len(mainFields))
    for i := range mainFields {
        // If primary key has blank value (0 for int, "" for string, nil for interface ...), skip it.
        // If field is ignore field, skip it.
        if (mainFields[i].IsPrimaryKey && mainFields[i].IsBlank) || (mainFields[i].IsIgnored) {
            continue
        }
        quoted = append(quoted, mainScope.Quote(mainFields[i].DBName))
    }

    placeholdersArr := make([]string, 0, len(objArr))

    for _, obj := range objArr {
        scope := db.NewScope(obj)
        fields := scope.Fields()
        placeholders := make([]string, 0, len(fields))
        for i := range fields {
            if (fields[i].IsPrimaryKey && fields[i].IsBlank) || (fields[i].IsIgnored) {
                continue
            }
            placeholders = append(placeholders, scope.AddToVars(fields[i].Field.Interface()))
        }
        placeholdersStr := "(" + strings.Join(placeholders, ", ") + ")"
        placeholdersArr = append(placeholdersArr, placeholdersStr)
        // add real variables for the replacement of placeholders' '?' letter later.
        mainScope.SQLVars = append(mainScope.SQLVars, scope.SQLVars...)
    }

    mainScope.Raw(fmt.Sprintf("INSERT INTO %s (%s) VALUES %s",
        mainScope.QuotedTableName(),
        strings.Join(quoted, ", "),
        strings.Join(placeholdersArr, ", "),
    ))

    if _, err := mainScope.SQLDB().Exec(mainScope.SQL, mainScope.SQLVars...); err != nil {
        return err
    }
    return nil
}
*/
