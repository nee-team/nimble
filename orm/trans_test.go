package orm

import (
	"testing"

	"github.com/spf13/viper"

	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/xcontext"
	"gitee.com/nee-team/nimble/xhttp"
	"gitee.com/nee-team/nimble/xlog"
)

func TestTransaction(t *testing.T) {
	xctx := xcontext.NewTestContext()
	tx := BeginDBTX(xctx)
	defer tx.EndTransaction()
	da := tx.NewQuery()
	da.Exec("update mpv_marks set update_time = NOW() where id = 1")
	// 注：defer需要紧跟BeginTransaction

}

func TestMain(m *testing.M) {
	//设置输出样式，自带的只有两种样式logrus.JSONFormatter{}和logrus.TextFormatter{}
	initLog()
	config.Init()
	dbSettings := &DbSettings{
		Dialect:         viper.GetString("orm.db.dialect"),
		DbName:          viper.GetString("orm.db.dbname"),
		Host:            viper.GetString("orm.db.host"),
		Port:            viper.GetInt("orm.db.port"),
		User:            viper.GetString("orm.db.user"),
		Key:             viper.GetString("orm.db.key"),
		Password:        viper.GetString("orm.db.password"),
		MaxIdleConns:    viper.GetInt("orm.db.max_idle_conns"),
		MaxOpenConns:    viper.GetInt("orm.db.max_open_conns"),
		ConnMaxLifetime: viper.GetInt("orm.db.conn_max_lifetime"),
	}
	Init(dbSettings)
	m.Run()
}

func initLog() error {
	defer xlog.Sync()

	err := xlog.Init(&xlog.LogSettings{
		Level:    config.GetStringOrDefault("log.level", xlog.DefaultLevel),
		Path:     config.GetStringOrDefault("log.path", xlog.DefaultPath),
		FileName: config.GetStringOrDefault("log.filename", xlog.DefaultFileName),
		CataLog:  config.GetStringOrDefault("log.catalog", xlog.DefaultCataLog),
		Caller:   config.GetBoolOrDefault("log.caller", xlog.DefaultCaller),
	})
	if err == nil {
		xhttp.Init("invoke")
	}
	return err
}
