package orm

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/jinzhu/gorm"
)

func GenerateUpdateSQL(st *gorm.ModelStruct, tableName string, rows interface{}, cols []string, start int, end int) (string, []interface{}, bool) {
	var (
		values = []interface{}{}
		raw    = reflect.ValueOf(rows)
		sql    = "UPDATE " + tableName + " SET "
	)

	fieldInfo := st.StructFields

	primaryKey, fieldName, err := getPrimaryKeyName(fieldInfo)
	if err != nil {
		panic(err.Error())
	}

	total := raw.Len()
	stop := false
	if end > total {
		end = total
		stop = true
	}

	ids := " WHERE " + primaryKey + " IN ("
	index := 0
	for _, col := range cols {
		line := " `" + col + "` = CASE " + primaryKey + " "
		for i := start; i < end; i++ {
			val := reflect.ValueOf(raw.Index(i).Interface())
			for val.Kind() == reflect.Ptr {
				val = val.Elem()
			}
			id := val.FieldByName(fieldName).Interface()
			if index == 0 {
				ids += fmt.Sprintf("%v,", id)
			}

			tp := reflect.Indirect(val).Type()
			for i := 0; i < val.NumField(); i++ {
				c := getFiled(fieldInfo, tp.Field(i).Name)
				if c == "" {
					continue
				}
				if c == col {
					line += fmt.Sprintf("when %v then ? ", id)
					values = append(values, val.Field(i).Interface())
				}
			}
		}

		line += "END,"
		sql += line
		index++
	}
	sql = strings.TrimSuffix(sql, ",")
	ids = strings.TrimSuffix(ids, ",")
	sql += ids + ")"

	return sql, values, stop
}
