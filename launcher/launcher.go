package launcher

import (
	"fmt"
	"os"
	"time"

	"gitee.com/nee-team/nimble/app"
	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/env"
	"gitee.com/nee-team/nimble/orm"
	"gitee.com/nee-team/nimble/redis"
	"gitee.com/nee-team/nimble/router"
	"gitee.com/nee-team/nimble/xlog"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

//NewApp 构造新应用
func NewApp() *app.App {
	fmt.Println("Application starting...")
	err := config.Init()
	if err != nil {
		panic(errors.Wrap(err, "init configuration failed"))
	}
	// env.VerifyConfig()
	// 获取当前运行环境，若没有配置 CFG_ENV，则会异常退出
	// currentEnv := env.GetRuntimeEnv()
	// fmt.Printf("CFG_ENV: %s\n", currentEnv)
	instance := &app.App{
		Domain:    viper.GetString("app.domain"),
		Project:   viper.GetString("app.project"),
		Module:    viper.GetString("app.module"),
		StartTime: time.Now(),
	}
	app.Instance = instance
	instance.
		InitModule(app.ModuleLog, func() error {
			defer xlog.Sync()
			err := xlog.Init(&xlog.LogSettings{
				Level:       config.GetStringOrDefault("log.level", xlog.DefaultLevel),
				Path:        config.GetStringOrDefault("log.path", xlog.DefaultPath),
				FileName:    config.GetStringOrDefault("log.filename", xlog.DefaultFileName),
				CataLog:     config.GetStringOrDefault("log.catalog", xlog.DefaultCataLog),
				MaxFileSize: config.GetIntOrDefault("log.maxfilesize", xlog.DefaultMaxFileSize),
				MaxBackups:  config.GetIntOrDefault("log.maxbackups", xlog.DefaultMaxBackups),
				MaxAge:      config.GetIntOrDefault("log.maxage", xlog.DefaultMaxAge),
				Caller:      config.GetBoolOrDefault("log.caller", xlog.DefaultCaller),
			})
			return err
		}).
		InitModule(app.ModuleGin, func() error {
			ginMode := os.Getenv("GIN_MODE")
			if len(ginMode) == 0 {
				runtimeEnv := env.GetRuntimeEnv()
				switch runtimeEnv {
				case env.Development:
					gin.SetMode(gin.DebugMode)
				case env.Test:
					gin.SetMode(gin.TestMode)
				default:
					gin.SetMode(gin.ReleaseMode)
				}
			}
			// Creates a router without any middleware by default
			engine := gin.New()
			engine.Use(gin.Recovery())
			xlog.Infof("%s: %s\n", gin.EnvGinMode, gin.Mode())
			app.Instance.Engine = engine
			return nil
		}).
		// InitModule(app.MWXHTTP, initXhttp). // 需要在 app.ModuleMonitor 之后初始化
		// InitModuleIfEnabled("atq.enabled", app.ModuleATQ, func() error {
		// 	return atq.Init(&atq.Options{
		// 		AuthKey:      viper.GetString("atq.auth.key"),
		// 		AuthSecurity: viper.GetString("atq.auth.security"),
		// 		MsgSplitter:  viper.GetString("atq.msg.splitter"),
		// 		Url:          viper.GetString("atq.url"),
		// 		RioToken:     viper.GetString("atq.rio.token"),
		// 	})
		// }).
		// InitModuleIfEnabled("myoa.enabled", app.ModuleMyOA, func() error {
		// 	return myoa.Init(&myoa.Options{
		// 		Token: viper.GetString("myoa.token"),
		// 		Url:   viper.GetString("myoa.url"),
		// 	})
		// }).
		// InitModuleIfEnabled("workflow.enabled", app.ModuleWorkflow, func() error {
		// 	return workflow.Init(&workflow.Options{
		// 		Url: viper.GetString("workflow.url"),
		// 	})
		// }).
		InitModuleIfEnabled("redis.enabled", app.ModuleRedis, func() error {
			opts := redis.Options{
				Address:        viper.GetString("redis.address"),
				Db:             viper.GetInt("redis.db"),
				Password:       viper.GetString("redis.password"),
				ConnectTimeout: viper.GetDuration("redis.timeout.connect"),
				ReadTimeout:    viper.GetDuration("redis.timeout.read"),
				WriteTimeout:   viper.GetDuration("redis.timeout.write"),
				IdleTimeout:    viper.GetDuration("redis.timeout.idle"),
				MaxIdle:        viper.GetInt("redis.max.idle"),
				MaxActive:      viper.GetInt("redis.max.active"),
			}
			return redis.Init(opts)
		}).
		InitModuleIfEnabled("orm.enabled", app.ModuleOrm, func() error {
			dbSettings := &orm.DbSettings{
				Dialect:         viper.GetString("orm.db.dialect"),
				DbName:          viper.GetString("orm.db.dbname"),
				Host:            viper.GetString("orm.db.host"),
				Port:            viper.GetInt("orm.db.port"),
				User:            viper.GetString("orm.db.user"),
				Key:             viper.GetString("orm.db.key"),
				Password:        viper.GetString("orm.db.password"),
				MaxIdleConns:    viper.GetInt("orm.db.max_idle_conns"),
				MaxOpenConns:    viper.GetInt("orm.db.max_open_conns"),
				ConnMaxLifetime: viper.GetInt("orm.db.conn_max_lifetime"),
			}
			return orm.Init(dbSettings)
		}).InitModule(app.ModuleSysAPI, func() error {
		if disabled := viper.GetBool("sysapi.disabled"); disabled {
			xlog.Info("build-in apis are disabled")
			return nil
		}
		app.Instance.PubApi(router.SysRegister())
		return nil
	})

	return instance
}
