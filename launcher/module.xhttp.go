package launcher

// import (
// 	"fmt"
// 	"net/http"

// 	"gitee.com/nee-team/nimble/app"
// 	"gitee.com/nee-team/nimble/xhttp"
// 	"gitee.com/nee-team/nimble/zhiyan/monitor"
// )

// func initXhttp() error {
// 	if app.Instance.Monitor != nil {
// 		xhttp.RequestCallbackHandler = func(args xhttp.RequestCallbackArgs) {
// 			statusCode := 0
// 			if args.Res != nil {
// 				statusCode = args.Res.StatusCode
// 			}
// 			tags := monitor.Tagset{
// 				"path":          args.Req.URL.String(),
// 				"method":        args.Req.Method,
// 				"code":          fmt.Sprintf("%d", statusCode),
// 				"code_category": fmt.Sprintf("%dxx", statusCode/100),
// 			}
// 			// TODO: 允许规则可配置
// 			// 默认为 5xx,4xx 为失败，其他为成功
// 			success := statusCode != 0 && statusCode < http.StatusBadRequest
// 			metricsCount := ""
// 			metricsAvg := ""
// 			if success {
// 				metricsCount = monitor.HTTPOutboundCount_Success
// 				metricsAvg = monitor.HTTPOutboundAvgElapsed_Success
// 			} else {
// 				metricsCount = monitor.HTTPOutboundCount_Fail
// 				metricsAvg = monitor.HTTPOutboundAvgElapsed_Fail
// 			}

// 			// 上报请求量信息
// 			{
// 				data := monitor.ReportData{
// 					Metric: metricsCount,
// 					Value:  1,
// 					Tags:   tags,
// 				}
// 				app.Instance.Monitor.ReportCounter(data)
// 			}

// 			// 上报请求平均耗时
// 			{
// 				latency := float64(args.Elapsed.Milliseconds())
// 				data := monitor.ReportData{
// 					Metric: metricsAvg,
// 					Value:  latency,
// 					Tags:   tags,
// 				}
// 				app.Instance.Monitor.ReportAvgGauge(data)

// 				data = monitor.ReportData{
// 					Metric: monitor.HTTPOutboundAvgElapsed,
// 					Value:  latency,
// 					Tags:   tags,
// 				}
// 				app.Instance.Monitor.ReportAvgGauge(data)
// 			}

// 		}
// 	}
// 	return xhttp.Init(xhttp.DefaultCateName)
// }
