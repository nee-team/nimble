package distributed

import (
	"math/rand"
	"runtime"
	"strconv"
	"sync"
	"testing"
	"time"

	"gitee.com/nee-team/nimble/config"
	"gitee.com/nee-team/nimble/redis"

	"github.com/spf13/viper"
)

func initForTest() redis.Options {
	config.Init()
	options := redis.Options{
		Address:        viper.GetString("redis.address"),
		Db:             viper.GetInt("redis.db"),
		Password:       viper.GetString("redis.password"),
		ConnectTimeout: viper.GetDuration("redis.timeout.connect"),
		ReadTimeout:    viper.GetDuration("redis.timeout.read"),
		WriteTimeout:   viper.GetDuration("redis.timeout.write"),
		IdleTimeout:    viper.GetDuration("redis.timeout.idle"),
		MaxIdle:        viper.GetInt("redis.max.idle"),
		MaxActive:      viper.GetInt("redis.max.active"),
	}

	return options
}

func TestMain(m *testing.M) {
	option := initForTest()
	cache := redis.New(option)

	//初始化
	Init(cache)
	m.Run()
}

// TestConsistent 并发情况下一致性校验(综合测试)
func TestConsistent(t *testing.T) {
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))
	resourceID := "distlock_testconsistent" + strconv.Itoa(rand.Int())

	//设置并发协程数
	routineCount := 100
	runtime.GOMAXPROCS(routineCount)

	//统计值
	var sum = 0
	var nodeCount = int(1e3)

	var wg sync.WaitGroup
	wg.Add(nodeCount)

	// 为保证一致性校验成功，要求每个 routine 都能成功获取到锁
	// 单个routine 的操作时间，考虑 redis 耗时，预估大于 500ms
	// 则锁的最大等待时间，不能小于 500ms * nodeCount
	var lockWaitTime time.Duration = 500 * time.Duration(nodeCount)
	// 锁的持有时间，需要保证一次操作完成
	var lockTimeout time.Duration = 10

	for nodeIndex := 0; nodeIndex < nodeCount; nodeIndex++ {
		go func() {
			//上锁
			lockedID, err := Lock(resourceID, time.Millisecond*lockWaitTime, lockTimeout*time.Second)
			if err != nil {
				wg.Done()
				t.Fatal(err.Error())
			}

			//业务逻辑
			temp := sum
			temp++
			sum = temp

			//释放锁
			UnLock(resourceID, lockedID)
			wg.Done()
		}()
	}

	wg.Wait()
	if sum != nodeCount {
		t.Fatal("一致性校验失败")
	}
}

func TestLock(t *testing.T) {
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))
	resourceID := "distlock_testlock" + strconv.Itoa(rand.Int())

	//首次上锁
	lockedID, err := Lock(resourceID, 5*time.Second, 100*time.Second)
	if err != nil {
		t.Fatal(err.Error())
	}

	//尝试再次上锁
	_, err = Lock(resourceID, 5*time.Second, 100*time.Second)
	if err == nil || ErrorCode(err) != ErrLockTimeout {
		t.Fatal("资源被重复上锁")
	}

	//释放资源
	err = UnLock(resourceID, lockedID)
	if err != nil {
		t.Fatal(err.Error())
	}

	//资源释放后再次尝试上锁
	lockedID, err = Lock(resourceID, 5*time.Second, 5*time.Second)
	if err != nil {
		t.Fatal(err.Error())
	}

	//等待锁过期后再次尝试上锁
	lockedID, err = Lock(resourceID, 10*time.Second, 100*time.Second)
	if err != nil {
		t.Fatal(err.Error())
	}

	//最后释放锁
	err = UnLock(resourceID, lockedID)
	if err != nil {
		t.Fatal(err.Error())
	}
}

func TestTryLock(t *testing.T) {
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))
	resourceID := "distlock_testtrylock" + strconv.Itoa(rand.Int())

	//首次上锁
	lockedID, err := TryLock(resourceID, 10*time.Second)
	if err != nil {
		t.Fatal(err.Error())
	}

	//尝试再次上锁
	_, err = TryLock(resourceID, 10*time.Second)
	if err == nil || ErrorCode(err) != ErrTryLockFailed {
		t.Fatal("资源被重复上锁")
	}

	//释放锁
	err = UnLock(resourceID, lockedID)
	if err != nil {
		t.Fatal(err.Error())
	}
}

func TestUnLock(t *testing.T) {
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))
	resourceID := "distlock_testunlock" + strconv.Itoa(rand.Int())

	//上锁
	lockedID, err := TryLock(resourceID, 10*time.Second)
	if err != nil {
		t.Fatal(err.Error())
	}

	//lockedID校验
	err = UnLock(resourceID, "another_lockedID")
	if err == nil || ErrorCode(err) != ErrUnlockNotAcquired {
		t.Fatal("lockedID验证失效")
	}

	//释放锁
	err = UnLock(resourceID, lockedID)
	if err != nil {
		t.Fatal(err.Error())
	}

	lockedID, _ = TryLock(resourceID, 5*time.Second)
	time.Sleep(6 * time.Second)

	//锁超时被自动释放后，尝试手动释放
	err = UnLock(resourceID, lockedID)
	if err == nil || ErrorCode(err) != ErrUnlockNotAcquired {
		t.Fatal("未获得锁情况下释放锁成功")
	}
}

func TestQueryLock(t *testing.T) {
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))
	resourceID := "distlock_testquerylock" + strconv.Itoa(rand.Int())

	//上锁
	lockedID, err := TryLock(resourceID, 10*time.Second)
	if err != nil {
		t.Fatal(err.Error())
	}

	//锁查询
	acquired, err := QueryLock(resourceID, lockedID)
	if err != nil {
		t.Fatal(err.Error())
	}
	if acquired == false {
		t.Fatal("锁查询失败")
	}

	//lockedID校验
	acquired, err = QueryLock(resourceID, "another_lockedID")
	if err != nil {
		t.Fatal(err.Error())
	}
	if acquired == true {
		t.Fatal("lockedID校验失败")
	}

	UnLock(resourceID, lockedID)
	lockedID, _ = TryLock(resourceID, 5*time.Second)
	time.Sleep(6 * time.Second)

	//锁超时被自动释放后，执行锁查询
	acquired, err = QueryLock(resourceID, lockedID)
	if err != nil {
		t.Fatal(err.Error())
	}
	if acquired == true {
		t.Fatal("锁释放后，查询校验失败")
	}
}

func BenchmarkLock(b *testing.B) {
	rand.Seed(time.Now().UnixNano() / int64(time.Microsecond))
	resourceID := "distlock_testbenchlock" + strconv.Itoa(rand.Int())
	for i := 0; i < b.N; i++ {
		//上锁
		lockedID, err := Lock(resourceID, 60*time.Second, 100*time.Second)
		if err != nil {
			b.Fatal(err.Error())
		}

		//释放
		UnLock(resourceID, lockedID)
	}
}
