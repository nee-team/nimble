package distributed

const (
	ErrNotInit           = "ErrNotInit"           //包未初始化，需先执行Init方法
	ErrParams            = "ErrParams"            //参数有误
	ErrRedis             = "ErrRedis"             //redis底层错误
	ErrTryLockFailed     = "ErrTryLockFailed"     //锁被占用，非阻塞式获取锁失败
	ErrLockTimeout       = "ErrLockTimeout"       //锁被占用，上锁超时
	ErrUnlockNotAcquired = "ErrUnlockNotAcquired" //释放失败，这可能是未获取锁或者锁已超时被自动释放
)

type LockError struct {
	Code string
	Msg  string
}

func (err *LockError) Error() string {
	return err.Msg
}

// ErrorCode 解析错误码
func ErrorCode(err error) string {
	value, ok := err.(*LockError)
	if ok {
		return value.Code
	} else {
		return ""
	}
}

// ErrorCode 解析错误内容
func ErrorMsg(err error) string {
	value, ok := err.(*LockError)
	if ok {
		return value.Msg
	} else {
		return ""
	}
}
