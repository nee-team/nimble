//*************************
//*
//* distlock 基于redis的分布式锁组件
//*
//*************************
package distributed

import (
	"fmt"

	"time"

	"gitee.com/nee-team/nimble/redis"
	"gitee.com/nee-team/nimble/xlog"

	redigo "github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
)

const (
	DistLockPrefix = "DIST_LOCK_" //redis key 前缀
)

var (
	delay = 512 * time.Millisecond //上锁尝试的间隔
)

var (
	_cache *redis.Cache
)

// Init 初始化
//  - cache redis缓存对象
//  - err.Code
//    - ErrParams 参数有误(nil)
func Init(cache *redis.Cache) (err error) {
	if cache == nil {
		xlog.Fatal("[distlock][Init]cache不能为nil")
		return &LockError{Code: ErrParams, Msg: "cache不能为nil"}
	}
	_cache = cache
	return nil
}

// Init 初始化
//  - cache redis缓存对象
//  - lockDelay 上锁时两次尝试上锁之间的时间间隔(ns)
//  - err.Code
//    - ErrParams 参数有误(nil)
func InitWithArgs(cache *redis.Cache, lockDelay time.Duration) (err error) {
	if cache == nil {
		xlog.Fatal("[distlockInitWithArgs][InitWithArgs]cache不能为nil")
		return &LockError{Code: ErrParams, Msg: "cache不能为nil"}
	}
	if lockDelay <= 0 {
		xlog.Fatal("[distlock][InitWithArgs]lockDelay必须为正整数")
		return &LockError{Code: ErrParams, Msg: "lockDelay必须为正整数"}
	}
	_cache = cache
	delay = lockDelay
	return nil
}

// Lock 获取锁
//  - resourceID 本次期望上锁的资源
//  - waitTimeout 获取锁操作最长等待时间，超时返回错误
//  - lockTimeout 锁超时时间，超过将自动释放锁
//  - lockedID 资源标记，释放锁时需要根据lockedID释放
//  - err.Code
//    - ErrNotInit 包没有初始化，需先调用Init
//    - ErrRedis redis底层错误
//    - ErrParams 参数有误
//    - ErrLockTimeout 锁被占用，上锁超时
func Lock(resourceID string, waitTimeout, lockTimeout time.Duration) (lockedID string, err error) {
	if _cache == nil {
		xlog.Error("[distlock][Lock]包未初始化，需先执行Init方法")
		return "", &LockError{Code: ErrNotInit, Msg: "包未初始化，需先执行Init方法"}
	}

	if waitTimeout < 0 || lockTimeout < 0 {
		xlog.Error("[distlock][Lock]参数有误,超时时间不能为负")
		return "", &LockError{Code: ErrParams, Msg: "超时时间不能为负"}
	}

	//尝试上锁
	lockedID, err = tryLock(resourceID, lockTimeout)

	//尝试上锁失败
	if err != nil {
		if ErrorCode(err) != ErrTryLockFailed {
			xlog.Error("[distlock][Lock]尝试上锁失败:" + err.Error())
			return "", err
		}
	} else {
		//上锁成功
		return lockedID, err
	}

	waitTimer := time.NewTimer(waitTimeout)
	lockTimer := time.NewTimer(delay)

	for {
		select {
		case <-waitTimer.C: //超时时间
			waitTimer.Stop()
			lockTimer.Stop()
			xlog.Error("[distlock][Lock]上锁超时")
			return "", &LockError{Code: ErrLockTimeout, Msg: "上锁操作超时"}
		case <-lockTimer.C: //循环上锁定时器

			//尝试上锁
			lockedID, err = tryLock(resourceID, lockTimeout)
			if err != nil && ErrorCode(err) == ErrTryLockFailed {
				lockTimer.Reset(delay)
				continue
			}
			waitTimer.Stop()
			lockTimer.Stop()
			return lockedID, err
		}
	}
}

func tryLock(resourceID string, lockTimeout time.Duration) (lockedID string, err error) {
	key := DistLockPrefix + resourceID
	value := uuid.New().String()
	//SETNX 尝试上锁
	reply, _err := setNXScript.Do(_cache, key, value, int64(lockTimeout/time.Second))
	if _err != nil {
		return "", &LockError{Code: ErrRedis, Msg: _err.Error()}
	}
	if fmt.Sprint(reply) != "1" {
		return "", &LockError{Code: ErrTryLockFailed, Msg: "非阻塞式获取锁失败,可能是锁已被占用"}
	}
	return value, nil
}

// TryLock 非阻塞式获取锁
//  - resourceID 本次期望上锁的资源
//  - lockTimeout 锁超时时间，超过将自动释放锁
//  - lockedID 资源标记，释放锁时需要根据lockedID释放
//  - err.Code
//    - ErrNotInit 包没有初始化，需先调用Init
//    - ErrRedis redis底层错误
//    - ErrParams 参数有误
//    - ErrTryLockFailed 锁被占用，上锁失败
func TryLock(resourceID string, lockTimeout time.Duration) (lockedID string, err error) {
	if _cache == nil {
		xlog.Error("[distlock][TryLock]包未初始化，需先执行Init方法")
		return "", &LockError{Code: ErrNotInit, Msg: "包未初始化，需先执行Init方法"}
	}

	if lockTimeout < 0 {
		xlog.Error("[distlock][TryLock]参数有误，超时时间不能为负")
		return "", &LockError{Code: ErrParams, Msg: "超时时间不能为负"}
	}
	//尝试上锁
	lockedID, err = tryLock(resourceID, lockTimeout)

	//尝试上锁失败,记录日志
	if err != nil {
		xlog.Error("[distlock][TryLock],err:" + err.Error())
	}
	return lockedID, err
}

// UnLock 释放锁
//  - resourceID 本次期望释放的资源
//  - lockedID 资源标记，由上锁操作时返回。如果资源仍由当前节点持有，释放成功，否则失败
//  - err.Code
//    - ErrNotInit 包没有初始化，需先调用Init
//    - ErrRedis redis底层错误
//    - ErrUnlockNotAcquired 当前未持有锁，释放失败，这可能是因为锁过期了
func UnLock(resourceID string, lockedID string) error {
	if _cache == nil {
		xlog.Error("[distlock][UnLock]包未初始化，需先执行Init方法")
		return &LockError{Code: ErrNotInit, Msg: "包未初始化，需先执行Init方法"}
	}
	key := DistLockPrefix + resourceID
	ret, err := delScript.Do(_cache, key, lockedID)
	if err != nil {
		xlog.Error("[distlock][UnLock]释放出错：" + err.Error())
		return &LockError{Code: ErrRedis, Msg: err.Error()}
	}
	if fmt.Sprint(ret) != "1" {
		xlog.Error("[distlock][UnLock]释放失败，这可能是因为未获取锁或者锁因超时已被自动释放")
		return &LockError{Code: ErrUnlockNotAcquired, Msg: "释放失败，这可能是因为未获取锁或者锁因超时已被自动释放"}
	}
	return nil
}

// QueryLock 查询是标记是否有效
//  - resourceID 本次查询的资源
//  - lockedID 获取锁时返回的资源标记
//  - acquired 当前是否仍持有锁
//  - err.Code
//    - ErrNotInit 包没有初始化，需先调用Init
//    - ErrRedis redis底层错误
func QueryLock(resourceID string, lockedID string) (acquired bool, err error) {
	if _cache == nil {
		xlog.Error("[distlock][QueryLock]包未初始化，需先执行Init方法")
		return false, &LockError{Code: ErrNotInit, Msg: "包未初始化，需先执行Init方法"}
	}
	key := DistLockPrefix + resourceID
	value, _err := _cache.GetString(key)
	if _err != nil {
		//资源未上锁或已过期
		if _err == redigo.ErrNil {
			xlog.Info("[distlock][QueryLock]锁已过期")
			return false, nil
		}
		xlog.Error("[distlock][QueryLock]redis错误：" + _err.Error())
		return false, &LockError{Code: ErrRedis, Msg: _err.Error()}
	}
	if len(lockedID) == 0 || value != lockedID {
		xlog.Info("[distlock][QueryLock]锁已过期")
		return false, nil
	}
	return true, nil
}

var delScript = redis.NewScript(1, `
if redis.call("get", KEYS[1]) == ARGV[1] then
	return redis.call("del", KEYS[1])
else
	return 0
end`)

var setNXScript = redis.NewScript(1, `
if redis.call("setnx", KEYS[1],ARGV[1]) == 1 then
	return redis.call("expire", KEYS[1],ARGV[2])
else
	return 0
end`)
